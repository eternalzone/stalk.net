#!/usr/bin/env bash

openssl ecparam -genkey -name prime256v1 -noout |tee priv.key| openssl ec -pubout|base64 && cat priv.key|base64 && rm priv.key