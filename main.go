package main

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/conf/params"
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/helpers/types"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api"
	"bitbucket.org/eternalzone/stalk.net/services/daemons"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"bitbucket.org/eternalzone/stalk.net/services/messaging"
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	vanillaLog "log"
	"os"
	"time"
)

const migrationsDir = "./dao/migrations"

func main() {
	// OS FLAGS
	configFile := flag.String("conf", "./config.json", "Pathname to config file")

	// Init Configuration
	cfg, err := conf.NewFromFile(configFile)
	if err != nil {
		log.Fatalf("Cannot decode config: %s", err.Error())
	}

	// Setup log
	log.Init(cfg.LogLevel)

	mysqlArgs := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		cfg.Mysql.User,
		cfg.Mysql.Password,
		cfg.Mysql.Host,
		cfg.Mysql.Port,
		cfg.Mysql.Database,
	)
	newLogger := logger.New(
		vanillaLog.New(os.Stdout, "\r\n", vanillaLog.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,        // Disable color
		},
	)
	m, err := gorm.Open(mysql.New(mysql.Config{
		DSN: mysqlArgs,
	}), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		logrus.Fatalf("Connection to Mysql cannot be established: %s", err.Error())
	}
	//m.LogMode(cfg.Mysql.DebugMode)

	dbMigrate(cfg)

	d := dao.NewGormDao(cfg, m)

	emailModule, err := mailer.NewMailService(cfg)
	if err != nil {
		log.Fatalf("Cannot run mailer module: %s", err.Error())
	}

	apiModule := api.NewAPI(cfg, m, d, emailModule)

	daemonsModule := daemons.NewDaemons(cfg, m, emailModule)

	messagingModule, err := messaging.NewMessaging(cfg, m)
	if err != nil {
		log.Fatalf("Cannot run messaging module: %s", err.Error())
	}

	RunModules(apiModule, daemonsModule, messagingModule)
}

func dbMigrate(c conf.Config) {
	// Mysql migrations
	err := helpers.Migrate(&params.MysqlParams{
		User:     c.Mysql.User,
		Password: c.Mysql.Password,
		Host:     c.Mysql.Host,
		Port:     c.Mysql.Port,
		Database: c.Mysql.Database,
	}, migrationsDir)
	if err != nil {
		logrus.Fatalf("Migrations to Mysql cannot be made: %s", err.Error())
	}
}

func RunModules(modules ...types.Module) {
	if len(modules) > 0 {
		for _, m := range modules {
			log.Infof("Starting module %s", m.Title())
			go m.Run()
		}

		select {} //lock execution
	}
}
