package conf

import (
	"encoding/json"
	"github.com/Nargott/goutils"
	"github.com/fatih/structs"
	"github.com/mitchellh/mapstructure"
	"os"
	"reflect"
)

// BaseConfig is interface for all validatable structures
type BaseConfig interface {
	Validate() error
}

// Init loads config data from files or from ENV
func Init(cfg interface{}, filename *string) error {
	//check if file with config exists
	if _, err := os.Stat(*filename); os.IsNotExist(err) {
		//expected file with config not exists
		//check special /.secrets directory (DevOps special)
		developmentConfigPath := "/.secrets/config.json"
		if _, err := os.Stat(developmentConfigPath); os.IsNotExist(err) {
			return err
		}

		filename = &developmentConfigPath
	}

	file, err := os.Open(*filename)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&cfg)
	if err != nil {
		return err
	}

	return overrideConfigWithEnvVars(cfg)
}

//Init loads configuration from the provided byte array.
func InitFromByteArray(cfg interface{}, buf []byte) error {
	err := json.Unmarshal(buf, cfg)
	if err != nil {
		return err
	}
	return overrideConfigWithEnvVars(cfg)
}

//overrideConfigWithEnvVars Overrides configuration with env parameters if any provided in UNDERSCORED_UPPERCASED format
func overrideConfigWithEnvVars(cfg interface{}) error {
	configsMap := structs.Map(cfg)

	for key := range configsMap {
		envVal, isPresent := os.LookupEnv(goutils.UndescoreUppercased(key))
		if isPresent {
			configsMap[key] = envVal
		}
	}

	err := mapstructure.Decode(configsMap, &cfg)
	if err != nil {
		return err
	}

	return err
}

// ValidateBaseConfigStructs validates additional structures (which implements BaseConfig)
func ValidateBaseConfigStructs(cfg interface{}) (err error) {
	v := reflect.ValueOf(cfg).Elem()
	baseConfigType := reflect.TypeOf((*BaseConfig)(nil)).Elem()

	for i := 0; i < v.NumField(); i++ {
		if v.Type().Field(i).Type.Implements(baseConfigType) {
			err = v.Field(i).Interface().(BaseConfig).Validate()
			if err != nil {
				return
			}
		}
	}

	return
}
