package params

import (
	"fmt"
	"time"
)

// ServiceParams is structure for basic service params
type ServiceParams struct {
	AppId        string
	ListenOnPort int
	WaitTimeout  time.Duration
	FullHostName string
}

// Validates ServiceParams fields
func (this ServiceParams) Validate() error {
	if this.ListenOnPort == 0 {
		return fmt.Errorf("bad ListenOnPort for service")
	}

	if this.AppId == "" {
		return fmt.Errorf("AppId is empty")
	}

	if this.FullHostName == "" {
		return fmt.Errorf("FullHostName is empty")
	}

	return nil
}
