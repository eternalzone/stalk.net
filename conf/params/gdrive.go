package params

import (
	"fmt"
)

type GoogleDriveParams struct {
	ClientID     string
	ClientSecret string
	IsEnabled    bool
}

// Validates GoogleDriveParams fields
func (gdp GoogleDriveParams) Validate() error {
	if gdp.IsEnabled && (gdp.ClientID == "" || gdp.ClientSecret == "") {
		return fmt.Errorf("GoogleDrive client data is empty")
	}

	return nil
}
