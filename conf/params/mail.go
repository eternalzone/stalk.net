package params

//Email SMTP params for system notification
type SmtpParams struct {
	Host      string
	Port      int
	User      string
	Password  string
	FromEmail string
	FromName  string
}
