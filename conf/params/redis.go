package params

import "fmt"

// Redis config struct
type Redis struct {
	Host string
	Port int
	DB   int //Default 0
}

// Validate checks Redis field
func (this Redis) Validate() error {
	if this.Host == "" {
		return fmt.Errorf("Bad param: Host")
	}
	if this.Port == 0 {
		return fmt.Errorf("Bad param: Port")
	}

	return nil
}
