package params

import (
	"fmt"
)

type TelegramParams struct {
	BotToken  string
	IsEnabled bool
}

// Validates ServiceParams fields
func (tp TelegramParams) Validate() error {
	if tp.IsEnabled && tp.BotToken == "" {
		return fmt.Errorf("BotToken is empty")
	}

	return nil
}
