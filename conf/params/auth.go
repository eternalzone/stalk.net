package params

import (
	"encoding/base64"
	"fmt"
)

type AuthParams struct {
	ECDSAPublicKeyBase64  string
	ECDSAPrivateKeyBase64 string
}

// Validates ServiceParams fields
func (ap AuthParams) Validate() error {
	_, err := base64.StdEncoding.DecodeString(ap.ECDSAPublicKeyBase64)
	if err != nil {
		return fmt.Errorf("cannot decode ECDSAPublicKeyBase64: %s", err.Error())
	}

	_, err = base64.StdEncoding.DecodeString(ap.ECDSAPrivateKeyBase64)
	if err != nil {
		return fmt.Errorf("cannot decode ECDSAPrivateKeyBase64: %s", err.Error())
	}

	return nil
}
