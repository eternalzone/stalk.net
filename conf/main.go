package conf

import (
	"bitbucket.org/eternalzone/stalk.net/conf/params"
	"bitbucket.org/eternalzone/stalk.net/log"
)

const (
	// Seconds to Withdraw Token will stay expired
	DelayCleanerExpiredTempImages = 3 * 60 * 60 // in seconds
	DelayRunMailerResender        = 2 * 60 * 60 // in seconds

	TtlTempImages = 12 * 60 * 60 // in seconds (8 hours, 2 times per day)

	TtlJWT          = 8 * 60 * 60  //8 hours
	TtlRefreshToken = 12 * 60 * 60 // 12 hours
)

type Config struct {
	LogLevel log.LogLevel

	Service     params.ServiceParams
	Mysql       params.MysqlParams
	SmtpSystem  params.SmtpParams
	Telegram    params.TelegramParams
	Auth        params.AuthParams
	GoogleDrive params.GoogleDriveParams

	FrontendHost string
}

func NewFromFile(filename *string) (Config, error) {
	var cfg = Config{}
	err := Init(&cfg, filename)
	if err != nil {
		return cfg, err
	}

	return cfg, cfg.Validate()
}

//validates all Config fields
func (c *Config) Validate() error {
	return ValidateBaseConfigStructs(c)
}
