FROM golang AS build-env
#RUN apk update && \
#    apk add git gcc build-base

WORKDIR /go/src/stalk.net
COPY . .

RUN go get
RUN go build

FROM alpine
#RUN apk update && \
#    apk add ca-certificates && \
#    update-ca-certificates && \
#    rm -rf /var/cache/apk/*
WORKDIR /app

RUN mkdir -p dao/mysql/migrations
RUN mkdir -p static
RUN mkdir -p photos

COPY --from=build-env /go/src/bitbucket.org/eternalzone/stalk.net/dao/mysql/migrations /app/dao/mysql/migrations
COPY --from=build-env /go/src/bitbucket.org/eternalzone/stalk.net/stalk.net /app
COPY --from=build-env /go/src/bitbucket.org/eternalzone/stalk.net/static /app/static
COPY --from=build-env /go/src/bitbucket.org/eternalzone/stalk.net/photos /app/photos

ENV LISTEN_ON_PORT=2000
EXPOSE ${LISTEN_ON_PORT}:${LISTEN_ON_PORT}

CMD ["/app/stalk.net"]