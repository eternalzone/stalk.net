import { Injectable } from "@angular/core";
import { ObservableCursor } from "meteor-rxjs";
import { PlacesCollection } from "../../../../both/collections/places.collection";

@Injectable()
export class PlacesDataService {
    private data: ObservableCursor<any>;

    constructor() {
        this.data = PlacesCollection.find({});
    }

    public getData(): ObservableCursor<any> {
        return this.data;
    }
}
