import { Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {PlacesCollection} from "../../../../both/collections/places.collection";

import template from './places-add.component.html';

@Component({
    selector: "places-add",
    template,
})

export class PlacesAddComponent implements OnInit{
    addFormPlace: FormGroup;

    constructor(private formBuilderPlace:FormBuilder){}

    ngOnInit(){
        this.addFormPlace = this.formBuilderPlace.group({
           title: ['', Validators.required],
           description: ['', Validators.required],
           //isActive: [true],
        });
    }

    addPlace():void {
    if (this.addFormPlace.valid) {
        PlacesCollection.insert(this.addFormPlace.value);
        this.addFormPlace.reset();
    }
}
}