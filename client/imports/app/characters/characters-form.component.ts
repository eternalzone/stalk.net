import {Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { CharactersCollection } from "../../../../both/collections/characters.collection";

import template from './characters-form.component.html';

@Component({
   selector : "character-form",
   template



})
export class CharactersFormComponent implements OnInit{

    addForm:FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit(){
        this.addForm = this.formBuilder.group({
            nickname  : ['', Validators.required],
            legend: [''],
            legendPrivate : ['']
        });
    }
    addCharacter():void{
        if(this.addForm.valid){
            CharactersCollection.insert(this.addForm.value);
            this.addForm.reset();
        }
    }

}

