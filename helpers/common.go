package helpers

import (
	"runtime"
	"strings"
)

func GetCurrentFuncName() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	path := strings.Split(frame.Function, "/")
	if len(path) > 0 {
		return path[len(path)-1]
	}

	return "unknown"
}
