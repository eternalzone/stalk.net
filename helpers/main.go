package helpers

import (
	"fmt"
	"github.com/goware/emailx"
	"github.com/sirupsen/logrus"
	"github.com/wedancedalot/decimal"
	"math/rand"
	"net"
	"net/http"
	"strings"
	"time"
)

func DecimalToNullDecimal(value decimal.Decimal) decimal.NullDecimal {
	return decimal.NullDecimal{
		Decimal: value,
		Valid:   true,
	}
}

func GetClearIpAddress(r *http.Request) string {
	ip := r.Header.Get("X-Forwarded-For")
	if ip == "" {
		ip = r.RemoteAddr
	}
	clearIp, _, err := net.SplitHostPort(ip)
	if err != nil {
		logrus.Debugf("Can not clean ip address %s, error: %v", ip, err)
		return ip
	}

	return clearIp
}

func GetUnixMillis() uint64 {
	return uint64(time.Now().UTC().UnixNano() / int64(time.Millisecond))
}

func RandString(n int) string {
	const letterBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" //abcdefghijklmnopqrstuvwxyz

	rand.Seed(time.Now().UnixNano())

	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func RandFullString(n int) string {
	const letterBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz_"

	rand.Seed(time.Now().UnixNano())

	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func RandFullCharString(n int) string {
	const letterBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz_*&%$#@!"

	rand.Seed(time.Now().UnixNano())

	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func RandHexString(n int) string {
	const hexBytes = "ABCDEF1234567890" //abcdefghijklmnopqrstuvwxyz

	rand.Seed(time.Now().UnixNano())

	b := make([]byte, n)
	for i := range b {
		b[i] = hexBytes[rand.Intn(len(hexBytes))]
	}
	return string(b)
}

func RandNumbersString(n int) string {
	const hexBytes = "1234567890"

	rand.Seed(time.Now().UnixNano())

	b := make([]byte, n)
	for i := range b {
		b[i] = hexBytes[rand.Intn(len(hexBytes))]
	}
	return string(b)
}

func GetImagePath(r *http.Request, name string) string {
	if name == "" {
		return ""
	}

	var URLScheme string

	if r != nil {
		URLScheme = r.URL.Scheme
	}

	if URLScheme == "" {
		URLScheme = "http"
	}

	return URLScheme + "://" + r.Host + "/photos/" + name
}

func ValidateEmail(email string) error {
	if strings.Contains(email, "+") {
		return fmt.Errorf("email %s contain '+' character", email)
	}

	return emailx.ValidateFast(email)
}
