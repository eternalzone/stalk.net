package types

import "context"

type (
	Module interface {
		Run()
		Title() string
		GracefulStop(context.Context) error
	}
)