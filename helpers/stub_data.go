package helpers

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"encoding/csv"
	"fmt"
	"gorm.io/gorm"
	"math/rand"
	"os"
	"strings"
	"time"
)

var doNotDisturbUsersEmailsList = []string{
	"KiberPUNK.mail@gmail.com",
	"ps2014yeganian@gmail.com",
}

func readFileData(fn string) ([]string, error) {
	var result = make([]string, 0)

	/*jsonFile, err := os.Open(fn)
	if err != nil {
		return result, fmt.Errorf("can't read file %s: %s", fn, err.Error())
	}
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return result, fmt.Errorf("can't read file %s content: %s", fn, err.Error())
	}

	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		return result, fmt.Errorf("can't parse JSON from file %s: %s", fn, err.Error())
	}

	err = jsonFile.Close()
	if err != nil {
		return result, fmt.Errorf("can't close file %s: %s", fn, err.Error())
	}*/

	csvfile, err := os.Open(fn)
	if err != nil {
		return result, fmt.Errorf("can't read file %s: %s", fn, err.Error())
	}
	r := csv.NewReader(csvfile)
	r.Comma = ';'
	lines, err := r.ReadAll()
	if err != nil {
		return result, fmt.Errorf("can't read file %s content: %s", fn, err.Error())
	}

	for _, line := range lines {
		result = append(result, line[1])
	}

	return result, nil
}

func FillStubbedData(c conf.Config, db *gorm.DB) error {
	//open data files
	rusNames, err := readFileData("./helpers/stub_data/russian_names.csv")
	if err != nil {
		return err
	}

	rusSurNames, err := readFileData("./helpers/stub_data/russian_surnames.csv")
	if err != nil {
		return err
	}

	DBTX := dao.NewTransaction(db)
	defer DBTX.Rollback()

	var usersList []models.User
	res := DBTX.DB.Where("`usr_email` NOT IN (?)", doNotDisturbUsersEmailsList).Find(&usersList)
	if res.Error != nil {
		return fmt.Errorf("loading user from DB error: %s", res.Error.Error())
	}

	var str string
	for i := range usersList {
		usersList[i].FirstName = rusNames[rand.Intn(len(rusNames))]
		usersList[i].LastName = rusSurNames[rand.Intn(len(rusSurNames))]

		str = RandString(rand.Intn(30-8+1) + 8)
		str += "@gmail.com"
		str = strings.ToLower(str)
		usersList[i].Email = str

		str = "+38 (0" //+38 (066) 328-0887
		str += RandNumbersString(2)
		str += ") "
		str += RandNumbersString(3)
		str += "-"
		str += RandNumbersString(4)
		usersList[i].Phone.String = str

		<-time.After(time.Millisecond) //wait for a proper random seed

		res = DBTX.DB.Save(usersList[i])
		if res.Error != nil {
			return fmt.Errorf("can't save user to DB error: %s", res.Error.Error())
		}
	}

	err = DBTX.Commit()
	if res.Error != nil {
		return fmt.Errorf("can't commit TX: %s", res.Error.Error())
	}

	return nil
}
