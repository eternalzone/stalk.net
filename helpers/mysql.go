package helpers

import (
	"bitbucket.org/eternalzone/stalk.net/conf/params"
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/rubenv/sql-migrate"
	"github.com/wedancedalot/decimal"
	"os"
	"path/filepath"
	"time"
)

// Migrate makes migration for DB from migrationsDir
func Migrate(c *params.MysqlParams, migrationsDir string) error {
	ex, err := os.Executable()
	if err != nil {
		return err
	}

	dir := filepath.Join(filepath.Dir(ex), migrationsDir)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		//return errors.New("Migrations dir does not exist: " + dir)
		dir = migrationsDir
		if _, err := os.Stat(migrationsDir); os.IsNotExist(err) {
			return errors.New("Migrations dir does not exist: " + dir)
		}
	}

	db, err := sqlx.Connect("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&multiStatements=true&parseTime=true", c.User, c.Password, c.Host, c.Port, c.Database))
	if err != nil {
		return err
	}

	migrations := &migrate.FileMigrationSource{
		Dir: dir,
	}

	_, err = migrate.Exec(db.DB, "mysql", migrations, migrate.Up)
	return err
}

func ToNullDecimal(value decimal.Decimal) decimal.NullDecimal {
	return decimal.NullDecimal{
		Decimal: value,
		Valid:   true,
	}
}

func ToNullTime(value time.Time) sql.NullTime {
	return sql.NullTime{
		Time:  value,
		Valid: !value.IsZero(),
	}
}

func ToNullString(s string) sql.NullString {
	return sql.NullString{
		String: s,
		Valid:  s != "",
	}
}

func ToNullBool(value bool) sql.NullBool {
	return sql.NullBool{
		Bool:  value,
		Valid: true,
	}
}

func ToNullInt64(value int64) sql.NullInt64 {
	return sql.NullInt64{
		Int64: value,
		Valid: true,
	}
}

func ToNullInt64NonZero(value int64) sql.NullInt64 {
	return sql.NullInt64{
		Int64: value,
		Valid: value != 0,
	}
}

func ToNullFloat64(value float64) sql.NullFloat64 {
	return sql.NullFloat64{
		Float64: value,
		Valid:   true,
	}
}
