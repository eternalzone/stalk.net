package models

import (
	"database/sql"
	"database/sql/driver"
	"time"
)

type (
	ItemType  string
	ItemOwner string
	ItemPlace string
)

const (
	ItemSuit     ItemType = "suit"
	ItemArtefact ItemType = "artefact"
	ItemQuest    ItemType = "quest"
	ItemOther    ItemType = "other"
)

const (
	ItemOwnerOwn      ItemOwner = "own"
	ItemOwnerPersonal ItemOwner = "personal"
	ItemOwnerOther    ItemOwner = "other"
)

const (
	ItemLocationStorage ItemPlace = "storage"
	ItemLocationgGarage ItemPlace = "garage"
	ItemLocationHome    ItemPlace = "home"
	ItemLocationOther   ItemPlace = "other"
)

const ItemCodeLength = 4

type Item struct {
	Id              uint           `gorm:"column:itm_id;primary_key"`
	Code            sql.NullString `gorm:"column:itm_code"`
	Desc            string         `gorm:"column:itm_description"`
	Title           string         `gorm:"column:itm_title"`
	ImageId         uint           `gorm:"column:img_id"`
	ItemType        ItemType       `gorm:"column:itm_type"`
	Owner           ItemOwner      `gorm:"column:itm_owner"`
	Place           ItemPlace      `gorm:"column:itm_place"`
	IsVirtual       bool           `gorm:"column:itm_is_virtual"`
	IsActive        bool           `gorm:"column:itm_is_active"`
	Comment         string         `gorm:"column:itm_comment"`
	Data            string         `gorm:"column:itm_data"`
	CreatedByUserId uint           `gorm:"column:itm_created_by_usr_id"`
	LocationId      uint           `gorm:"column:loc_id"`
	CreatedAt       time.Time      `gorm:"column:itm_created_at"`
	UpdatedAt       sql.NullTime   `gorm:"column:itm_updated_at"`

	GPSId uint `gorm:"-"`
	GPS   GPS  `gorm:"-"`

	Image    Image    `gorm:"foreignKey:img_id"`
	Location Location `gorm:"foreignKey:loc_id"`

	GPSList []GPS `gorm:"many2many:items_gps;foreignKey:itm_id;joinForeignKey:itm_id;References:gps_id;JoinReferences:gps_id"`
}

func (u *ItemType) Scan(value interface{}) error { *u = ItemType(value.([]byte)); return nil }
func (u ItemType) Value() (driver.Value, error)  { return string(u), nil }

func (u *ItemOwner) Scan(value interface{}) error { *u = ItemOwner(value.([]byte)); return nil }
func (u ItemOwner) Value() (driver.Value, error)  { return string(u), nil }

func (u *ItemPlace) Scan(value interface{}) error { *u = ItemPlace(value.([]byte)); return nil }
func (u ItemPlace) Value() (driver.Value, error)  { return string(u), nil }
