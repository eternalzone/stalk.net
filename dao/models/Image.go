package models

import (
	"database/sql"
	"database/sql/driver"
	"gorm.io/gorm"
	"time"
)

type ImageType string

const (
	ImageBasePath    = "./images"
	ImageBaseURLPath = "/photos/"
)

const (
	ImagePhoto  ImageType = "photo"
	ImageAvatar ImageType = "avatar"
	ImageIcon   ImageType = "icon"
	ImageItem   ImageType = "item"
	ImageNone   ImageType = "none"
)

type Image struct {
	Id        uint         `gorm:"column:img_id;primary_key"`
	Name      string       `gorm:"column:img_name"`
	ImageType ImageType    `gorm:"column:img_type"`
	IsTemp    bool         `gorm:"column:img_is_tmp"`
	CreatedAt time.Time    `gorm:"column:img_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:img_updated_at"`

	Path    string `gorm:"-"`
	SysPath string `gorm:"-"`
}

const (
	ImagesSysPathDir       = "/images/"
	StaticImagesSysPathDir = "/images/static/"
	PassportImagesDir      = "passports"
)

func (u *ImageType) Scan(value interface{}) error { *u = ImageType(value.([]byte)); return nil }
func (u ImageType) Value() (driver.Value, error)  { return string(u), nil }

func (i *Image) AfterFind(tx *gorm.DB) (err error) {
	i.Path = ImageBaseURLPath + i.Name
	i.SysPath = ImagesSysPathDir + i.Name
	return nil
}
