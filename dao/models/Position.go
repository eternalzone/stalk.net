package models

import (
	"database/sql"
	"time"
)

type Position struct {
	Id         uint         `gorm:"column:pos_id;primary_key"`
	Title      string       `gorm:"column:pos_title"`
	Desc       string       `gorm:"column:pos_description"`
	LocationId uint         `gorm:"column:loc_id"`
	ImageId    uint         `gorm:"column:img_id"`
	IsActive   bool         `gorm:"column:pos_is_active"`
	CreatedAt  time.Time    `gorm:"column:pos_created_at"`
	UpdatedAt  sql.NullTime `gorm:"column:pos_updated_at"`

	Location Location `gorm:"foreignKey:loc_id"`
	Image    Image    `gorm:"foreignKey:img_id"`

	Members []Member `gorm:"many2many:positions_members;foreignKey:pos_id;joinForeignKey:pos_id;References:mbr_id;JoinReferences:mbr_id"`
}
