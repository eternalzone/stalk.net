package models

import (
	"database/sql"
	"database/sql/driver"
	"net/http"
	"time"
)

type (
	FirmwareType string
)

const (
	FirmwareUnknown  FirmwareType = "unknown"
	FirmwareArtefact FirmwareType = "artefact"
	FirmwareSoul     FirmwareType = "soul"
	FirmwareDefence  FirmwareType = "defence"
	FirmwareAnomaly  FirmwareType = "anomaly"
)

func (ft FirmwareType) IsValid() bool {
	switch ft {
	case FirmwareArtefact,
		FirmwareSoul,
		FirmwareDefence,
		FirmwareAnomaly,
		FirmwareUnknown:
		return true
	default:
		return false
	}
}

const (
	FirmwareBasePath    = "firmwares"
	FirmwareBaseURLPath = "/data/firmwares"
)

type Firmware struct {
	Id            uint           `gorm:"column:fwr_id;primary_key"`
	Version       uint           `gorm:"column:fwr_version"`
	Title         sql.NullString `gorm:"column:fwr_title"`
	Desc          sql.NullString `gorm:"column:fwr_description"`
	FirmwareType  FirmwareType   `gorm:"column:fwr_type"`
	FirmwareTagId sql.NullInt64  `gorm:"column:ftg_id"`
	EventId       sql.NullInt64  `gorm:"column:evt_id"`
	Filename      string         `gorm:"column:fwr_filename"`
	IsCurrent     bool           `gorm:"column:fwr_is_current"`

	CreatedAt time.Time    `gorm:"column:fwr_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:fwr_updated_at"`

	URL string `gorm:"-"`

	FirmwareTag *FirmwareTag `gorm:"foreignKey:ftg_id"`
	Event       *Event       `gorm:"foreignKey:evt_id"`
}

type FirmwareQueryParams struct {
	FirmwareTagTitle string
	FirmwareTagId    uint
	EventId          uint
	FirmwareId       uint
}

func (fw *Firmware) GenerateFirmwareFilePath(r *http.Request) {
	if fw == nil {
		return
	}
	if r == nil {
		return
	}

	var URLScheme string
	URLScheme = r.URL.Scheme

	if URLScheme == "" || URLScheme == "https" {
		URLScheme = "http"
	}
	fw.URL = URLScheme + "://" + r.Host + FirmwareBaseURLPath + "/" + string(fw.FirmwareType) + "/" + fw.Filename
}

func (ft *FirmwareType) Scan(value interface{}) error { *ft = FirmwareType(value.([]byte)); return nil }
func (ft FirmwareType) Value() (driver.Value, error)  { return string(ft), nil }
