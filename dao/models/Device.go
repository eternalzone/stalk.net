package models

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"time"

	"gorm.io/datatypes"
)

type (
	DeviceType string
)

const (
	DeviceTypeUnknown  DeviceType = "unknown"
	DeviceTypeArtefact DeviceType = "artefact"
	DeviceTypeSoul     DeviceType = "soul"
	DeviceTypeDefence  DeviceType = "defence"
)

func (dt DeviceType) IsValid() bool {
	switch dt {
	case DeviceTypeUnknown,
		DeviceTypeArtefact,
		DeviceTypeSoul:
		return true
	default:
		return false
	}
}

type Device struct {
	Id            uint           `gorm:"column:dev_id;primary_key"`
	UUID          uuid.UUID      `gorm:"column:dev_uuid"`
	MAC           string         `gorm:"column:dev_mac"`
	Type          DeviceType     `gorm:"column:dev_type"`
	SubType       uint8          `gorm:"column:dev_sub_type"`
	IsActive      bool           `gorm:"column:dev_is_active"`
	FirmwareId    sql.NullInt64  `gorm:"column:fwr_id"`
	FirmwareTagId sql.NullInt64  `gorm:"column:ftg_id"`
	MemberId      sql.NullInt64  `gorm:"column:mbr_id"`
	Config        datatypes.JSON `gorm:"column:dev_config"`

	ActiveAt  sql.NullTime `gorm:"column:dev_active_at"`
	CreatedAt time.Time    `gorm:"column:dev_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:dev_updated_at"`

	Firmware    *Firmware    `gorm:"foreignKey:fwr_id"`
	FirmwareTag *FirmwareTag `gorm:"foreignKey:ftg_id"`
	Member      *Member      `gorm:"foreignKey:mbr_id"`

	LatestFirmware *Firmware `gorm:"-"`
}

func (d Device) Validate() error {
	/*if len(d.MAC) != 12 {
		return fmt.Errorf("wrong MAC length %d, must be %d", len(d.MAC), 12)
	}*/
	if len(d.MAC) < 5 {
		return fmt.Errorf("wrong MAC length %d, must be %d", len(d.MAC), 12)
	}
	return nil
}

func (d Device) GetFirmwareType() FirmwareType {
	switch d.Type {
	case DeviceTypeUnknown:
		return FirmwareUnknown
	case DeviceTypeArtefact:
		return FirmwareArtefact
	case DeviceTypeSoul:
		return FirmwareSoul
	case DeviceTypeDefence:
		return FirmwareDefence
	default:
		return FirmwareUnknown
	}
}

func (dt *DeviceType) Scan(value interface{}) error { *dt = DeviceType(value.([]byte)); return nil }
func (dt DeviceType) Value() (driver.Value, error)  { return string(dt), nil }
