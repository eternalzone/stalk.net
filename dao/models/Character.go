package models

import (
	"database/sql"
	"time"
)

type Character struct {
	Id        uint         `gorm:"column:chr_id;primary_key"`
	Name      string       `gorm:"column:chr_name"`
	ImageId   uint         `gorm:"column:img_id"`
	Legend    string       `gorm:"column:chr_legend"`
	IsActive  bool         `gorm:"column:chr_is_active"`
	CreatedAt time.Time    `gorm:"column:chr_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:chr_updated_at"`

	Image   Image    `gorm:"foreignKey:img_id"`
	Members []Member `gorm:"many2many:characters_members;foreignKey:chr_id;joinForeignKey:chr_id;References:mbr_id;JoinReferences:mbr_id"`
}
