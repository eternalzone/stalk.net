package models

import (
	"database/sql"
	"database/sql/driver"
	"gorm.io/gorm"
	"time"
)

type MemberChangeType string

const (
	MCTUnknown       MemberChangeType = "unknown"
	MCTLegend        MemberChangeType = "legend"
	MCTLegendMaster  MemberChangeType = "legend_master"
	MCTPassportPhoto MemberChangeType = "passport_photo"
	MCTPhotos        MemberChangeType = "photos"
	MCTCallsign      MemberChangeType = "callsign"
	MCTSuit          MemberChangeType = "suit"
	MCTFraction      MemberChangeType = "fraction"
	MCTNewRequest    MemberChangeType = "new_request"
)

type MembersChangeLog struct {
	Id                uint             `gorm:"column:mcl_id;primary_key"`
	EditorUserId      uint             `gorm:"column:usr_id_editor"`
	TargetMemberId    uint             `gorm:"column:mbr_id_target"`
	Type              MemberChangeType `gorm:"column:mcl_type"`
	ProcessedByUserId sql.NullInt64    `gorm:"column:usr_id_processed_by"`
	PreviousVersion   sql.NullString   `gorm:"column:mcl_previous"`
	CurrentVersion    sql.NullString   `gorm:"column:mcl_current"`
	CreatedAt         time.Time        `gorm:"column:mcl_created_at"`
	UpdatedAt         sql.NullTime     `gorm:"column:mcl_updated_at"`

	Editor      User   `gorm:"foreignKey:usr_id_editor"`
	Target      Member `gorm:"foreignKey:mbr_id_target"`
	ProcessedBy *User  `gorm:"foreignKey:usr_id_processed_by"`

	IsNew bool `gorm:"-"`
}

func (mcl *MemberChangeType) Scan(value interface{}) error {
	*mcl = MemberChangeType(value.([]byte))
	return nil
}
func (mcl MemberChangeType) Value() (driver.Value, error) { return string(mcl), nil }

func (mcl *MembersChangeLog) AfterFind(tx *gorm.DB) (err error) {
	mcl.IsNew = !mcl.ProcessedByUserId.Valid

	return nil
}
