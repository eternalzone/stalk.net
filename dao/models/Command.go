package models

import (
	"database/sql"
	"time"
)

type Command struct {
	Id        uint          `gorm:"column:cmd_id;primary_key"`
	Desc      string        `gorm:"column:cmd_description"`
	Title     string        `gorm:"column:cmd_title"`
	ImageId   sql.NullInt64 `gorm:"column:img_id"`
	CreatedAt time.Time     `gorm:"column:cmd_created_at"`
	UpdatedAt sql.NullTime  `gorm:"column:cmd_updated_at"`

	Image Image `gorm:"foreignKey:img_id"`
}
