package models

import (
	"database/sql"
	"time"
)

type FractionsEvent struct {
	Id            uint         `gorm:"column:fev_id;primary_key"`
	FractionId    uint         `gorm:"column:frc_id"`
	EventId       uint         `gorm:"column:evt_id"`
	MembersMax    uint8        `gorm:"column:fev_max_members"`
	MembersTotal  uint8        `gorm:"column:fev_total_members"`
	RequestsTotal uint8        `gorm:"column:fev_total_requests"`
	IsActive      bool         `gorm:"column:fev_is_active"`
	CreatedAt     time.Time    `gorm:"column:mev_created_at"`
	UpdatedAt     sql.NullTime `gorm:"column:mev_updated_at"`

	Event Event `gorm:"foreignKey:evt_id"`
}
