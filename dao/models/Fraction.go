package models

import (
	"database/sql"
	"time"
)

type Fraction struct {
	Id        uint         `gorm:"column:frc_id;primary_key"`
	Desc      string       `gorm:"column:frc_description"`
	Title     string       `gorm:"column:frc_title"`
	IsActive  bool         `gorm:"column:frc_is_active"`
	Influence int64        `gorm:"column:frc_influence"`
	Money     int64        `gorm:"column:frc_money"`
	ImageId   uint         `gorm:"column:img_id"`
	DeputyId  uint         `gorm:"column:mbr_id_deputy"`
	LeaderId  uint         `gorm:"column:mbr_id_leader"`
	MasterId  uint         `gorm:"column:mbr_id_master"`
	CreatedAt time.Time    `gorm:"column:frc_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:frc_updated_at"`

	Image          Image          `gorm:"foreignKey:img_id"`
	FractionsEvent FractionsEvent //`gorm:"foreignKey:img_id"`
	//Deputy Member
	//Leader Member
}
