package models

type CharacterMember struct {
	Id          uint `gorm:"column:chm_id;primary_key"`
	CharacterId uint `gorm:"column:chr_id"`
	MemberId    uint `gorm:"column:mbr_id"`

	Character Character `gorm:"foreignKey:chr_id"`
	Member    Member    `gorm:"foreignKey:mbr_id"`
}
