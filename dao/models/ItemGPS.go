package models

type ItemGPS struct {
	Id       uint `gorm:"column:itg_id;primary_key"`
	ItemId   uint `gorm:"column:itm_id"`
	GPSId    uint `gorm:"column:gps_id"`
	IsActive bool `gorm:"column:itg_is_active"`

	Item Item `gorm:"foreignKey:itm_id"`
	GPS  GPS  `gorm:"foreignKey:gps_id"`
}

func (ItemGPS) TableName() string {
	return "items_gps"
}
