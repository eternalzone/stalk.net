package models

import (
	"database/sql"
	"database/sql/driver"
	"github.com/wedancedalot/decimal"
	"time"
)

type MemberEventPda string

const (
	PdaOwn      MemberEventPda = "own"
	PdaRent     MemberEventPda = "rent"
	PdaPurchase MemberEventPda = "purchase"
	PdaNone     MemberEventPda = "none"
)

type MemberEventShotLight string

const (
	ShotLightOwn      MemberEventShotLight = "own"
	ShotLightPurchase MemberEventShotLight = "purchase"
	ShotLightNone     MemberEventShotLight = "none"
)

type MemberEventConfirmation string
type MemberEventPaymentType string

const (
	ConfirmationPaid          MemberEventConfirmation = "paid"
	ConfirmationPartiallyPaid MemberEventConfirmation = "partially_paid"
	ConfirmationApproved      MemberEventConfirmation = "approved"
	ConfirmationCommented     MemberEventConfirmation = "commented"
	ConfirmationDeclined      MemberEventConfirmation = "declined"
	ConfirmationNone          MemberEventConfirmation = "none"
)

const (
	PaymentTypeCash      MemberEventPaymentType = "cash"
	PaymentTypeCombatant MemberEventPaymentType = "combatant"
	PaymentTypeFree      MemberEventPaymentType = "free"
	PaymentTypeCashless  MemberEventPaymentType = "cashless"
	PaymentTypeNone      MemberEventPaymentType = "none"
)

type MembersEvent struct {
	Id                uint                    `gorm:"column:mev_id;primary_key"`
	MemberId          uint                    `gorm:"column:mbr_id"`
	EventId           uint                    `gorm:"column:evt_id"`
	Pda               MemberEventPda          `gorm:"column:mev_pda"`
	ShotLight         MemberEventShotLight    `gorm:"column:mev_shot_light"`
	HasPassport       bool                    `gorm:"column:mev_has_passport"`
	LeaderCandidate   sql.NullString          `gorm:"column:mev_leader_candidate"`
	Confirmation      MemberEventConfirmation `gorm:"column:mev_confirmation"`
	PaymentType       MemberEventPaymentType  `gorm:"column:mev_payment_type"`
	IsLookOK          bool                    `gorm:"column:mev_is_look_ok"`
	IsLegendOK        bool                    `gorm:"column:mev_is_legend_ok"`
	Comment           sql.NullString          `gorm:"column:mev_comment"`
	ConfirmedAt       sql.NullTime            `gorm:"column:mev_confirmed_at"`
	AmountUAH         decimal.Decimal         `gorm:"column:mev_amount_uah"`
	DiscountAmountUAH decimal.Decimal         `gorm:"column:mev_discount_amount_uah"`
	AfterRegAmountUAH decimal.Decimal         `gorm:"column:mev_after_reg_amount_uah"`
	PayedAmountUAH    decimal.Decimal         `gorm:"column:mev_payed_amount_uah"`
	CreatedAt         time.Time               `gorm:"column:mev_created_at"`
	UpdatedAt         sql.NullTime            `gorm:"column:mev_updated_at"`

	Member Member `gorm:"foreignKey:mbr_id"`
	Event  Event  `gorm:"foreignKey:evt_id"`
}

type MembersEventInfo struct {
	Max      uint64
	Total    uint64
	Requests uint64
}

func (u *MemberEventPda) Scan(value interface{}) error {
	*u = MemberEventPda(value.([]byte))
	return nil
}
func (u MemberEventPda) Value() (driver.Value, error) { return string(u), nil }

func (u *MemberEventShotLight) Scan(value interface{}) error {
	*u = MemberEventShotLight(value.([]byte))
	return nil
}
func (u MemberEventShotLight) Value() (driver.Value, error) { return string(u), nil }

func (mec *MemberEventConfirmation) Scan(value interface{}) error {
	*mec = MemberEventConfirmation(value.([]byte))
	return nil
}
func (mec MemberEventConfirmation) Value() (driver.Value, error) { return string(mec), nil }

func (mec MemberEventConfirmation) IsConfirmed() bool {
	switch mec {
	case ConfirmationApproved,
		ConfirmationPartiallyPaid,
		ConfirmationPaid:
		return true
	default:
		return false
	}
}
