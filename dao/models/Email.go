package models

import (
	"database/sql"
	"database/sql/driver"
	"time"
)

type EmailStatus string
type EmailType string

const (
	EmailStatusCreated    EmailStatus = "created"
	EmailStatusSent       EmailStatus = "sent"
	EmailStatusError      EmailStatus = "error"
	EmailStatusProcessing EmailStatus = "processing"
)

const (
	EmailTypeAnnouncement EmailType = "announcement"
	EmailTypePM           EmailType = "pm"
	EmailTypeNotification EmailType = "notification"
	EmailTypeMailing      EmailType = "mailing"
)

type Email struct {
	Id       uint        `gorm:"column:eml_id;primary_key"`
	Title    string      `gorm:"column:eml_title"`
	From     string      `gorm:"column:eml_from"`
	To       string      `gorm:"column:eml_to"`
	UserIdTo uint        `gorm:"column:usr_id_to"`
	Body     string      `gorm:"column:eml_body"`
	BodyHtml string      `gorm:"column:eml_body_html"`
	Status   EmailStatus `gorm:"column:eml_status"`
	Type     EmailType   `gorm:"column:eml_type"`
	Notice   string      `gorm:"column:eml_notice"`

	CreatedAt time.Time    `gorm:"column:eml_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:eml_updated_at"`

	User User `gorm:"foreignKey:usr_id_to"`
}

func (u *EmailStatus) Scan(value interface{}) error { *u = EmailStatus(value.([]byte)); return nil }
func (u EmailStatus) Value() (driver.Value, error)  { return string(u), nil }

func (u *EmailType) Scan(value interface{}) error { *u = EmailType(value.([]byte)); return nil }
func (u EmailType) Value() (driver.Value, error)  { return string(u), nil }
