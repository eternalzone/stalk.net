package models

import (
	"database/sql"
	"fmt"
	"time"
)

type FirmwareTag struct {
	Id       uint   `gorm:"column:ftg_id;primary_key"`
	Title    string `gorm:"column:ftg_title"`
	Desc     string `gorm:"column:ftg_description"`
	IsActive bool   `gorm:"column:ftg_is_active"`

	CreatedAt time.Time    `gorm:"column:ftg_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:ftg_updated_at"`
}

func (ft FirmwareTag) Validate() error {
	if len(ft.Title) < 3 {
		return fmt.Errorf("wrong Title length %d, must be greater then %d", len(ft.Title), 2)
	}
	return nil
}
