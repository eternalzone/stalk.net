package models

type Country struct {
	Id   uint   `gorm:"column:cou_id;primary_key"`
	Name string `gorm:"column:cou_name"`
}

func (Country) TableName() string {
	return "countries"
}
