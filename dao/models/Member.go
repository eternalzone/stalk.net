package models

import (
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"gorm.io/gorm"
	"os"
	"time"
)

type MemberRank string

const (
	RankNovice      MemberRank = "novice"
	RankExperienced MemberRank = "experienced"
	RankVeteran     MemberRank = "veteran"
	RankMaster      MemberRank = "master"
	RankLegend      MemberRank = "legend"
)

const (
	CodeLength = 8
)

type Member struct {
	Id               uint           `gorm:"column:mbr_id;primary_key"`
	UserId           uint           `gorm:"column:usr_id"`
	Code             string         `gorm:"column:mbr_code"`
	CharacterId      sql.NullInt64  `gorm:"column:chr_id"`
	FractionId       uint           `gorm:"column:frc_id"`
	IsActive         bool           `gorm:"column:mbr_is_active"`
	Callsign         string         `gorm:"column:mbr_callsign"`
	PassportCode     sql.NullString `gorm:"column:mbr_passport_code"`
	Legend           sql.NullString `gorm:"column:mbr_legend"`
	LegendMaster     sql.NullString `gorm:"column:mbr_legend_master"`
	SuitId           uint           `gorm:"column:itm_id_suit"`
	Money            int64          `gorm:"column:mbr_money"`
	Experience       uint64         `gorm:"column:mbr_experience"`
	TotalExperience  uint64         `gorm:"column:mbr_total_experience"`
	Rank             MemberRank     `gorm:"column:mbr_rank"`
	Level            uint8          `gorm:"column:mbr_level"`
	PassportPhotoId  uint           `gorm:"column:img_id"`
	Data             string         `gorm:"column:mbr_data"`
	IsPassportFilled bool           `gorm:"column:mbr_is_passport_filled"`

	CreatedAt time.Time    `gorm:"column:mbr_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:mbr_updated_at"`

	User          User     `gorm:"foreignKey:usr_id"`
	Fraction      Fraction `gorm:"foreignKey:frc_id"`
	Suit          Item     `gorm:"foreignKey:itm_id_suit"`
	PassportPhoto Image    `gorm:"foreignKey:img_id"`
	Photos        []Image  `gorm:"many2many:members_images;foreignKey:mbr_id;joinForeignKey:mbr_id;References:img_id;JoinReferences:img_id"`

	PassportImages apimodels.PassportImages `gorm:"-"`
}

func (u *MemberRank) Scan(value interface{}) error { *u = MemberRank(value.([]byte)); return nil }
func (u MemberRank) Value() (driver.Value, error)  { return string(u), nil }

func (m Member) GetPassportImages() (pis apimodels.PassportImages, err error) {
	dir, err := os.Getwd()
	if err != nil {
		return pis, fmt.Errorf("cannot get current directory: %s", err.Error())
	}

	pis = apimodels.PassportImages{
		FrontImg:  fmt.Sprintf("%s%s/%s_front.png", ImageBaseURLPath, PassportImagesDir, m.Code),
		BackImg:   fmt.Sprintf("%s%s/%s_back.png", ImageBaseURLPath, PassportImagesDir, m.Code),
		QRCodeImg: fmt.Sprintf("%s%s/%s_qr.png", ImageBaseURLPath, PassportImagesDir, m.Code),

		FrontImgSys:  fmt.Sprintf("%s%s/%s/%s_front.png", dir, ImagesSysPathDir, PassportImagesDir, m.Code),
		BackImgSys:   fmt.Sprintf("%s%s/%s/%s_back.png", dir, ImagesSysPathDir, PassportImagesDir, m.Code),
		QRCodeImgSyS: fmt.Sprintf("%s%s/%s/%s_qr.png", dir, ImagesSysPathDir, PassportImagesDir, m.Code),
	}

	return pis, nil
}

func (m *Member) AfterFind(tx *gorm.DB) (err error) {
	pis, err := m.GetPassportImages()
	if err != nil {
		log.Errorf("cannot GetPassportImages: %s", err.Error())
	}

	if _, err := os.Stat(pis.FrontImgSys); os.IsNotExist(err) {
		//no such photo is exists
		pis.FrontImg = ""
	}

	if _, err := os.Stat(pis.BackImgSys); os.IsNotExist(err) {
		//no such photo is exists
		pis.BackImg = ""
	}

	if _, err := os.Stat(pis.QRCodeImgSyS); os.IsNotExist(err) {
		//no such photo is exists
		pis.QRCodeImg = ""
	}

	//TODO cleared to not to return to front. Do this in more proper way in the future (`json:"-"` didn't work ()
	pis.FrontImgSys = ""
	pis.BackImgSys = ""
	pis.QRCodeImgSyS = ""

	m.PassportImages = pis

	return nil
}
