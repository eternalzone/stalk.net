package models

type LocationsImage struct {
	Id         uint `gorm:"column:lim_id;primary_key"`
	LocationId uint `gorm:"column:loc_id"`
	ImageId    uint `gorm:"column:img_id"`

	Location Location `gorm:"foreignKey:loc_id"`
	Image    Image    `gorm:"foreignKey:img_id"`
}
