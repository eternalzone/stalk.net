package models

import (
	"database/sql"
	"github.com/wedancedalot/decimal"
	"time"
)

type Event struct {
	Id                     uint            `gorm:"column:evt_id;primary_key"`
	Desc                   string          `gorm:"column:evt_description"`
	Title                  string          `gorm:"column:evt_title"`
	ImageId                uint            `gorm:"column:img_id"`
	IsRegistrationOpened   bool            `gorm:"column:evt_is_reg_opened"`
	PriceUAH               decimal.Decimal `gorm:"column:evt_price_uah"`
	DiscountUntil          sql.NullTime    `gorm:"column:evt_discount_until"`
	DiscountPriceUAH       decimal.Decimal `gorm:"column:evt_discount_price_uah"`
	RegistrationUntil      sql.NullTime    `gorm:"column:evt_reg_until"`
	AfterRegClosedPriceUAH decimal.Decimal `gorm:"column:evt_price_after_reg"`
	DiscountVariants       sql.NullString  `gorm:"column:evt_discount_variants"`
	StartsAt               sql.NullTime    `gorm:"column:evt_starts_at"`
	EndsAt                 sql.NullTime    `gorm:"column:evt_ends_at"`
	CreatedAt              time.Time       `gorm:"column:evt_created_at"`
	UpdatedAt              sql.NullTime    `gorm:"column:evt_updated_at"`

	Image Image `gorm:"foreignKey:img_id"`

	MembersInfo MembersEventInfo `gorm:"-"`
}
