package models

type Region struct {
	Id        uint   `gorm:"column:reg_id;primary_key"`
	Name      string `gorm:"column:reg_name"`
	CountryId uint   `gorm:"column:cou_id"`

	Country Country `gorm:"foreignKey:cou_id"`
}
