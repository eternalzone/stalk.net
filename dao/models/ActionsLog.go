package models

import (
	"database/sql"
	"database/sql/driver"
	"time"
)

type (
	ActionLogType string
)

const (
	ActionLogTypeCreate     ActionLogType = "create"
	ActionLogTypeUpdate     ActionLogType = "update"
	ActionLogTypeDelete     ActionLogType = "delete"
	ActionLogTypeDeactivate ActionLogType = "deactivate"
	ActionLogTypeOther      ActionLogType = "other"
)

type ActionLog struct {
	Id           uint          `gorm:"column:alg_id;primary_key"`
	EntityName   string        `gorm:"column:alg_entity_name"`
	EntityId     uint          `gorm:"column:alg_entity_id"`
	Desc         string        `gorm:"column:alg_description"`
	FuncName     string        `gorm:"column:alg_func"`
	Type         ActionLogType `gorm:"column:alg_type"`
	TypeExtended string        `gorm:"column:alg_type_ext"`
	UserId       uint          `gorm:"column:usr_id"`
	Data         string        `gorm:"column:alg_data"`
	CreatedAt    time.Time     `gorm:"column:alg_created_at"`
	UpdatedAt    sql.NullTime  `gorm:"column:alg_updated_at"`

	User User `gorm:"foreignKey:usr_id"`
}

func (ActionLog) TableName() string {
	return "actions_log"
}

func (u *ActionLogType) Scan(value interface{}) error { *u = ActionLogType(value.([]byte)); return nil }
func (u ActionLogType) Value() (driver.Value, error)  { return string(u), nil }
