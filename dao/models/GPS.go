package models

import (
	"database/sql"
	"time"
)

type GPS struct {
	Id        uint            `gorm:"column:gps_id;primary_key"`
	Desc      sql.NullString  `gorm:"column:gps_description"`
	Accuracy  sql.NullFloat64 `gorm:"column:gps_accuracy"`
	Latitude  float64         `gorm:"column:gps_latitude"`
	Longitude float64         `gorm:"column:gps_longitude"`
	Speed     sql.NullFloat64 `gorm:"column:gps_speed"`
	IsActive  bool            `gorm:"column:gps_is_active"`
	CreatedAt time.Time       `gorm:"column:gps_created_at"`
	UpdatedAt sql.NullTime    `gorm:"column:gps_updated_at"`
}
