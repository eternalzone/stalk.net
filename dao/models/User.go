package models

import (
	"database/sql"
	"database/sql/driver"
	"github.com/satori/go.uuid"
	"time"
)

type UserGroup string

const (
	UserMember         UserGroup = "member"
	UserGameMaster     UserGroup = "gamemaster"
	UserFractionLeader UserGroup = "fraction_leader"
	UserAdmin          UserGroup = "admin"
)

type User struct {
	Id                  uint           `gorm:"column:usr_id;primary_key"`
	Uuid                uuid.UUID      `gorm:"column:usr_uuid"`
	Email               string         `gorm:"column:usr_email"`
	PassHash            string         `json:"-" gorm:"column:usr_pass_hash"`
	FirstName           string         `gorm:"column:usr_firstname"`
	LastName            string         `gorm:"column:usr_lastname"`
	FatherName          sql.NullString `gorm:"column:usr_fathername"`
	Phone               sql.NullString `gorm:"column:usr_phone"`
	Group               UserGroup      `gorm:"column:usr_group"`
	CityId              uint           `gorm:"column:cty_id"`
	CommandId           uint           `gorm:"column:cmd_id"`
	BornAt              sql.NullTime   `gorm:"column:usr_born_at"`
	Contraindications   sql.NullString `gorm:"column:usr_contraindications"`
	IsBanned            bool           `gorm:"column:usr_is_banned"`
	IsPassportGenerated bool           `gorm:"column:usr_is_passport_generated"`
	CreatedAt           time.Time      `gorm:"column:usr_created_at"`
	UpdatedAt           sql.NullTime   `gorm:"column:usr_updated_at"`

	City    City    `gorm:"foreignKey:cty_id"`
	Command Command `gorm:"foreignKey:cmd_id"`
}

func (u *UserGroup) Scan(value interface{}) error { *u = UserGroup(value.([]byte)); return nil }
func (u UserGroup) Value() (driver.Value, error)  { return string(u), nil }
