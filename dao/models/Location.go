package models

import (
	"database/sql"
	"database/sql/driver"
	"time"
)

type (
	LocationType string
	LocationZone string
)

const (
	LocationMain  LocationType = "main"
	LocationQuest LocationType = "quest"
	LocationOther LocationType = "other"
)

const (
	LocationZoneRed    LocationZone = "red"
	LocationZoneYellow LocationZone = "yellow"
	LocationZoneGreen  LocationZone = "green"
	LocationZoneBlue   LocationZone = "blue"
)

type Location struct {
	Id        uint          `gorm:"column:loc_id;primary_key"`
	Title     string        `gorm:"column:loc_title"`
	Desc      string        `gorm:"column:loc_description"`
	Type      LocationType  `gorm:"column:loc_type"`
	Zone      LocationZone  `gorm:"column:loc_zone"`
	GPSId     sql.NullInt64 `gorm:"column:gps_id"`
	IsActive  bool          `gorm:"column:loc_is_active"`
	CreatedAt time.Time     `gorm:"column:loc_created_at"`
	UpdatedAt sql.NullTime  `gorm:"column:loc_updated_at"`

	GPS GPS `gorm:"foreignKey:gps_id"`

	Photos []Image `gorm:"many2many:locations_images;foreignKey:loc_id;joinForeignKey:loc_id;References:img_id;JoinReferences:img_id"`
}

func (u *LocationType) Scan(value interface{}) error { *u = LocationType(value.([]byte)); return nil }
func (u LocationType) Value() (driver.Value, error)  { return string(u), nil }

func (u *LocationZone) Scan(value interface{}) error { *u = LocationZone(value.([]byte)); return nil }
func (u LocationZone) Value() (driver.Value, error)  { return string(u), nil }
