package models

type MembersImage struct {
	Id       uint `gorm:"column:mim_id;primary_key"`
	MemberId uint `gorm:"column:mbr_id"`
	ImageId  uint `gorm:"column:img_id"`

	Member Member `gorm:"foreignKey:mbr_id"`
	Image  Image  `gorm:"foreignKey:img_id"`
}
