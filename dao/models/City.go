package models

type City struct {
	Id        uint   `gorm:"column:cty_id;primary_key" json:"id"`
	Name      string `gorm:"column:cty_name" json:"name"`
	PhoneCode string `gorm:"column:cty_phone_code" json:"phone_code"`
	RegionId  uint   `gorm:"column:reg_id" json:"region_id"`

	Region Region `gorm:"foreignKey:reg_id" json:"region"`
}

func (City) TableName() string {
	return "cities"
}
