package models

import (
	"database/sql"
	"database/sql/driver"
	uuid "github.com/satori/go.uuid"
	"time"
)

type (
	KeyType string
)

const (
	KeyTypeCommon   KeyType = "common"
	KeyTypeExternal KeyType = "external"
	KeyTypeDevice   KeyType = "device"
	KeyTypeApp      KeyType = "application"
)

func (kt KeyType) IsValid() bool {
	switch kt {
	case KeyTypeCommon,
		KeyTypeExternal,
		KeyTypeDevice,
		KeyTypeApp:
		return true
	default:
		return false
	}
}

type APIKey struct {
	Id          uint          `gorm:"column:key_id;primary_key"`
	UUID        uuid.UUID     `gorm:"column:key_uuid"`
	Name        string        `gorm:"column:key_name"`
	Type        KeyType       `gorm:"column:key_type"`
	Description string        `gorm:"column:key_description"`
	Key         string        `gorm:"column:key_key"`
	IsActive    bool          `gorm:"column:key_is_active"`
	UserId      sql.NullInt64 `gorm:"column:usr_id"`

	ActiveTo  sql.NullTime `gorm:"column:key_active_to"`
	CreatedAt time.Time    `gorm:"column:key_created_at"`
	UpdatedAt sql.NullTime `gorm:"column:key_updated_at"`

	User *User `gorm:"foreignKey:usr_id"`
}

func (key APIKey) IsKeyTypeValidFor(kt KeyType) bool {
	if key.Type == KeyTypeCommon || key.Type == kt {
		return true
	}

	return false
}

func (key APIKey) IsValid() bool {
	if !key.IsActive {
		return false
	}

	if key.ActiveTo.Valid && key.ActiveTo.Time.Before(time.Now()) {
		return false
	}

	return true
}

func (kt *KeyType) Scan(value interface{}) error { *kt = KeyType(value.([]byte)); return nil }
func (kt KeyType) Value() (driver.Value, error)  { return string(kt), nil }
