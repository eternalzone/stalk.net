package dao

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
)

func (gd gormDAO) PutActionsLogItem(log *models.ActionLog, DBTX ...Transaction) error {
	DB := gd.db

	if len(DBTX) == 1 {
		DB = DBTX[0].DB //use transaction if provided
	}

	res := DB.Create(log)
	if res.Error != nil {
		return res.Error
	}

	return nil
}
