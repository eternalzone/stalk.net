package dao

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"errors"
	"fmt"
	"gorm.io/gorm"
)

func (gd gormDAO) GetActiveUserMember(userId uint) (*models.Member, error) {
	if userId == 0 {
		return nil, fmt.Errorf("GetActiveUserMember: bad userId is provided")
	}

	var member models.Member
	res := gd.db.Where("usr_id = ? AND mbr_is_active = 1", userId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if res.Error != nil {
		return nil, res.Error
	}

	return &member, nil
}
