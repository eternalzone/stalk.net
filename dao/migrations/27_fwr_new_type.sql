-- +migrate Up
alter table firmwares
	modify fwr_type enum ('unknown', 'artefact', 'soul', 'anomaly', 'defence') default 'unknown' not null;

ALTER TABLE `devices`
	modify dev_type enum ('unknown', 'artefact', 'soul', 'anomaly', 'defence') NOT NULL DEFAULT 'unknown';

-- +migrate Down