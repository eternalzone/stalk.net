-- +migrate Up
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `api_keys`
(
	`key_id`          INT UNSIGNED AUTO_INCREMENT                          NOT NULL,
	`key_uuid`        CHAR(36)                                             NOT NULL,
	`key_name`        VARCHAR(255)                                         NOT NULL,
	`key_type`        ENUM ('common', 'external', 'device', 'application') NOT NULL DEFAULT 'common',
	`key_key`         VARCHAR(255)                                         NOT NULL,
	`key_description` TEXT                                                 NULL DEFAULT NULL,
	`key_is_active`   BOOLEAN                                              NOT NULL DEFAULT FALSE,
	`usr_id`          INT UNSIGNED                                         NULL     DEFAULT NULL,
	`key_active_to`   TIMESTAMP                                            NULL     DEFAULT NULL,
	`key_created_at`  TIMESTAMP                                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`key_updated_at`  TIMESTAMP                                            NULL     DEFAULT NULL
		ON update CURRENT_TIMESTAMP,
	PRIMARY KEY (`key_id`),
	KEY `FK_api_keys_usr_id` (`usr_id`)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `api_keys`
	ADD CONSTRAINT `FK_api_keys_usr_id` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;

-- import base keys
INSERT INTO api_keys (key_id, key_uuid, key_name, key_type, key_key, key_description, key_is_active, usr_id, key_active_to, key_created_at, key_updated_at) VALUES (1, 'f26f1127-7c79-45a7-98fe-761b36c0ee49', 'VS Code Firmware Upload', 'common', 'U7hI0Bd3qzp89fnojtZG', 'service key', 1, 1, null, '2021-01-08 16:56:38', '2021-01-08 16:56:38');
INSERT INTO api_keys (key_id, key_uuid, key_name, key_type, key_key, key_description, key_is_active, usr_id, key_active_to, key_created_at, key_updated_at) VALUES (2, 'e45d8bd8-069b-4a3a-882e-0912c3eaf5da', 'Soul device base key', 'device', 'kmCiw5AvmtsVfI9YDJqe', 'service key', 1, 1, null, '2021-01-08 16:57:11', '2021-01-08 16:57:11');
INSERT INTO api_keys (key_id, key_uuid, key_name, key_type, key_key, key_description, key_is_active, usr_id, key_active_to, key_created_at, key_updated_at) VALUES (3, '190822e5-5c6d-4a6a-9fdd-5e880f7a5c58', 'Flutter application base key', 'application', 'px2R08UKHE7B1wCq1nPL', 'service key', 1, 1, null, '2021-01-08 16:57:29', '2021-01-08 16:57:29');

COMMIT;
-- +migrate Down
DROP TABLE `api_keys`;
