-- +migrate Up
START TRANSACTION;
ALTER TABLE `members_events` ADD `mev_has_passport` BOOLEAN DEFAULT FALSE AFTER `mev_shot_light`;

COMMIT;
-- +migrate Down
ALTER TABLE `members_events` DROP `mev_has_passport`;
