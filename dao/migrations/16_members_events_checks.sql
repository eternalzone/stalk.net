-- +migrate Up
START TRANSACTION;
ALTER TABLE `members_events` ADD `mev_is_look_ok` BOOLEAN DEFAULT FALSE NOT NULL AFTER `mev_confirmation`;
ALTER TABLE `members_events` ADD `mev_is_legend_ok` BOOLEAN DEFAULT FALSE NOT NULL AFTER `mev_is_look_ok`;

COMMIT;
-- +migrate Down
ALTER TABLE `members_events` DROP `mev_is_look_ok`;
ALTER TABLE `members_events` DROP `mev_is_legend_ok`;
