-- +migrate Up
START TRANSACTION;
CREATE TABLE IF NOT EXISTS `firmwares` (
  `fwr_id`          INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `fwr_version`     INT UNSIGNED                                           NOT NULL,
  `fwr_title`       VARCHAR(255)                                           NULL,
  `fwr_description` TEXT                                                   NULL,
  `fwr_type`        ENUM ('unknown', 'artefact', 'soul', 'anomaly')        NOT NULL                                DEFAULT 'unknown',
  `fwr_filename`    VARCHAR(255)                                           NOT NULL,
  `fwr_is_current`  BOOLEAN                                                                                        DEFAULT TRUE,
  `fwr_created_at`  TIMESTAMP                                              NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `fwr_updated_at`  TIMESTAMP                                              NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fwr_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

COMMIT;
-- +migrate Down
DROP TABLE `firmwares`;
