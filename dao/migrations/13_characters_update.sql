-- +migrate Up
START TRANSACTION;

ALTER TABLE `characters` ADD `img_id` INT UNSIGNED DEFAULT NULL NULL AFTER `chr_name`;

ALTER TABLE `characters`
  ADD CONSTRAINT `FK_characters_img_id` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

ALTER TABLE `characters` ADD `chr_is_active` BOOLEAN DEFAULT TRUE AFTER `chr_legend`;

CREATE TABLE IF NOT EXISTS `characters_members` (
  `chm_id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `chr_id` INT UNSIGNED DEFAULT NULL,
  `mbr_id` INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`chm_id`),
  KEY `FK_characters_members_character` (`chr_id`),
  KEY `FK_characters_members_member` (`mbr_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `characters_members`
  ADD CONSTRAINT `FK_characters_members_character` FOREIGN KEY (`chr_id`) REFERENCES `characters` (`chr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_characters_members_member` FOREIGN KEY (`mbr_id`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE;

COMMIT;
-- +migrate Down
ALTER TABLE `characters` DROP COLUMN `img_id`;
ALTER TABLE `characters` DROP COLUMN `chr_is_active`;
drop TABLE `characters_members`;