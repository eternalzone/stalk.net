-- +migrate Up
START TRANSACTION;
alter table `events`
	add `evt_reg_until` timestamp null after `evt_discount_price_uah`;

alter table events
	add `evt_price_after_reg` decimal(10,2) default 0.00 null after `evt_reg_until`;

alter table `members_events`
	add `mev_after_reg_amount_uah` decimal(10,2) default 0.00 null after `mev_discount_amount_uah`;

commit;
-- +migrate Down
ALTER TABLE `events` DROP `evt_reg_until`;
ALTER TABLE `events` DROP `evt_price_after_reg`;
ALTER TABLE `members_events` DROP `mev_after_reg_amount_uah`;