-- +migrate Up
START TRANSACTION;

ALTER TABLE `locations`	ADD `loc_zone` enum('red', 'yellow', 'green', 'blue') DEFAULT 'green' NOT NULL AFTER `loc_type`;

COMMIT;
-- +migrate Down
ALTER TABLE locations DROP COLUMN `loc_zone`;