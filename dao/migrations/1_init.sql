-- +migrate Up
CREATE TABLE IF NOT EXISTS `images` (
  `img_id`         INT UNSIGNED AUTO_INCREMENT               NOT NULL,
  `img_name`       VARCHAR(255)                              NOT NULL,
  `img_type`       ENUM ('photo', 'avatar', 'icon', 'other') NOT NULL                              DEFAULT 'other',
  `img_is_tmp`     BOOLEAN                                                                         DEFAULT TRUE,
  `img_created_at` TIMESTAMP                                 NOT NULL                              DEFAULT CURRENT_TIMESTAMP,
  `img_updated_at` TIMESTAMP                                 NULL                                  DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`img_id`),
  UNIQUE (`img_name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `countries` (
  `cou_id`   INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `cou_name` VARCHAR(255)                NOT NULL,
  PRIMARY KEY (`cou_id`),
  UNIQUE (`cou_name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `regions` (
  `reg_id`   INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `reg_name` VARCHAR(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cou_id`   INT UNSIGNED                            DEFAULT NULL,
  PRIMARY KEY (`reg_id`),
  UNIQUE (`reg_name`),
  KEY `fk_region_country_id` (`cou_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `cities` (
  `cty_id`         INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `cty_name`       VARCHAR(255) DEFAULT NULL,
  `cty_phone_code` VARCHAR(255) DEFAULT NULL,
  `reg_id`         INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`cty_id`),
  KEY `fk_city_region_id` (`reg_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `items` (
  `itm_id`          INT UNSIGNED AUTO_INCREMENT                              NOT NULL,
  `itm_code`        VARCHAR(255)                                                                                     DEFAULT NULL,
  `itm_title`       VARCHAR(255)                                             NOT NULL,
  `itm_description` LONGTEXT,
  `itm_type`        ENUM ('suit', 'artefact', 'quest', 'entourage', 'other') NOT NULL                                DEFAULT 'other',
  `img_id`          INT UNSIGNED                                                                                     DEFAULT NULL,
  `itm_is_virtual`  BOOLEAN                                                                                          DEFAULT FALSE,
  `itm_is_active`   BOOLEAN                                                                                          DEFAULT TRUE,
  `itm_created_at`  TIMESTAMP                                                NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `itm_updated_at`  TIMESTAMP                                                NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`itm_id`),
  UNIQUE (`itm_title`),
  KEY `FK_items_img` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `fractions` (
  `frc_id`          INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `frc_title`       VARCHAR(255)                NOT NULL,
  `frc_description` LONGTEXT,
  `frc_is_active`   BOOLEAN                              DEFAULT TRUE,
  `frc_influence`   INT                                  DEFAULT NULL,
  `frc_money`       INT                                  DEFAULT NULL,
  `img_id`          INT UNSIGNED                         DEFAULT NULL,
  `mbr_id_deputy`   INT UNSIGNED                         DEFAULT NULL,
  `mbr_id_leader`   INT UNSIGNED                         DEFAULT NULL,
  `mbr_id_master`   INT UNSIGNED                         DEFAULT NULL,
  `frc_created_at`  TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `frc_updated_at`  TIMESTAMP                   NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`frc_id`),
  UNIQUE (`frc_title`),
  KEY `FK_fractions_leader` (`mbr_id_leader`),
  KEY `FK_fractions_deputy_leader` (`mbr_id_deputy`),
  KEY `FK_fractions_master` (`mbr_id_master`),
  KEY `FK_fractions_img` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `commands` (
  `cmd_id`          INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `cmd_description` LONGTEXT,
  `cmd_title`       VARCHAR(255)                         DEFAULT NULL,
  `img_id`          INT UNSIGNED                         DEFAULT NULL,
  `cmd_created_at`  TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmd_updated_at`  TIMESTAMP                   NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cmd_id`),
  UNIQUE (`cmd_title`),
  KEY `FK_commands_img` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `events` (
  `evt_id`            INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `evt_description`   LONGTEXT                             DEFAULT NULL,
  `evt_title`         VARCHAR(255)                         DEFAULT NULL,
  `img_id`            INT UNSIGNED                         DEFAULT NULL,
  `evt_is_reg_opened` BOOLEAN                              DEFAULT TRUE,
  `evt_starts_at`     TIMESTAMP                   NULL,
  `evt_ends_at`       TIMESTAMP                   NULL,
  `evt_created_at`    TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `evt_updated_at`    TIMESTAMP                   NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`evt_id`),
  UNIQUE (`evt_title`),
  KEY `FK_events_img` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `fractions_events` (
  `fev_id`            INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `frc_id`            INT UNSIGNED                                                        DEFAULT NULL,
  `evt_id`            INT UNSIGNED                                                        DEFAULT NULL,
  `fev_max_members`   SMALLINT UNSIGNED DEFAULT 0,
  `fev_total_members` SMALLINT UNSIGNED DEFAULT 0,
  `fev_is_active`     BOOLEAN                                                             DEFAULT TRUE,
  `mev_created_at`    TIMESTAMP                   NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `mev_updated_at`    TIMESTAMP                   NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fev_id`),
  KEY `FK_fractions_event_frc` (`frc_id`),
  KEY `FK_fractions_event_evt` (`evt_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `usr_id`                INT UNSIGNED AUTO_INCREMENT                               NOT NULL,
  `usr_uuid`              CHAR(36)                                                  NOT NULL,
  `usr_email`             VARCHAR(255)                                              NOT NULL,
  `usr_pass_hash`         VARCHAR(255)                                              NOT NULL,
  `usr_firstname`         VARCHAR(255)                                              NOT NULL,
  `usr_lastname`          VARCHAR(255)                                              NOT NULL,
  `usr_fathername`        VARCHAR(255)                                              NULL                                    DEFAULT NULL,
  `usr_phone`             VARCHAR(36)                                               NULL                                    DEFAULT NULL,
  `usr_telegram_id`       INT UNSIGNED                                              NULL                                    DEFAULT NULL,
  `usr_group`             ENUM ('member', 'gamemaster', 'fraction_leader', 'admin') NOT NULL                                DEFAULT 'member',
  `cty_id`                INT UNSIGNED                                              NOT NULL,
  `cmd_id`                INT UNSIGNED                                              NULL                                    DEFAULT NULL,
  `usr_born_at`           TIMESTAMP                                                 NULL                                    DEFAULT NULL,
  `usr_contraindications` TEXT                                                      NULL                                    DEFAULT NULL,
  `usr_created_at`        TIMESTAMP                                                 NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `usr_updated_at`        TIMESTAMP                                                 NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`usr_id`),
  KEY `FK_users_city`    (`cty_id`),
  KEY `FK_users_command` (`cmd_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `characters` (
  `chr_id`         INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `chr_name`       VARCHAR(255)                NOT NULL,
  `chr_legend`     LONGTEXT                    NULL                                        DEFAULT NULL,
  `chr_created_at` TIMESTAMP                   NOT NULL                                    DEFAULT CURRENT_TIMESTAMP,
  `chr_updated_at` TIMESTAMP                   NULL                                        DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`chr_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `members` (
  `mbr_id`            INT UNSIGNED AUTO_INCREMENT                                   NOT NULL,
  `usr_id`            INT UNSIGNED                                                  NULL                                    DEFAULT NULL,
  `mbr_code`          VARCHAR(36)                                                   NOT NULL,
  `chr_id`            INT UNSIGNED                                                  NULL                                    DEFAULT NULL,
  `frc_id`            INT UNSIGNED                                                  NOT NULL,
  `mbr_is_active`     BOOLEAN                                                                                               DEFAULT FALSE,
  `mbr_callsign`      VARCHAR(255)                                                  NOT NULL,
  `mbr_passport_code` VARCHAR(255)                                                  NULL                                    DEFAULT NULL,
  `mbr_legend`        LONGTEXT                                                      NULL                                    DEFAULT NULL,
  `mbr_legend_master` LONGTEXT                                                      NULL                                    DEFAULT NULL,
  `itm_id_suit`       INT UNSIGNED                                                  NULL                                    DEFAULT NULL,
  `mbr_money`         INT                                                                                                   DEFAULT 0,
  `mbr_experience`    INT UNSIGNED                                                                                          DEFAULT 0,
  `mbr_rank`          ENUM ('novice', 'experienced', 'veteran', 'master', 'legend') NOT NULL                                DEFAULT 'novice',
  `mbr_level`         TINYINT                                                                                               DEFAULT 0,
  `img_id`            INT UNSIGNED                                                  NULL                                    DEFAULT NULL,
  `mbr_created_at`    TIMESTAMP                                                     NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `mbr_updated_at`    TIMESTAMP                                                     NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mbr_id`),
  UNIQUE (`mbr_code`),
  KEY `FK_members_fraction` (`frc_id`),
  KEY `FK_members_character` (`chr_id`),
  KEY `FK_members_suit` (`itm_id_suit`),
  KEY `FK_members_avatar` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `members_events` (
  `mev_id`                  INT UNSIGNED AUTO_INCREMENT                        NOT NULL,
  `mbr_id`                  INT UNSIGNED                                                                               DEFAULT NULL,
  `evt_id`                  INT UNSIGNED                                                                               DEFAULT NULL,
  `mev_pda`                 ENUM ('own', 'rent', 'purchase', 'none')           NOT NULL                                DEFAULT 'none',
  `mev_shot_light`          ENUM ('own', 'purchase', 'none')                   NOT NULL                                DEFAULT 'none',
  `mev_leader_candidate`    TEXT                                               NULL                                    DEFAULT NULL,
  `mev_confirmation`        ENUM ('approved', 'commented', 'declined', 'none') NOT NULL                                DEFAULT 'none',
  `mev_comment`             TEXT                                               NULL                                    DEFAULT NULL,
  `mev_confirmed_at`        TIMESTAMP                                          NULL,
  `mev_need_pay_amount_uah` DECIMAL(10, 2)                                     NULL                                    DEFAULT NULL,
  `mev_payed_amount_uah`    DECIMAL(10, 2)                                     NULL                                    DEFAULT NULL,
  `mev_created_at`          TIMESTAMP                                          NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `mev_updated_at`          TIMESTAMP                                          NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mev_id`),
  KEY `FK_members_event_mbr` (`mbr_id`),
  KEY `FK_members_event_evt` (`evt_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `members_images` (
  `mim_id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `mbr_id` INT UNSIGNED DEFAULT NULL,
  `img_id` INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`mim_id`),
  KEY `FK_members_images_member` (`mbr_id`),
  KEY `FK_members_images_image` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `regions`
  ADD CONSTRAINT `fk_region_country_id` FOREIGN KEY (`cou_id`) REFERENCES `countries` (`cou_id`)
  ON UPDATE CASCADE;

ALTER TABLE `cities`
  ADD CONSTRAINT `fk_city_region_id` FOREIGN KEY (`reg_id`) REFERENCES `regions` (`reg_id`)
  ON UPDATE CASCADE;

ALTER TABLE `items`
  ADD CONSTRAINT `FK_items_img` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

ALTER TABLE `events`
  ADD CONSTRAINT `FK_events_img` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

ALTER TABLE `fractions`
  ADD CONSTRAINT `FK_fractions_img` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_fractions_deputy_leader` FOREIGN KEY (`mbr_id_deputy`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_fractions_leader` FOREIGN KEY (`mbr_id_leader`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_fractions_master` FOREIGN KEY (`mbr_id_master`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE;

ALTER TABLE `fractions_events`
  ADD CONSTRAINT `FK_fractions_event_frc` FOREIGN KEY (`frc_id`) REFERENCES `fractions` (`frc_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_fractions_event_evt` FOREIGN KEY (`evt_id`) REFERENCES `events` (`evt_id`)
  ON UPDATE CASCADE;

ALTER TABLE `commands`
  ADD CONSTRAINT `FK_commands_img` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_city` FOREIGN KEY (`cty_id`) REFERENCES `cities` (`cty_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_users_command` FOREIGN KEY (`cmd_id`) REFERENCES `commands` (`cmd_id`)
  ON UPDATE CASCADE;

ALTER TABLE `members`
  ADD CONSTRAINT `FK_members_user` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_character` FOREIGN KEY (`chr_id`) REFERENCES `characters` (`chr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_fraction` FOREIGN KEY (`frc_id`) REFERENCES `fractions` (`frc_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_itm_id_suit` FOREIGN KEY (`itm_id_suit`) REFERENCES `items` (`itm_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_avatar` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

ALTER TABLE `members_events`
  ADD CONSTRAINT `FK_members_event_mbr` FOREIGN KEY (`mbr_id`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_event_evt` FOREIGN KEY (`evt_id`) REFERENCES `events` (`evt_id`)
  ON UPDATE CASCADE;

ALTER TABLE `members_images`
  ADD CONSTRAINT `FK_members_images_member` FOREIGN KEY (`mbr_id`) REFERENCES `members` (`mbr_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_members_images_image` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;