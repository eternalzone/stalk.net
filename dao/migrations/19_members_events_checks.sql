-- +migrate Up
START TRANSACTION;
ALTER TABLE `fractions_events` ADD `fev_total_requests` BOOLEAN DEFAULT FALSE NOT NULL AFTER `fev_total_members`;

COMMIT;
-- +migrate Down
ALTER TABLE `fractions_events` DROP `fev_total_requests`;
