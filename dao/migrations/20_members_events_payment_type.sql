-- +migrate Up
START TRANSACTION;
ALTER TABLE `members_events`
	ADD `mev_payment_type` enum('cash', 'combatant', 'free', 'cashless', 'none') DEFAULT 'cashless' NOT NULL AFTER `mev_confirmation`;

COMMIT;
-- +migrate Down
ALTER TABLE `members_events` DROP `mev_payment_type`;
