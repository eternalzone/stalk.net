-- +migrate Up
INSERT INTO images (img_name, img_type, img_is_tmp) VALUES
("static/press.png", "icon", 0);

INSERT INTO fractions (img_id, frc_title, frc_is_active, frc_description) VALUES
((SELECT img_id FROM images WHERE img_name = "static/press.png"), 'Пресса', true, 'Корреспонденты различных издательств, которые работают в ЧЗО, освещая непростую жизнь её обитателей.');

-- +migrate Down