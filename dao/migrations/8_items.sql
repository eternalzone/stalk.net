-- +migrate Up
START TRANSACTION;
ALTER TABLE `items` ADD `itm_owner` ENUM('own', 'personal', 'other') DEFAULT 'own' NULL AFTER `img_id`;
ALTER TABLE `items` ADD `itm_location` ENUM('storage', 'garage', 'home', 'other') DEFAULT 'storage' NULL AFTER `itm_owner`;
ALTER TABLE `items` ADD `itm_comment` TEXT DEFAULT NULL AFTER `itm_is_active`;

ALTER TABLE `items` ALTER COLUMN `itm_code` SET DEFAULT NULL;
CREATE UNIQUE INDEX `items_itm_code_uindex` ON `items` (`itm_code`);
DROP INDEX `itm_title` ON `items`;

ALTER TABLE `images` MODIFY `img_type` enum('photo', 'avatar', 'icon', 'item', 'other') NOT NULL DEFAULT 'other';

COMMIT;
-- +migrate Down
ALTER TABLE `items` DROP `itm_owner`;
ALTER TABLE `items` DROP `itm_location`;
ALTER TABLE `items` DROP `itm_comment`;
DROP INDEX `items_itm_code_uindex` ON `items`;
CREATE UNIQUE INDEX `items_itm_title_uindex` ON `items` (`itm_title`);
ALTER TABLE `images` MODIFY `img_type` enum('photo', 'avatar', 'icon', 'other') NOT NULL DEFAULT 'other';
