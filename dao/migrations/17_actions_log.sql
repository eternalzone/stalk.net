-- +migrate Up
START TRANSACTION;
create table if not exists `actions_log` (
  `alg_id`          INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `alg_entity_name` VARCHAR(255)                                           NOT NULL,
  `alg_entity_id`   INT UNSIGNED                                           NOT NULL,
  `alg_description` TEXT                                                   NULL,
  `alg_func`        VARCHAR(255)                                           NOT NULL,
  `alg_type`        ENUM ('create', 'update', 'delete', 'deactivate', 'other')  NOT NULL                         DEFAULT 'create',
  `alg_type_ext`    VARCHAR(255)                                           NOT NULL                               DEFAULT 'create',
  `usr_id`          INT UNSIGNED                                                                                  NOT NULL,
  `alg_data`        TEXT                                                   NULL,
  `alg_created_at`  TIMESTAMP                                              NOT NULL                               DEFAULT CURRENT_TIMESTAMP,
  `alg_updated_at`  TIMESTAMP                                              NULL                                   DEFAULT NULL
  ON update CURRENT_TIMESTAMP,
  PRIMARY KEY (`alg_id`),
  KEY `fk_actions_log_usr_id` (`usr_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

alter table `actions_log`
  ADD CONSTRAINT `fk_actions_log_usr_id` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`)
  ON update CASCADE;

commit;
-- +migrate Down
drop table `actions_log`;