-- +migrate Up
START TRANSACTION;
alter table members
	add `mbr_data` TEXT default NULL null after `img_id`;

alter table members
	add `mbr_is_passport_filled` BOOL default FALSE not null;

COMMIT;
-- +migrate Down
ALTER TABLE `members` DROP `mbr_data`;
ALTER TABLE `members` DROP `mbr_is_passport_filled`;
