-- +migrate Up
START TRANSACTION;
alter table members
	add `mbr_total_experience` INT UNSIGNED DEFAULT 0 after `mbr_experience`;

COMMIT;
-- +migrate Down
ALTER TABLE `members` DROP `mbr_total_experience`;