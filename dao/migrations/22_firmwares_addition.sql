-- +migrate Up
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `devices`
(
	`dev_id`         INT UNSIGNED AUTO_INCREMENT          NOT NULL,
	`dev_uuid`       CHAR(36)                             NOT NULL,
	`dev_mac`        VARCHAR(12)                                   DEFAULT NULL,
	`dev_type`       ENUM ('unknown', 'artefact', 'soul') NOT NULL DEFAULT 'unknown',
	`dev_sub_type`   SMALLINT                             NOT NULL DEFAULT 0,
	`dev_is_active`  BOOLEAN                              NOT NULL DEFAULT FALSE,
	`fwr_id`         INT UNSIGNED                         NULL     DEFAULT NULL,
	`dev_config`     JSON                                 NULL     DEFAULT NULL,
	`dev_active_at`  TIMESTAMP                            NULL     DEFAULT NULL,
	`dev_created_at` TIMESTAMP                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`dev_updated_at` TIMESTAMP                            NULL     DEFAULT NULL
		ON update CURRENT_TIMESTAMP,
	PRIMARY KEY (`dev_id`),
	KEY `FK_devices_fwr_id` (`fwr_id`)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `devices`
	ADD CONSTRAINT `FK_devices_fwr_id` FOREIGN KEY (`fwr_id`) REFERENCES `firmwares` (`fwr_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;

COMMIT;
-- +migrate Down
DROP TABLE `devices`;
