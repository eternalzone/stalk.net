-- +migrate Up
START TRANSACTION;
CREATE TABLE IF NOT EXISTS `emails` (
  `eml_id`         INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `eml_title`      VARCHAR(255)                                           NOT NULL,
  `eml_from`       VARCHAR(255)                                           NOT NULL,
  `eml_to`         VARCHAR(255)                                           NOT NULL,
  `usr_id_to`      INT UNSIGNED                                           NULL                                    DEFAULT NULL,
  `eml_body`       TEXT                                                   NOT NULL,
  `eml_body_html`  TEXT                                                   NULL                                    DEFAULT NULL,
  `eml_status`     ENUM ('created', 'sent', 'error', 'processing')        NOT NULL                                DEFAULT 'created',
  `eml_type`       ENUM ('announcement', 'pm', 'notification', 'mailing') NOT NULL                                DEFAULT 'notification',
  `eml_notice`     TEXT                                                   NULL                                    DEFAULT NULL,
  `eml_created_at` TIMESTAMP                                              NOT NULL                                DEFAULT CURRENT_TIMESTAMP,
  `eml_updated_at` TIMESTAMP                                              NULL                                    DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eml_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `emails`
  ADD CONSTRAINT `FK_emails_user_to` FOREIGN KEY (`usr_id_to`) REFERENCES `users` (`usr_id`)
  ON UPDATE CASCADE;

ALTER TABLE members_events MODIFY mev_confirmation enum('paid', 'partially_paid', 'approved', 'commented', 'declined', 'none') NOT NULL DEFAULT 'none';

COMMIT;
-- +migrate Down
DROP TABLE `emails`;
ALTER TABLE members_events MODIFY mev_confirmation enum('approved', 'commented', 'declined', 'none') NOT NULL DEFAULT 'none';
