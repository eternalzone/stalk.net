-- +migrate Up
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `firmware_tags`
(
	`ftg_id`          INT UNSIGNED AUTO_INCREMENT NOT NULL,
	`ftg_title`       VARCHAR(255) UNIQUE         NOT NULL,
	`ftg_description` TEXT                        NULL     DEFAULT NULL,
	`ftg_is_active`   BOOLEAN                     NOT NULL DEFAULT FALSE,
	`ftg_created_at`  TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`ftg_updated_at`  TIMESTAMP                   NULL     DEFAULT NULL
		ON update CURRENT_TIMESTAMP,
	PRIMARY KEY (`ftg_id`)
)
	ENGINE = InnoDB
	DEFAULT CHARSET = utf8mb4
	COLLATE = utf8mb4_unicode_ci;
CREATE UNIQUE INDEX `firmware_tags_ftg_title_uindex` ON `firmware_tags` (ftg_title);

-- tags for firmwares
ALTER TABLE `firmwares`
	ADD `ftg_id` INT UNSIGNED NULL DEFAULT NULL AFTER `fwr_type`;
ALTER TABLE `firmwares`
	ADD CONSTRAINT `FK_firmwares_tags` FOREIGN KEY (`ftg_id`) REFERENCES `firmware_tags` (`ftg_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX `firmwares_ftg_id_index` ON `firmwares` (`ftg_id`);

-- events for firmwares
ALTER TABLE `firmwares`
	ADD `evt_id` INT UNSIGNED NULL DEFAULT NULL AFTER `ftg_id`;
ALTER TABLE `firmwares`
	ADD CONSTRAINT `FK_firmwares_events` FOREIGN KEY (`evt_id`) REFERENCES `events` (`evt_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX `firmwares_evt_id_index` ON `firmwares` (`evt_id`);

-- tags for devices
ALTER TABLE `devices`
	ADD `ftg_id` INT UNSIGNED NULL DEFAULT NULL AFTER `fwr_id`;
ALTER TABLE `devices`
	ADD CONSTRAINT `FK_devices_firmwares_tags` FOREIGN KEY (`ftg_id`) REFERENCES `firmware_tags` (`ftg_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX `devices_ftg_id_index` ON `devices` (`ftg_id`);

-- members for devices
ALTER TABLE `devices`
	ADD `mbr_id` INT UNSIGNED NULL DEFAULT NULL AFTER `ftg_id`;
ALTER TABLE `devices`
	ADD CONSTRAINT `FK_devices_members` FOREIGN KEY (`mbr_id`) REFERENCES `members` (`mbr_id`)
		ON UPDATE CASCADE ON DELETE CASCADE;
CREATE INDEX `devices_mbr_id_index` ON `devices` (`mbr_id`);

COMMIT;
-- +migrate Down
START TRANSACTION;

DROP INDEX `devices_mbr_id_index` ON `devices`;
ALTER TABLE `devices`
	DROP FOREIGN KEY `FK_devices_members`;
ALTER TABLE `devices`
	DROP COLUMN `fwr_id`;

DROP INDEX `devices_ftg_id_index` ON `devices`;
ALTER TABLE `devices`
	DROP FOREIGN KEY `FK_devices_firmwares_tags`;
ALTER TABLE `devices`
	DROP COLUMN `ftg_id`;

DROP INDEX `firmwares_evt_id_index` ON `firmwares`;
ALTER TABLE `firmwares`
	DROP FOREIGN KEY `FK_firmwares_events`;
ALTER TABLE `firmwares`
	DROP COLUMN `evt_id`;

DROP INDEX `firmwares_ftg_id_index` ON `firmwares`;
ALTER TABLE `firmwares`
	DROP FOREIGN KEY `FK_firmwares_tags`;
ALTER TABLE `firmwares`
	DROP COLUMN `ftg_id`;

DROP TABLE `firmware_tags`;

COMMIT;

