-- +migrate Up
START TRANSACTION;
ALTER TABLE `items` ADD `itm_data` TEXT DEFAULT NULL AFTER `itm_is_active`;

ALTER TABLE `items` ADD `itm_created_by_usr_id` INT UNSIGNED DEFAULT NULL AFTER `itm_data`;
ALTER TABLE `items`
  ADD CONSTRAINT `FK_items_itm_created_by_usr_id` FOREIGN KEY (`itm_created_by_usr_id`) REFERENCES `users` (`usr_id`)
  ON UPDATE CASCADE;

ALTER TABLE `items` CHANGE `itm_location` `itm_place` ENUM('storage', 'garage', 'home', 'other') DEFAULT 'storage' NULL;

ALTER TABLE `items` ADD `loc_id` INT UNSIGNED DEFAULT NULL AFTER `itm_created_by_usr_id`;
ALTER TABLE `items`
  ADD CONSTRAINT `FK_items_loc_id` FOREIGN KEY (`loc_id`) REFERENCES `locations` (`loc_id`)
  ON UPDATE CASCADE ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `items_gps` (
  `itg_id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `itm_id` INT UNSIGNED DEFAULT NULL,
  `gps_id` INT UNSIGNED DEFAULT NULL,
  `itg_is_active` BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (`itg_id`),
  KEY `FK_items_gps_itm_id` (`itm_id`),
  KEY `FK_items_gps_gps_id` (`gps_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `items_gps`
  ADD CONSTRAINT `FK_items_gps_item` FOREIGN KEY (`itm_id`) REFERENCES `items` (`itm_id`)
  ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_items_gps_gps` FOREIGN KEY (`gps_id`) REFERENCES `gps` (`gps_id`)
  ON UPDATE CASCADE ON DELETE CASCADE;

COMMIT;
-- +migrate Down
ALTER TABLE `items` DROP `itm_data`;
DROP INDEX `FK_items_itm_created_by_usr_id` ON `items`;
ALTER TABLE `items` DROP `itm_created_by_usr_id`;
ALTER TABLE `items` CHANGE `itm_place` `itm_location` ENUM('storage', 'garage', 'home', 'other') DEFAULT 'storage' NULL;
DROP INDEX `FK_items_loc_id` ON `items`;
ALTER TABLE `items` DROP `loc_id`;
DROP TABLE `items_gps`;
