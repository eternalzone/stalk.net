-- +migrate Up
START TRANSACTION;
ALTER TABLE `users` ADD `usr_is_banned` BOOLEAN DEFAULT FALSE NOT NULL AFTER `usr_contraindications`;
ALTER TABLE `users` ADD `usr_is_passport_generated` BOOLEAN DEFAULT FALSE NULL AFTER `usr_is_banned`;

COMMIT;
-- +migrate Down
ALTER TABLE `users` DROP `usr_is_banned`;
ALTER TABLE `users` DROP `usr_is_passport_generated`;