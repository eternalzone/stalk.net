-- +migrate Up
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `members_change_logs`
(
    `mcl_id`              INT UNSIGNED AUTO_INCREMENT                                                                                        NOT NULL,
    `usr_id_editor`       INT UNSIGNED                                                                                                       NOT NULL,
    `mbr_id_target`       INT UNSIGNED                                                                                                       NOT NULL,
    `mcl_type`            ENUM ('unknown', 'legend', 'legend_master', 'passport_photo', 'photos', 'callsign', 'suit', 'fraction', 'new_request') NOT NULL DEFAULT 'unknown',
    `usr_id_processed_by` INT UNSIGNED                                                                                                       NULL     DEFAULT NULL,
    `mcl_previous`        TEXT                                                                                                               NULL     DEFAULT NULL,
    `mcl_current`         TEXT                                                                                                               NULL     DEFAULT NULL,
    `mcl_created_at`      TIMESTAMP                                                                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `mcl_updated_at`      TIMESTAMP                                                                                                          NULL     DEFAULT NULL
        ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`mcl_id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `members_change_logs`
    ADD CONSTRAINT `fk_mcl_usr_id_editor` FOREIGN KEY (`usr_id_editor`) REFERENCES `users` (`usr_id`)
        ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `members_change_logs`
    ADD CONSTRAINT `fk_mcl_mbr_id_target` FOREIGN KEY (`mbr_id_target`) REFERENCES `members` (`mbr_id`)
        ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `members_change_logs`
    ADD CONSTRAINT `fk_mcl_usr_id_processed_by` FOREIGN KEY (`usr_id_processed_by`) REFERENCES `users` (`usr_id`)
        ON UPDATE CASCADE ON DELETE CASCADE;

COMMIT;
-- +migrate Down
START TRANSACTION;

DROP TABLE `members_change_logs`;

COMMIT;

