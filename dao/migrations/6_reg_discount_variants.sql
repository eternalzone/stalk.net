-- +migrate Up
START TRANSACTION;

ALTER TABLE `events` ADD `evt_discount_variants` TEXT DEFAULT NULL NULL AFTER `evt_discount_price_uah`;
UPDATE `events` SET `evt_discount_variants` = "[{\"uah\":945.00, \"usd\": 35}]" WHERE `evt_id` = 1;

COMMIT;

-- +migrate Down
ALTER TABLE `events` DROP `evt_discount_variants`;
