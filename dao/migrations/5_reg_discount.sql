-- +migrate Up
START TRANSACTION;

ALTER TABLE `events` ADD `evt_price_uah` decimal(10,2) DEFAULT 0 NULL AFTER `evt_is_reg_opened`;
UPDATE `events` SET `evt_price_uah` = 1350.00 WHERE `evt_id` = 1;

ALTER TABLE `events` ADD `evt_discount_until` timestamp NULL AFTER `evt_price_uah`;
UPDATE `events` SET `evt_discount_until` = '2019-04-21 00:00:00' WHERE `evt_id` = 1;

ALTER TABLE `events` ADD `evt_discount_price_uah` decimal(10,2) DEFAULT 0 NULL AFTER `evt_discount_until`;
UPDATE `events` SET `evt_discount_price_uah` = 1080.00 WHERE `evt_id` = 1;

ALTER TABLE `members_events` CHANGE `mev_need_pay_amount_uah` `mev_amount_uah` decimal(10,2) DEFAULT 0;
ALTER TABLE `members_events` ADD `mev_discount_amount_uah` decimal(10,2) DEFAULT 0 NULL AFTER `mev_amount_uah`;
COMMIT;

-- +migrate Down
START TRANSACTION;
ALTER TABLE `events` DROP `evt_price_uah`;
ALTER TABLE `events` DROP `evt_discount_until`;
ALTER TABLE `events` DROP `evt_discount_price_uah`;

ALTER TABLE `members_events` CHANGE `mev_amount_uah` `mev_need_pay_amount_uah` decimal(10,2);
ALTER TABLE `events` DROP `mev_discount_amount_uah`;
COMMIT;