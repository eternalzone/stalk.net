-- +migrate Up
START TRANSACTION;
create table if not exists `gps` (
  `gps_id`          INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `gps_description` TEXT                                                   NULL,
  `gps_accuracy`    FLOAT                                                                                         DEFAULT NULL,
  `gps_latitude`    FLOAT                                                                                         DEFAULT NULL,
  `gps_longitude`   FLOAT                                                                                         DEFAULT NULL,
  `gps_speed`       FLOAT                                                                                         DEFAULT NULL,
  `gps_is_active`   BOOLEAN                                                                                       DEFAULT TRUE,
  `gps_created_at`  TIMESTAMP                                              NOT NULL                               DEFAULT CURRENT_TIMESTAMP,
  `gps_updated_at`  TIMESTAMP                                              NULL                                   DEFAULT NULL
  ON update CURRENT_TIMESTAMP,
  PRIMARY KEY (`gps_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

create table if not exists `locations` (
  `loc_id`          INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `loc_title`       VARCHAR(255)                                           NOT NULL,
  `loc_description` TEXT                                                   NULL,
  `loc_type`        ENUM ('main', 'quest', 'other')                        NOT NULL                               DEFAULT 'main',
  `gps_id`          INT UNSIGNED                                                                                  DEFAULT NULL,
  `loc_is_active`   BOOLEAN                                                                                       DEFAULT TRUE,
  `loc_created_at`  TIMESTAMP                                              NOT NULL                               DEFAULT CURRENT_TIMESTAMP,
  `loc_updated_at`  TIMESTAMP                                              NULL                                   DEFAULT NULL
  ON update CURRENT_TIMESTAMP,
  PRIMARY KEY (`loc_id`),
  UNIQUE (`loc_title`),
  KEY `fk_location_gps_id` (`gps_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `locations`
  ADD CONSTRAINT `FK_locations_gps` FOREIGN KEY (`gps_id`) REFERENCES `gps` (`gps_id`)
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `locations_images` (
  `lim_id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `loc_id` INT UNSIGNED DEFAULT NULL,
  `img_id` INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`lim_id`),
  KEY `FK_locations_images_location` (`loc_id`),
  KEY `FK_locations_images_image` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `locations_images`
  ADD CONSTRAINT `FK_locations_images_location` FOREIGN KEY (`loc_id`) REFERENCES `locations` (`loc_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_locations_images_image` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

create table if not exists `positions` (
  `pos_id`          INT UNSIGNED AUTO_INCREMENT                            NOT NULL,
  `pos_title`       VARCHAR(255)                                           NOT NULL,
  `pos_description` TEXT                                                   NULL,
  `loc_id`          INT UNSIGNED                                           NULL                                   DEFAULT NULL,
  `img_id`          INT UNSIGNED                                           NULL                                   DEFAULT NULL,
  `pos_is_active`   BOOLEAN                                                                                       DEFAULT TRUE,
  `pos_created_at`  TIMESTAMP                                              NOT NULL                               DEFAULT CURRENT_TIMESTAMP,
  `pos_updated_at`  TIMESTAMP                                              NULL                                   DEFAULT NULL
  ON update CURRENT_TIMESTAMP,
  PRIMARY KEY (`pos_id`),
  UNIQUE (`pos_title`),
  KEY `fk_location_loc_id` (`loc_id`),
  KEY `fk_location_img_id` (`img_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `positions`
  ADD CONSTRAINT `FK_positions_location` FOREIGN KEY (`loc_id`) REFERENCES `locations` (`loc_id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_positions_image` FOREIGN KEY (`img_id`) REFERENCES `images` (`img_id`)
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `positions_members` (
  `pom_id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `pos_id` INT UNSIGNED DEFAULT NULL,
  `mbr_id` INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`pom_id`),
  KEY `FK_positions_members_position` (`pos_id`),
  KEY `FK_positions_members_member` (`mbr_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `positions_members`
ADD CONSTRAINT `FK_positions_members_position` FOREIGN KEY (`pos_id`) REFERENCES `positions` (`pos_id`)
ON UPDATE CASCADE,
ADD CONSTRAINT `FK_positions_members_member` FOREIGN KEY (`mbr_id`) REFERENCES `members` (`mbr_id`)
ON UPDATE CASCADE;

commit;
-- +migrate Down
drop TABLE `gps`;
drop TABLE `locations`;
drop TABLE `locations_images`;
drop TABLE `positions`;
drop TABLE `positions_members`;