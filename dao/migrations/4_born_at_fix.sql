-- +migrate Up
ALTER TABLE users MODIFY usr_born_at DATETIME;

-- +migrate Down
ALTER TABLE users MODIFY usr_born_at TIMESTAMP;