package dao

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"errors"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"time"
)

func (gd gormDAO) GetActiveRegistrationEvent() (*models.Event, error) {
	var ev models.Event
	res := gd.db.Where(&models.Event{IsRegistrationOpened: true}).Where("evt_reg_until is NULL OR evt_reg_until >= ?", time.Now()).First(&ev)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		logrus.Warn("no active event found")
		return nil, nil
	}
	if res.Error != nil {
		return nil, res.Error
	}

	return &ev, nil
}

func (gd gormDAO) GetLastEvent() (*models.Event, error) {
	var ev models.Event
	res := gd.db.Model(&models.Event{}).Last(&ev)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if res.Error != nil {
		return nil, res.Error
	}

	if ev.RegistrationUntil.Valid && ev.RegistrationUntil.Time.Before(time.Now()) {
		ev.IsRegistrationOpened = false
	}

	return &ev, nil
}
