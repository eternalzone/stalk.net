package dao

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"gorm.io/gorm"
	"net/http"
)

type IDao interface {
	GetActiveRegistrationEvent() (*models.Event, error)
	GetLastEvent() (*models.Event, error)
	GetLatestFirmware(r *http.Request, fwType models.FirmwareType, params ...models.FirmwareQueryParams) (*models.Firmware, error)
	GetActiveUserMember(userId uint) (*models.Member, error)

	PutActionsLogItem(log *models.ActionLog, DBTX ...Transaction) error
}

type gormDAO struct {
	config conf.Config
	db     *gorm.DB
}

func NewGormDao(cfg conf.Config, m *gorm.DB) IDao {
	return gormDAO{
		config: cfg,
		db:     m,
	}
}

type Transaction struct {
	//once     sync.Once
	finished bool
	DB       *gorm.DB
}

func (t *Transaction) Commit() (err error) {
	if !t.finished {
		err = t.DB.Commit().Error
		t.finished = true
	}

	return err
}

func (t *Transaction) Rollback() {
	if !t.finished {
		t.DB.Rollback()
		t.finished = true
	}
}

func NewTransaction(db *gorm.DB) Transaction {
	return Transaction{DB: db.Begin()}
}
