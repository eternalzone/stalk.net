package dao

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"errors"
	"gorm.io/gorm"
	"net/http"
	"strings"
)

func (gd gormDAO) GetLatestFirmware(r *http.Request, fwType models.FirmwareType, params ...models.FirmwareQueryParams) (*models.Firmware, error) {
	var fw models.Firmware
	q := gd.db.Where("fwr_type = ?", fwType) //just dummy cup

	var (
		eventsList    []uint
		firmwaresList []uint
		tagsList      []uint
		tagsTitleList []string

		isParamsAddedToQuery = false
	)
	log.Debugf("params = %+v", params)
	if len(params) > 0 {
		for i := range params {
			if params[i].EventId > 0 {
				eventsList = append(eventsList, params[i].EventId)
			}
			if params[i].FirmwareId > 0 {
				firmwaresList = append(firmwaresList, params[i].FirmwareId)
			}
			if params[i].FirmwareTagId > 0 {
				tagsList = append(tagsList, params[i].FirmwareTagId)
			}
			if !strings.EqualFold(strings.TrimSpace(params[i].FirmwareTagTitle), "") {
				tagsTitleList = append(tagsTitleList, strings.TrimSpace(params[i].FirmwareTagTitle))
			}
		}
		if len(eventsList) > 0 { //add events query
			q = q.Where("evt_id IN (?)", eventsList)
			isParamsAddedToQuery = true
		}
		if len(firmwaresList) > 0 { //add only selected firmwares query
			q = q.Where("fwr_id IN (?)", firmwaresList)
			isParamsAddedToQuery = true
		}
		if len(tagsList) > 0 { //add tags ids query
			q = q.Where("ftg_id IN (?)", tagsList)
			isParamsAddedToQuery = true
		}
		if len(tagsTitleList) > 0 { //add tags query
			q = q.Joins("JOIN firmware_tags ON firmware_tags.ftg_id = firmwares.ftg_id").Where("firmware_tags.ftg_title IN (?)", tagsTitleList)
			isParamsAddedToQuery = true
		}
	}
	if len(params) == 0 || !isParamsAddedToQuery {
		q = q.Where("fwr_is_current = ?", 1) //using is_current flag only by default
	}

	res := q.Last(&fw)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if res.Error != nil {
		return nil, res.Error
	}

	fw.GenerateFirmwareFilePath(r)

	return &fw, nil
}
