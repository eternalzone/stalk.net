default: list

list:
	@sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

run:
	go build && ./stalk.net

build-js:
	cd static && npm i && npm run build && cd ..

dev-js:
	cd static && npm i && npm run dev && cd ..

build-all:
	@echo '> Building go (backend) part...'
	go build
	@echo '> Building js (frontend) part...'
	make build-js

no_targets__:
	echo
