package daemons

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"encoding/hex"
	"errors"
	"gorm.io/gorm"
	"time"
)

func (d *Daemons) ChangeMembersData(maxId uint64) (uint64, error) {
	log.Debugf("Running ChangeMembersData()...")
	defer func(start time.Time, name string) {
		elapsed := time.Since(start)
		log.Debugf("%s took %s", name, elapsed)
	}(time.Now(), "ChangeMembersData()")

	DBTX := d.db.Begin()

	var mbrs []models.Member

	res := DBTX.Where("mbr_id <= ?", maxId).Find(&mbrs)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(mbrs) < 1 {
		return 0, nil // nothing to do
	}
	if res.Error != nil {
		return 0, res.Error
	}

	var err error
	var changedMembers uint64 = 0
	for _, mbr := range mbrs {
		_, err := hex.DecodeString(mbr.Code)
		if err == nil { //can parse as hex
			continue //do nothing
		}

		log.Debugf("Member %d", mbr.Id)
		mbr.Code = helpers.RandHexString(models.CodeLength)

		res = DBTX.Save(&mbr)
		if res.Error != nil {
			log.Warnf("Cannot update member id %d to set code %s: %s", mbr.Id, mbr.Code, res.Error.Error())
			continue
		}

		changedMembers++
	}

	res = DBTX.Commit()
	if res.Error != nil {
		return changedMembers, err
	}

	if changedMembers > 0 {
		log.Infof("Member changed %d", changedMembers)
	}

	return changedMembers, nil
}
