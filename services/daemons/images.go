package daemons

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"errors"
	"gorm.io/gorm"
	"os"
	"time"
)

func (d *Daemons) CleanerExpiredTempImages(timeoutInSec int64) {
	log.Info("Starting cleaner for temp images...")
	go func() {
		for {
			deletedImagesCount, err := d.deleteExpiredTempImages()
			if err != nil {
				log.Error(err.Error())
			}
			log.Debugf("%d temp images has been deleted", deletedImagesCount)
			time.Sleep(time.Duration(timeoutInSec) * time.Second)
		}
	}()
}

func (d *Daemons) deleteExpiredTempImages() (uint64, error) {
	log.Debugf("Running deleteExpiredTempImages()...")
	defer func(start time.Time, name string) {
		elapsed := time.Since(start)
		log.Debugf("%s took %s", name, elapsed)
	}(time.Now(), "deleteExpiredTempImages()")

	DBTX := d.db.Begin()

	var imgs []models.Image

	res := DBTX.Where("img_is_tmp = ? AND img_created_at < ? AND img_type in (?)",
		true,
		time.Now().Add(time.Second*-1*conf.TtlTempImages),
		[]string{string(models.ImageAvatar), string(models.ImagePhoto)},
	).Find(&imgs)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(imgs) < 1 {
		return 0, nil // nothing to do
	}
	if res.Error != nil {
		return 0, res.Error
	}

	var err error
	var deletedImages uint64 = 0
	for _, img := range imgs {
		log.Debugf("IMG %s", img.Name)
		filePath := models.ImageBasePath + "/" + img.Name
		if _, err := os.Stat(filePath); err == nil { //if file exist
			err = os.Remove(filePath)
			if err != nil {
				log.Warnf("Cannot delete file %s: %s", img.Name, err.Error())
				continue
			}
		}

		res = DBTX.Delete(&img)
		if res.Error != nil {
			log.Warnf("Cannot delete file in DB %s: %s", img.Name, res.Error.Error())
			continue
		}

		deletedImages++
	}

	res = DBTX.Commit()
	if res.Error != nil {
		return deletedImages, err
	}

	return deletedImages, nil
}
