package daemons

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"context"
	"gorm.io/gorm"
)

type Daemons struct {
	config conf.Config
	db     *gorm.DB
	mail   mailer.IMailService
}

func NewDaemons(cfg conf.Config, d *gorm.DB, m mailer.IMailService) Daemons {
	return Daemons{
		config: cfg,
		db:     d,
		mail:   m,
	}
}

func (d Daemons) GracefulStop(ctx context.Context) error {
	//TODO realise this
	return nil
}

// Title returns the title.
func (d Daemons) Title() string {
	return "Daemons"
}

// Run subscribes to the queue.
func (d Daemons) Run() {
	d.CleanerExpiredTempImages(conf.DelayCleanerExpiredTempImages)
	d.Mailer(conf.DelayRunMailerResender)

	//d.ChangeMembersData(57)
}
