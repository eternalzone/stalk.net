package daemons

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"time"
)

func (d *Daemons) Mailer(timeoutInSec int64) {
	log.Info("Starting mailer re-sender...")
	go func() {
		for {
			err := d.sendmail()
			if err != nil {
				log.Error(err.Error())
			}
			time.Sleep(time.Duration(timeoutInSec) * time.Second)
		}
	}()
}

func (d *Daemons) sendmail() error {
	defer func(start time.Time, name string) {
		elapsed := time.Since(start)
		log.Debugf("%s took %s", name, elapsed)
	}(time.Now(), "sendmail()")

	DBTX := d.db.Begin()

	var emails []models.Email

	res := DBTX.Where("eml_status in (?)", []string{string(models.EmailStatusError)}).Find(&emails)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(emails) < 1 {
		return nil // nothing to do
	}
	if res.Error != nil {
		return res.Error
	}

	var err error
	for _, email := range emails {
		log.Debugf("sending EMAIL %d to %s", email.Id, email.To)

		email.Status = models.EmailStatusSent

		err = d.mail.Send(email.To, email.Title, email.Body, email.BodyHtml)
		if err != nil {
			email.Status = models.EmailStatusError
			email.Notice = fmt.Sprintf("error: %s", err.Error())
			log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		}

		res = DBTX.Save(&email)
		if res.Error != nil {
			log.Errorf("Cannot save email %d to DB: %s", email.Id, res.Error.Error())
			continue
		}
	}

	res = DBTX.Commit()
	if res.Error != nil {
		return err
	}

	return nil
}
