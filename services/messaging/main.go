package messaging

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/log"
	"context"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gorm.io/gorm"
)

type Messaging struct {
	config     conf.Config
	db         *gorm.DB
	bot        *tgbotapi.BotAPI
	updateConf tgbotapi.UpdateConfig
	updates    tgbotapi.UpdatesChannel
}

func NewMessaging(cfg conf.Config, d *gorm.DB) (m Messaging, err error) {
	m = Messaging{
		config: cfg,
		db:     d,
	}
	if !m.config.Telegram.IsEnabled { //do nothing
		return m, nil
	}

	m.bot, err = tgbotapi.NewBotAPI(m.config.Telegram.BotToken)
	if err != nil {
		return m, err
	}

	if m.config.LogLevel == log.DebugLevel {
		m.bot.Debug = true
	}

	log.Infof("Telegram bot authorized on account %s", m.bot.Self.UserName)

	m.updateConf = tgbotapi.NewUpdate(0)
	m.updateConf.Timeout = 60

	m.updates, err = m.bot.GetUpdatesChan(m.updateConf)
	if err != nil {
		return m, err
	}

	return m, nil
}

func (m Messaging) GracefulStop(ctx context.Context) error {
	//TODO realise this
	return nil
}

// Title returns the title.
func (m Messaging) Title() string {
	return "Messaging"
}

func (m Messaging) Run() {
	if !m.config.Telegram.IsEnabled { //do nothing
		return
	}

	if m.bot == nil {
		log.Fatal("Bot is not init properly. Run Init() first")
	}

	//msg := tgbotapi.NewMessage(309010681, "Hello, Stalker!")
	//_, err := m.bot.Send(msg)
	//if err != nil {
	//	log.Errorf("m.bot.Send error: %s", err.Error())
	//}

	for update := range m.updates {
		if update.Message == nil {
			continue
		}

		log.Debugf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		msg.ReplyToMessageID = update.Message.MessageID

		_, err := m.bot.Send(msg)
		if err != nil {
			log.Errorf("m.bot.Send error: %s", err.Error())
		}
	}
}
