package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

var (
	hiddenForAdminLocationsFields = []string{}
	hiddenForAdminGPSFields       = []string{}

	hiddenLocationsFields = []string{
		"Id",
	}

	hiddenGPSFields = []string{
		"Id",
	}
)

func (api *API) LocationsList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var locations []models.Location

	/*var filters string
	var args []interface{}

	title := r.URL.Query().Get("filter")
	if title != "" {
		filters = "loc_title LIKE ?"
		args = append(args, "%"+title+"%")
	}*/
	//Where(filters, args)
	res := api.db.Where("loc_is_active = ?", true).Order("loc_id DESC").Find(&locations).Limit(50) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading locations from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "locations loading"))
		return
	}

	for i := range locations {
		api.db.Model(&locations[i]).Association("GPS").Find(&locations[i].GPS)
		//api.db.Model(&locations[i]).Association("Photos").Find(&locations[i].Photos)
	}

	response.Json(w, api.MakeModelResponseList(locations, hiddenForAdminLocationsFields))
}

func (api *API) LocationCreateOrUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err      error
		res      *gorm.DB
		image    models.Image
		location models.Location
	)

	var lcu apimodels.LocationCreateOrUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&lcu)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := lcu.Validate(); err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, err.Error()))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	if lcu.Id == 0 { //create the new one
		log.Debugf("Received location Create request with Id %d, Title %s", lcu.Id, lcu.Title)

		location.Desc = lcu.Desc
		location.Title = lcu.Title
		location.Type = models.LocationType(lcu.Type)
		location.Zone = models.LocationZone(lcu.Zone)

		location.GPSId = sql.NullInt64{
			Int64: 0,
			Valid: false,
		}
		if lcu.GPSId > 0 {
			location.GPSId = sql.NullInt64{
				Int64: lcu.GPSId,
				Valid: true,
			}
		}

		location.IsActive = lcu.IsActive

		res = DBTX.DB.Create(&location)
		if res.Error != nil {
			log.Errorf("Cannot create location to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "location creating"))
			return
		}

		//create images
		for _, ph := range lcu.Photos {
			image, err = api.createNewImageFromBase64(DBTX, ph, models.ImagePhoto)
			if err != nil {
				log.Errorf("Cannot createNewImageFromBase64: %s", err.Error())
				response.JsonError(w, err)
				return
			}

			mi := models.LocationsImage{
				LocationId: location.Id,
				ImageId:    image.Id,
			}

			res = DBTX.DB.Create(&mi)
			if res.Error != nil {
				log.Errorf("Cannot create location image to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "location image assoc creating"))
				return
			}
		}
	} else { //or update
		log.Debugf("Received location Update request with Id %d, Title %s", lcu.Id, lcu.Title)

		res = DBTX.DB.Where("loc_id = ?", lcu.Id).First(&location)
		if res.Error != nil {
			log.Error("loading location %d from DB error: %s", lcu.Id, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "location loading"))
			return
		}

		location.Desc = lcu.Desc
		location.Title = lcu.Title
		location.Type = models.LocationType(lcu.Type)
		location.Zone = models.LocationZone(lcu.Zone)
		location.IsActive = lcu.IsActive

		location.GPSId = sql.NullInt64{
			Int64: 0,
			Valid: false,
		}
		if lcu.GPSId > 0 {
			location.GPSId = sql.NullInt64{
				Int64: lcu.GPSId,
				Valid: true,
			}
		}

		//update images
		for _, ph := range lcu.Photos {
			if ph == "[object Object]" {
				continue //means some old photo sent, no need to update
			}

			image, err = api.createNewImageFromBase64(DBTX, ph, models.ImagePhoto)
			if err != nil {
				log.Errorf("Cannot createNewImageFromBase64: %s", err.Error())
				response.JsonError(w, err)
				return
			}

			mi := models.LocationsImage{
				LocationId: location.Id,
				ImageId:    image.Id,
			}

			res = DBTX.DB.Create(&mi)
			if res.Error != nil {
				log.Errorf("Cannot create location image to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "location image assoc creating"))
				return
			}
		}

		res = DBTX.DB.Save(&location)
		if res.Error != nil {
			log.Errorf("Cannot update location to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "location updating"))
			return
		}
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit DB transaction: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, api.MakeModelResponse(location, hiddenForAdminLocationsFields))
	return
}

func (api *API) GetLocationByIdAdmin(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	itemId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad location id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var location models.Location
	res := api.db.Where("loc_id = ?", itemId).First(&location)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("location with provided id %d was not found", itemId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(itemId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading location from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "location loading"))
		return
	}

	api.db.Model(&location).Association("GPS").Find(&location.GPS)
	api.db.Model(&location).Association("Photos").Find(&location.Photos)

	response.Json(w, api.MakeModelResponse(location, hiddenForAdminItemsFields))
	return
}

func (api *API) GPSCreateOrUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err error
		res *gorm.DB
		gps models.GPS
	)

	var gpscu apimodels.GPSCreateOrUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&gpscu)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := gpscu.Validate(); err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, err.Error()))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	if gpscu.Id == 0 { //create the new one
		log.Debugf("Received GPS Create request with Latitude %f, Longitude %f", gpscu.Latitude, gpscu.Longitude)

		gps.Desc = helpers.ToNullString(gpscu.Desc)
		gps.Latitude = gpscu.Latitude
		gps.Longitude = gpscu.Longitude
		gps.Accuracy = helpers.ToNullFloat64(gpscu.Accuracy)
		gps.Speed = helpers.ToNullFloat64(gpscu.Speed)
		gps.IsActive = gpscu.IsActive

		res = DBTX.DB.Create(&gps)
		if res.Error != nil {
			log.Errorf("Cannot create GPS to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "GPS creating"))
			return
		}
	} else { //or update
		log.Debugf("Received GPS Update request with Latitude %f, Longitude %f", gpscu.Latitude, gpscu.Longitude)

		res = DBTX.DB.Where("gps_id = ?", gpscu.Id).First(&gps)
		if res.Error != nil {
			log.Error("loading GPS %d from DB error: %s", gpscu.Id, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "GPS loading"))
			return
		}

		gps.Desc = helpers.ToNullString(gpscu.Desc)
		gps.Latitude = gpscu.Latitude
		gps.Longitude = gpscu.Longitude
		gps.Accuracy = helpers.ToNullFloat64(gpscu.Accuracy)
		gps.Speed = helpers.ToNullFloat64(gpscu.Speed)
		gps.IsActive = gpscu.IsActive

		res = DBTX.DB.Save(&gps)
		if res.Error != nil {
			log.Errorf("Cannot update GPS to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "GPS updating"))
			return
		}
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit DB transaction: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, api.MakeModelResponse(gps, hiddenForAdminGPSFields))
	return
}
