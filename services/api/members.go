package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fogleman/gg"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/skip2/go-qrcode"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"image/color"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

var (
	hiddenForAdminMemberFields = []string{}

	hiddenMemberFields = []string{
		"LegendMaster",
		"CharacterId",
		"User",
		"UserId",
		"UpdatedAt",
	}

	hiddenNotSelfMemberFields = []string{
		"LegendMaster",
		"CharacterId",
		"IsActive",
		"User",
		"UserId",
		"Money",
		"CharacterId",
		"PassportImages",
		"PassportPhotoId",
		"UpdatedAt",
		"IsPassportFilled",
		"Experience",
		"Data",
		"CreatedAt",
		"CharacterId",
		"Legend",
		"Money",
		"TotalExperience",
		"Photos",
	}
)

const (
	PassportFrontFileName = "passport_front.jpg"
	PassportBackFileName  = "passport_back.jpg"
	PassportFontName      = "3789.ttf"
	PassportArialFontName = "Arial.ttf"
	Version               = 1
	MaxUserMembers        = 3
)

func (api *API) registerNewMember(DBTX dao.Transaction, mc apimodels.MemberCreate, user models.User) (models.Member, error) {
	var member models.Member

	code := helpers.RandHexString(models.CodeLength)

	//check code, callsign to be unique
	res := DBTX.DB.Where("mbr_code = ? OR mbr_callsign = ?",
		code, mc.Callsign,
	).First(&member)
	if !errors.Is(res.Error, gorm.ErrRecordNotFound) && res == nil {
		log.Warnf("Duplicate member was found by code %s or callsign %s", code, mc.Callsign)
		return member, resperrors.NewWithDesc(resperrors.ErrAlreadyExists, "member", fmt.Sprintf("%s %s", mc.Callsign, code))
	}
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Errorf("Cannot load member check from DB: %s", res.Error.Error())
		return member, resperrors.New(resperrors.ErrService)
	}

	member = models.Member{
		UserId:     user.Id,
		Code:       code,
		Callsign:   mc.Callsign,
		Rank:       models.RankNovice,
		Legend:     helpers.ToNullString(mc.Legend),
		FractionId: mc.FractionId,
		SuitId:     mc.SuitId,
		IsActive:   true,
	}

	// set all connected images as not temporary
	if mc.PassportPhotoId == 0 {
		log.Errorf("Bad param passport_photo_id %d", mc.PassportPhotoId)
		return member, resperrors.New(resperrors.ErrBadParam, "passport_photo_id")
	}
	err := api.makeImageStatic(DBTX, mc.PassportPhotoId)
	if err != nil {
		return member, err
	}

	member.PassportPhotoId = mc.PassportPhotoId

	res = DBTX.DB.Create(&member)
	if res.Error != nil {
		log.Errorf("Cannot create member to DB: %s", res.Error.Error())
		return member, resperrors.New(resperrors.ErrService)
	}

	//create images
	for i, imgId := range mc.PhotosIds {
		if imgId == 0 {
			log.Errorf("Bad param photos_ids[%d]", i)
			return member, resperrors.New(resperrors.ErrBadParam, "photos_ids")
		}
		err = api.makeImageStatic(DBTX, imgId)
		if err != nil {
			return member, err
		}

		mi := models.MembersImage{
			MemberId: member.Id,
			ImageId:  imgId,
		}

		res = DBTX.DB.Create(&mi)
		if res.Error != nil {
			log.Errorf("Cannot create member image to DB: %s", res.Error.Error())
			return member, resperrors.New(resperrors.ErrService)
		}
	}

	log.Debugf("Register member with id=%d and code %s", member.Id, member.Code)

	return member, nil
}

func (api *API) CheckCallsignIsFree(w http.ResponseWriter, r *http.Request) {
	callsign := r.URL.Query().Get("callsign")

	if utf8.RuneCountInString(callsign) < 2 {
		log.Warn("provided callsign is too short")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "callsign"))
		return
	}

	var member models.Member

	res := api.db.Where("mbr_callsign = ?", callsign).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.Json(w, true)
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	response.Json(w, false)
	return
}

func (api *API) MembersList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	queryMap := r.URL.Query()

	userGroups, ok := queryMap["group"]
	if !ok {
		userGroups = []string{string(models.UserMember)} // default group
	}

	var (
		event *models.Event
		err   error
	)
	event, err = api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetActiveRegistrationEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil { //if no reg event found, just load the last one
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var requests []models.MembersEvent

	res := api.db.Where("evt_id = ? AND mev_confirmation IN (?)", event.Id, []models.MemberEventConfirmation{
		models.ConfirmationPaid,
	}).Order("mev_id asc").Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	var resp []models.MembersEvent
	for i := range requests {
		err := api.db.Model(&requests[i]).Where("mbr_is_active = 1").Association("Member").Find(&requests[i].Member)
		if err != nil {
			log.Errorf("loading request Member Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Member loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("PassportPhoto").Find(&requests[i].Member.PassportPhoto)
		if err != nil {
			log.Errorf("loading request PassportPhoto Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request PassportPhoto loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("User").Find(&requests[i].Member.User)
		if err != nil {
			log.Errorf("loading request User Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request user loading"))
			return
		}
		if !requests[i].Member.IsActive || requests[i].Member.User.IsBanned || !func() bool {
			for ug := range userGroups {
				if strings.EqualFold(string(requests[i].Member.User.Group), userGroups[ug]) {
					return true
				}
			}
			return false
		}() {
			continue
		}

		err = api.db.Model(&requests[i]).Association("Event").Find(&requests[i].Event)
		if err != nil {
			log.Errorf("loading request Event Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Event loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Fraction").Find(&requests[i].Member.Fraction)
		if err != nil {
			log.Errorf("loading request Fraction Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Fraction loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Suit").Find(&requests[i].Member.Suit)
		if err != nil {
			log.Errorf("loading request Suit Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Fraction loading"))
			return
		}

		err = api.db.Model(&requests[i].Member.User).Association("City").Find(&requests[i].Member.User.City)
		if err != nil {
			log.Errorf("loading request City Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request City loading"))
			return
		}

		resp = append(resp, requests[i])
	}

	response.Json(w, api.MakeModelResponseList(resp, hiddenForAdminUserFields))
	return
}

func (api *API) GetMemberByUserIdAdmin(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	userId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad user id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var member models.Member
	res := api.db.Where("usr_id = ? AND mbr_is_active = 1", userId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided user id %d was not found", userId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(userId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	api.db.Model(&member).Association("Photos").Find(&member.Photos)
	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	api.db.Model(&member).Association("User").Find(&member.User)

	response.Json(w, api.MakeModelResponse(member, hiddenForAdminMemberFields))
	return
}

func (api *API) GetMemberByEventRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var me models.MembersEvent
	res := api.db.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(requestId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading"))
		return
	}

	var member models.Member
	res = api.db.Where("mbr_id = ?", me.MemberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided id %d was not found", me.MemberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.MemberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	api.db.Model(&member).Association("Photos").Find(&member.Photos)
	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	//api.db.Model(&member).Association("User").Find(&member.User)

	response.Json(w, api.MakeModelResponse(member, hiddenForAdminMemberFields))
	return
}

func (api *API) generatePassport(member models.Member) (imgs apimodels.PassportImages, err error) {
	api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)

	pis, err := member.GetPassportImages()
	if err != nil {
		return imgs, fmt.Errorf("cannot GetPassportImages: %s", err.Error())
	}

	dir, err := os.Getwd()
	if err != nil {
		return imgs, fmt.Errorf("cannot get current directory: %s", err.Error())
	}

	passportFront, err := gg.LoadImage(dir + models.StaticImagesSysPathDir + PassportFrontFileName)
	if err != nil {
		return imgs, fmt.Errorf("cannot open image file %s: %s", PassportFrontFileName, err.Error())
	}
	dc := gg.NewContextForImage(passportFront)

	passportPhoto, err := gg.LoadImage(dir + member.PassportPhoto.SysPath)
	if err != nil {
		return imgs, fmt.Errorf("cannot open image file %s: %s", member.PassportPhoto.SysPath, err.Error())
	}

	if err := dc.LoadFontFace(dir+models.StaticImagesSysPathDir+PassportFontName, 31); err != nil {
		return imgs, fmt.Errorf("cannot load font: %s", err.Error())
	}

	dc.SetRGB255(0xCC, 0xCC, 0xCC)
	dc.DrawStringWrapped("SID: "+member.Code, 627, 111, 0, 0, 400, 1.2, gg.AlignLeft)

	if err := dc.LoadFontFace(dir+models.StaticImagesSysPathDir+PassportArialFontName, 32); err != nil {
		return imgs, fmt.Errorf("cannot load font: %s", err.Error())
	}
	dc.DrawStringWrapped(member.Callsign, 627, 548, 0, 0, 300, 1.5, gg.AlignCenter)

	dc.DrawImage(passportPhoto, 627, 141)

	err = dc.SavePNG(pis.FrontImgSys)
	if err != nil {
		return imgs, fmt.Errorf("cannot save front image %s: %s", pis.FrontImgSys, err.Error())
	}

	qrCodeData, err := json.Marshal(apimodels.QRData{
		Id:      string(member.Id),
		Code:    member.Code,
		Version: Version,
	})
	qrCodeDataEncoded := base64.StdEncoding.EncodeToString(qrCodeData)

	err = qrcode.WriteColorFile(qrCodeDataEncoded, qrcode.Medium, 451, color.Gray16{0xcccc}, color.Black, pis.QRCodeImgSyS)
	if err != nil {
		return imgs, fmt.Errorf("cannot generate qrcode: %s", err.Error())
	}

	passportBack, err := gg.LoadImage(dir + models.StaticImagesSysPathDir + PassportBackFileName)
	if err != nil {
		return imgs, fmt.Errorf("cannot open image file %s: %s", PassportBackFileName, err.Error())
	}

	dc = gg.NewContextForImage(passportBack)

	memberQRCode, err := gg.LoadImage(pis.QRCodeImgSyS)
	if err != nil {
		return imgs, fmt.Errorf("cannot open image file %s: %s", member.PassportPhoto.SysPath, err.Error())
	}

	dc.DrawImage(memberQRCode, 525, 119)

	//add member code
	if err := dc.LoadFontFace(dir+models.StaticImagesSysPathDir+PassportFontName, 17); err != nil {
		return imgs, fmt.Errorf("cannot load font: %s", err.Error())
	}

	dc.SetRGB255(0x99, 0x99, 0x99)
	dc.DrawStringWrapped(member.Code, 70, 185, 0, 0, 400, 1.2, gg.AlignLeft)

	err = dc.SavePNG(pis.BackImgSys)
	if err != nil {
		return imgs, fmt.Errorf("cannot save back image %s: %s", pis.FrontImgSys, err.Error())
	}

	return pis, nil
}

func (api *API) MemberPassportGenerate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	memberId, err := strconv.ParseInt(params["mid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var member models.Member
	res := api.db.Where("mbr_id = ?", memberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(memberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	pis, err := api.generatePassport(member)
	if err != nil {
		log.Errorf("passport generating error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "passport generating"))
		return
	}

	response.Json(w, pis)
	return
}

func (api *API) MembersAllPassportGenerate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	log.Debugf("MembersAllPassportGenerate called")
	var (
		event *models.Event
		err   error
	)
	event, err = api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetActiveRegistrationEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil { //if no reg event found, just load the last one
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var requests []models.MembersEvent
	res := api.db.Where("evt_id = ? AND mev_confirmation IN (?) AND mev_has_passport = 0", event.Id, []models.MemberEventConfirmation{
		models.ConfirmationPaid,
		models.ConfirmationNone,
		models.ConfirmationCommented,
		models.ConfirmationPartiallyPaid,
		models.ConfirmationApproved,
	}).Order("mev_id asc").Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	var membersIds []uint
	for i := range requests {
		membersIds = append(membersIds, requests[i].MemberId)
	}

	log.Debugf("Found %d requests for passport generation", len(membersIds))

	var members []models.Member
	res = api.db.Where("mbr_id IN (?)", membersIds).Order("mbr_id asc").Find(&members)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(members) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading members list from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "members loading"))
		return
	}

	var resp []apimodels.PassportImages
	for i := range members {
		log.Debugf("generating passport #%d of %d for member id = %d", i, len(members), members[i].Id)
		pis, err := api.generatePassport(members[i])
		if err != nil {
			log.Errorf("passport #%d (member id = %d) generating error: %s", i, members[i].Id, err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "passport generating"))
			return
		}

		resp = append(resp, pis)
	}

	if w != nil {
		response.Json(w, resp)
	}
	log.Debugf("finished MembersAllPassportGenerate")
	return
}

func (api *API) ScanCode(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	APIKey, isAPIKey := ctx.Value(middleware.ContextAPIKey).(models.APIKey)
	if !isAPIKey {
		log.Errorf("AUTH: %s is empty. Probably middleware was not called or bad auth!", middleware.ContextAPIKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	if !APIKey.IsValid() {
		log.Errorf("Api Key %s is not valid", ctx.Value(middleware.ContextAPIKey).(models.APIKey).Key)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	decoded, err := base64.StdEncoding.DecodeString(params["data"])
	if err != nil {
		log.Errorf("cannot decode base64 request data param: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "data"))
		return
	}

	var qr apimodels.QRData

	err = json.Unmarshal(decoded, &qr)
	if err != nil {
		log.Errorf("cannot decode json request data param: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "data"))
		return
	}

	var member models.Member
	res := api.db.Where("mbr_id = ?", qr.Id).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(qr.Id)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	//api.db.Model(&member).Association("Photos").Find(&member.Photos)
	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	//api.db.Model(&member).Association("User").Find(&member.User)

	response.Json(w, api.MakeModelResponse(member, hiddenMemberFields))

}

func (api *API) GetMemberByUserId(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	userUuid, isUserUuid := ctx.Value(middleware.ContextUserUuidKey).(string)
	if !isUserUuid {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserUuidKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)
	userId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad user id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var member models.Member
	res := api.db.Where("usr_id = ? AND mbr_is_active = 1", userId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided user id %d was not found", userId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(userId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	if err != nil {
		log.Errorf("loading member PassportPhoto assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("Photos").Find(&member.Photos)
	if err != nil {
		log.Errorf("loading member Photos assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	log.Debugf("member %d photos loaded %d", member.Id, len(member.Photos))

	err = api.db.Model(&member).Association("Suit").Find(&member.Suit)
	if err != nil {
		log.Errorf("loading member Suit assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	if err != nil {
		log.Errorf("loading member Fraction assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("User").Find(&member.User)
	if err != nil {
		log.Errorf("loading member User assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(member, hiddenForAdminMemberFields))
		return
	}

	if userUuid == member.User.Uuid.String() {
		response.Json(w, api.MakeModelResponse(member, hiddenMemberFields))
		return
	}

	response.Json(w, api.MakeModelResponse(member, hiddenNotSelfMemberFields))
	return
}

func (api *API) GetMemberByCode(w http.ResponseWriter, r *http.Request) {
	/*ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	userUuid, isUserUuid := ctx.Value(middleware.ContextUserUuidKey).(string)
	if !isUserUuid {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserUuidKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}*/

	params := mux.Vars(r)
	code := params["code"]

	var member models.Member
	res := api.db.Where("mbr_code = ? AND mbr_is_active = 1", code).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided code %s was not found", code)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, code))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	/*err := api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	if err != nil {
		log.Errorf("loading member PassportPhoto assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("Photos").Find(&member.Photos)
	if err != nil {
		log.Errorf("loading member Photos assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}*/

	log.Debugf("member %d photos loaded %d", member.Id, len(member.Photos))

	err := api.db.Model(&member).Association("Suit").Find(&member.Suit)
	if err != nil {
		log.Errorf("loading member Suit assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	err = api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	if err != nil {
		log.Errorf("loading member Fraction assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	/*err = api.db.Model(&member).Association("User").Find(&member.User)
	if err != nil {
		log.Errorf("loading member User assoc from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}*/

	response.Json(w, api.MakeModelResponse(member, hiddenNotSelfMemberFields))
	return
}

func (api *API) MembersListByEvent(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)
	eventId, err := strconv.ParseInt(params["eventId"], 10, 64)
	if err != nil {
		log.Errorf("bad user eventId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "eventId"))
		return
	}

	query := api.db.Where("evt_id = ? AND mev_confirmation IN (?)", eventId, []models.MemberEventConfirmation{
		models.ConfirmationPaid,
	}).Order("mev_id asc")

	queryMap := r.URL.Query()
	fractionsIds, fractionsFilterExists := queryMap["fractionId"]
	onlyMembers, _ := strconv.ParseBool(r.URL.Query().Get("onlyMembers"))

	var requests []models.MembersEvent
	res := query.Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	var resp []models.MembersEvent
	for i := range requests {
		err := api.db.Model(&requests[i]).Association("Member").Find(&requests[i].Member)
		if err != nil {
			log.Errorf("loading request Member Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Member loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("User").Find(&requests[i].Member.User)
		if err != nil {
			log.Errorf("loading request User Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request User loading"))
			return
		}

		err = api.db.Model(&requests[i]).Association("Event").Find(&requests[i].Event)
		if err != nil {
			log.Errorf("loading request Event Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Event loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Fraction").Find(&requests[i].Member.Fraction)
		if err != nil {
			log.Errorf("loading request Fraction Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Fraction loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Suit").Find(&requests[i].Member.Suit)
		if err != nil {
			log.Errorf("loading request Suit Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Suit loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("PassportPhoto").Find(&requests[i].Member.PassportPhoto)
		if err != nil {
			log.Errorf("loading request PassportPhoto Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request PassportPhoto loading"))
			return
		}

		//make PassportPhoto path
		if requests[i].Member.PassportPhoto.Id != 0 && strings.TrimSpace(requests[i].Member.PassportPhoto.Path) != "" {
			requests[i].Member.PassportPhoto.Path = helpers.GetImagePath(r, requests[i].Member.PassportPhoto.Name)
		}

		if func() bool { //process deep filters
			if fractionsFilterExists {
				var frId uint64
				for f := range fractionsIds {
					frId, _ = strconv.ParseUint(fractionsIds[f], 10, 64)
					if frId == uint64(requests[i].Member.Fraction.Id) {
						return true
					}
				}
			}
			return false
		}() {
			resp = append(resp, requests[i])
		}
	}

	if onlyMembers { //do not return additional data if not requested
		var memberResp []models.Member
		for i := range resp {
			if !api.checkIfAdminGroup(resp[i].Member.User.Group) {
				memberResp = append(memberResp, resp[i].Member)
			}
		}

		if api.checkIfAdminGroup(userGroup) {
			response.Json(w, api.MakeModelResponseList(memberResp, hiddenForAdminMemberFields))
			return
		}

		response.Json(w, api.MakeModelResponseList(memberResp, hiddenNotSelfMemberFields))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponseList(resp, hiddenForAdminMemberFields))
		return
	}
	response.Json(w, api.MakeModelResponseList(resp, hiddenNotSelfMemberFields))
	return
}

func (api *API) UpdateMember(w http.ResponseWriter, r *http.Request) {
	log.Debugf("UpdateMember called")
	ctx := r.Context()
	userGroup, isUserGroupEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserGroupEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := uint(uid)

	params := mux.Vars(r)
	memberId, err := strconv.ParseInt(params["memberId"], 10, 64)
	if err != nil {
		log.Errorf("bad user memberId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "memberId"))
		return
	}

	log.Debugf("UpdateMember memberId = %d", memberId)

	var mu apimodels.MemberUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&mu)
	if err != nil {
		log.Errorf("can't decode MemberUpdate data: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := mu.Validate(); err != nil {
		log.Errorf("MemberUpdate validation error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var member models.Member

	res := DBTX.DB.Where("mbr_id = ?", memberId).First(&member) //TODO: use Clauses(clause.Locking{Strength: "UPDATE"}) ?
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member %d is not found in DB", memberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, fmt.Sprintf("member %d", memberId)))
		return
	}
	if res.Error != nil {
		log.Error("loading member %d from DB error: %s", memberId, res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	//some restrictions
	if !api.checkIfAdminGroup(userGroup) {
		if member.UserId != userId { //restrict of editing other user for a members
			log.Error("user %d trying to update info of a member %d of other user %d which is not allowed for group %s",
				userId, memberId, member.UserId, string(userGroup))
			response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed, "other member editing"))
			return
		}

		//check request
		event, err := api.dao.GetLastEvent()
		if err != nil {
			log.Error("loading last event from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "last event loading"))
			return
		}

		var isEventFinished = true
		if event.EndsAt.Valid {
			isEventFinished = event.EndsAt.Time.Before(time.Now())
		}

		var me models.MembersEvent
		res := api.db.Where("evt_id = ? AND mbr_id = ?", event.Id, member.Id).Order("mev_id asc").First(&me)
		if !errors.Is(res.Error, gorm.ErrRecordNotFound) && !isEventFinished && me.Confirmation.IsConfirmed() {
			log.Warnf("user %d rtying to edit his member's %d profile, when he has a confirmed request", userId, member.Id)
			response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed, "you have confirmed request to an event"))
			return
		}
		if !errors.Is(res.Error, gorm.ErrRecordNotFound) && res.Error != nil {
			log.Errorf("loading request from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
			return
		}
	}
	//set fields
	if mu.Legend != nil && len(*mu.Legend) > 0 && !strings.EqualFold(member.Legend.String, *mu.Legend) {
		log.Debugf("UpdateMember member %d legend changed, processing", member.Id)
		res = DBTX.DB.Create(&models.MembersChangeLog{
			EditorUserId:      userId,
			TargetMemberId:    member.Id,
			Type:              models.MCTLegend,
			ProcessedByUserId: sql.NullInt64{},
			PreviousVersion:   member.Legend,
			CurrentVersion:    helpers.ToNullString(*mu.Legend),
		})
		if res.Error != nil {
			log.Errorf("Cannot create member change log (legend) record to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}
		member.Legend = helpers.ToNullString(*mu.Legend)
	}

	if mu.SuitId != nil && *mu.SuitId != 0 && member.SuitId != *mu.SuitId {
		log.Debugf("UpdateMember member %d Suit changed, processing", member.Id)
		//load suit to save title
		err = DBTX.DB.Model(&member).Association("Suit").Find(&member.Suit)
		if err != nil {
			log.Errorf("Cannot find Suit %d member %d assoc: %s", *mu.SuitId, member.Id, err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		var item models.Item
		res := DBTX.DB.Where("itm_id = ?", *mu.SuitId).First(&item)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("item (member suit) with provided id %d was not found", *mu.SuitId)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatUint(uint64(*mu.SuitId), 10)))
			return
		}
		if res.Error != nil {
			log.Errorf("loading item (member suit) from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "item loading"))
			return
		}
		res = DBTX.DB.Create(&models.MembersChangeLog{
			EditorUserId:      userId,
			TargetMemberId:    member.Id,
			Type:              models.MCTSuit,
			ProcessedByUserId: sql.NullInt64{},
			PreviousVersion:   helpers.ToNullString(member.Suit.Title),
			CurrentVersion:    helpers.ToNullString(item.Title),
		})
		if res.Error != nil {
			log.Errorf("Cannot create member change log (suit) record to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}
		member.SuitId = *mu.SuitId
	}

	// set all connected images as not temporary
	if mu.PassportPhotoId != nil && *mu.PassportPhotoId != 0 {
		log.Debugf("UpdateMember member %d PassportPhoto changed, processing", member.Id)
		err := api.makeImageStatic(DBTX, *mu.PassportPhotoId)
		if err != nil {
			log.Errorf("Cannot makeImageStatic %d: %s", *mu.PassportPhotoId, err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		//load PassportPhoto to save title
		err = DBTX.DB.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
		if err != nil {
			log.Errorf("Cannot find PassportPhoto %d member %d assoc: %s", *mu.PassportPhotoId, member.Id, err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		var img models.Image
		res := DBTX.DB.Where("img_id = ?", *mu.PassportPhotoId).First(&img)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Errorf("Cannot find such image id %d in DB (may be cleaner bot removed it?)", *mu.PassportPhotoId)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatUint(uint64(*mu.PassportPhotoId), 10)))
			return
		}
		if res.Error != nil {
			log.Errorf("Cannot find such image id %d in DB: %s", *mu.PassportPhotoId, res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "PassportPhoto loading"))
			return
		}
		res = DBTX.DB.Create(&models.MembersChangeLog{
			EditorUserId:      userId,
			TargetMemberId:    member.Id,
			Type:              models.MCTPassportPhoto,
			ProcessedByUserId: sql.NullInt64{},
			PreviousVersion:   helpers.ToNullString(helpers.GetImagePath(r, member.PassportPhoto.Name)),
			CurrentVersion:    helpers.ToNullString(helpers.GetImagePath(r, img.Name)),
		})
		if res.Error != nil {
			log.Errorf("Cannot create member change log (passport photo) record to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}

		member.PassportPhotoId = *mu.PassportPhotoId
	}

	if len(mu.PhotosIds) > 0 {
		log.Debugf("UpdateMember member %d Photos changed, processing", member.Id)
		//store old photos
		err = DBTX.DB.Model(&member).Association("Photos").Find(&member.Photos)
		if err != nil {
			log.Errorf("Cannot find Photos member %d assoc: %s", member.Id, err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		var oldPhotosList = make([]string, len(member.Photos))
		for i := range member.Photos {
			oldPhotosList[i] = helpers.GetImagePath(r, member.Photos[i].Name)
		}
		oldPhotosListJson, err := json.Marshal(oldPhotosList)
		if err != nil {
			log.Errorf("Cannot marshall member photos list to json: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}

		for _, imgId := range mu.PhotosIds { //find if photo created
			if imgId == 0 {
				continue
			}
			if !func() bool {
				for j := range member.Photos {
					if imgId == member.Photos[j].Id {
						return true
					}
				}
				return false
			}() {
				member.Photos = []models.Image{}
				err = api.makeImageStatic(DBTX, imgId)
				if err != nil {
					log.Errorf("Cannot makeImageStatic %d: %s", imgId, err.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}

				mi := models.MembersImage{
					MemberId: member.Id,
					ImageId:  imgId,
				}

				res = DBTX.DB.Create(&mi)
				if res.Error != nil {
					log.Errorf("Cannot create member image to DB: %s", res.Error.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}
				err = DBTX.DB.Model(&member).Association("Photos").Find(&member.Photos)
				if err != nil {
					log.Errorf("Cannot find Photos member %d assoc: %s", member.Id, err.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}
			}
		}

		for _, rmImg := range member.Photos { //find if photo removed
			if rmImg.Id == 0 {
				continue
			}
			if !func() bool {
				for j := range mu.PhotosIds {
					if rmImg.Id == mu.PhotosIds[j] { //such photo exist
						return true
					}
				}

				return false //not existing photo in update
			}() {
				member.Photos = []models.Image{}
				res = DBTX.DB.Delete(models.MembersImage{}, "mbr_id = ? AND img_id = ?", member.Id, rmImg.Id)
				if res.Error != nil {
					log.Errorf("Cannot remove member image from DB: %s", res.Error.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}

				err = api.makeImageRemoved(DBTX, rmImg.Id)
				if err != nil {
					log.Errorf("Cannot makeImageRemoved %d: %s", rmImg.Id, err.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}

				err = DBTX.DB.Model(&member).Association("Photos").Delete()
				if err != nil {
					log.Errorf("Cannot find Photos member %d assoc: %s", member.Id, err.Error())
					response.JsonError(w, resperrors.New(resperrors.ErrService))
					return
				}
			}
		}

		var newPhotosList = make([]string, len(member.Photos))
		for i := range member.Photos {
			newPhotosList[i] = helpers.GetImagePath(r, member.Photos[i].Name)
		}
		newPhotosListJson, err := json.Marshal(newPhotosList)
		if err != nil {
			log.Errorf("Cannot marshall member photos list to json: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}

		//store change log
		res = DBTX.DB.Create(&models.MembersChangeLog{
			EditorUserId:      userId,
			TargetMemberId:    member.Id,
			Type:              models.MCTPhotos,
			ProcessedByUserId: sql.NullInt64{},
			PreviousVersion:   helpers.ToNullString(string(oldPhotosListJson)),
			CurrentVersion:    helpers.ToNullString(string(newPhotosListJson)),
		})
		if res.Error != nil {
			log.Errorf("Cannot create member change log (photos) record to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}
	}

	//process admin fields
	if api.checkIfAdminGroup(userGroup) {
		if mu.FractionId != nil && *mu.FractionId != 0 && member.FractionId != *mu.FractionId {
			//load fraction to save title
			err = DBTX.DB.Model(&member).Association("Fraction").Find(&member.Fraction)
			if err != nil {
				log.Errorf("Cannot find Fraction %d member %d assoc: %s", member.FractionId, member.Id, err.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService))
				return
			}

			var fraction models.Fraction
			res := DBTX.DB.Where("frc_id = ?", *mu.FractionId).First(&fraction)
			if errors.Is(res.Error, gorm.ErrRecordNotFound) {
				log.Error("member fraction with provided id %d was not found", *mu.FractionId)
				response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatUint(uint64(*mu.FractionId), 10)))
				return
			}
			if res.Error != nil {
				log.Errorf("loading member fraction from DB error: %s", res.Error.Error())
				response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "fraction loading"))
				return
			}
			res = DBTX.DB.Create(&models.MembersChangeLog{
				EditorUserId:      userId,
				TargetMemberId:    member.Id,
				Type:              models.MCTFraction,
				ProcessedByUserId: sql.NullInt64{},
				PreviousVersion:   helpers.ToNullString(member.Fraction.Title),
				CurrentVersion:    helpers.ToNullString(fraction.Title),
			})
			if res.Error != nil {
				log.Errorf("Cannot create member change log (fraction) record to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService))
			}

			member.FractionId = *mu.FractionId
		}

		if mu.Rank != nil && *mu.Rank != "" && member.Rank != models.MemberRank(*mu.Rank) {
			member.Rank = models.MemberRank(*mu.Rank)
		}

		if mu.LegendMaster != nil && *mu.LegendMaster != "" && member.LegendMaster.String != *mu.LegendMaster {
			res = DBTX.DB.Create(&models.MembersChangeLog{
				EditorUserId:      userId,
				TargetMemberId:    member.Id,
				Type:              models.MCTLegendMaster,
				ProcessedByUserId: sql.NullInt64{},
				PreviousVersion:   member.LegendMaster,
				CurrentVersion:    helpers.ToNullString(*mu.LegendMaster),
			})
			if res.Error != nil {
				log.Errorf("Cannot create member change log record to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService))
			}
			member.LegendMaster = helpers.ToNullString(*mu.LegendMaster)
		}

		if mu.Level != nil && member.Level != *mu.Level {
			member.Level = *mu.Level
		}

		if mu.Money != nil && member.Money != *mu.Money {
			member.Money = *mu.Money
		}

		if mu.Experience != nil && member.Experience != *mu.Experience {
			member.Experience = *mu.Experience
		}
	}

	res = DBTX.DB.Omit(clause.Associations).Save(&member)
	if res.Error != nil {
		log.Errorf("Cannot update member to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	api.db.Model(&member).Association("PassportPhoto").Find(&member.PassportPhoto)
	api.db.Model(&member).Association("Photos").Find(&member.Photos)
	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)
	api.db.Model(&member).Association("User").Find(&member.User)

	log.Debugf("Updated member with id=%d and code %s", member.Id, member.Code)

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(member, hiddenForAdminMemberFields))
		return
	}

	response.Json(w, api.MakeModelResponse(member, hiddenMemberFields))
	return
}

func (api *API) GetMemberData(w http.ResponseWriter, r *http.Request) {
	callsign := r.URL.Query().Get("callsign")
	code := r.URL.Query().Get("code")

	if utf8.RuneCountInString(callsign) > 0 && utf8.RuneCountInString(callsign) < 2 {
		log.Warn("provided callsign is too short")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "callsign"))
		return
	}

	if utf8.RuneCountInString(code) > 0 && utf8.RuneCountInString(code) < 8 {
		log.Warn("provided code is too short")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "code"))
		return
	}

	var member models.Member
	query := ""
	arg := ""
	if utf8.RuneCountInString(callsign) > 0 {
		query, arg = "LOWER(mbr_callsign) LIKE LOWER(?)", "%"+callsign+"%"
	}
	if utf8.RuneCountInString(code) > 0 {
		query, arg = "UPPER(mbr_code) LIKE UPPER(?)", "%"+code+"%"
	}
	res := api.db.Where(query, arg).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member %s %s is not found in DB", callsign, code)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, fmt.Sprintf("member %s %s", callsign, code)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)

	response.Json(w, api.MakeModelResponse(member, hiddenMemberFields))
	return
}

func (api *API) SetMemberData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	memberId, err := strconv.ParseInt(params["memberId"], 10, 64)
	if err != nil {
		log.Errorf("bad user memberId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "memberId"))
		return
	}

	var mud apimodels.MemberUpdateData
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&mud)
	if err != nil {
		log.Errorf("can't decode SetMemberData data: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := mud.Validate(); err != nil {
		log.Errorf("SetMemberData validation error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam))
		return
	}

	var member models.Member
	res := api.db.Where("mbr_id = ?", memberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member %d is not found in DB", memberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, fmt.Sprintf("member %d", memberId)))
		return
	}
	if res.Error != nil {
		log.Error("loading member %d from DB error: %s", memberId, res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	//set fields
	//TODO may be do it in more proper way by reflection?
	if mud.Data != nil && len(*mud.Data) > 0 {
		member.Data = *mud.Data
		member.IsPassportFilled = true
	}

	if mud.Rank != nil && len(*mud.Rank) > 0 {
		member.Rank = models.MemberRank(*mud.Rank)
	}

	if mud.Level != nil {
		member.Level = *mud.Level
	}

	if mud.Money != nil {
		member.Money = *mud.Money
	}

	if mud.Experience != nil {
		member.Experience = *mud.Experience
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	res = DBTX.DB.Save(&member)
	if res.Error != nil {
		log.Errorf("Cannot update member to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	api.db.Model(&member).Association("Suit").Find(&member.Suit)
	api.db.Model(&member).Association("Fraction").Find(&member.Fraction)

	log.Debugf("Updated member data with id=%d and code %s", member.Id, member.Code)

	response.Json(w, api.MakeModelResponse(member, hiddenMemberFields))
	return
}

func (api *API) AddMember(w http.ResponseWriter, r *http.Request) {
	log.Debugf("AddMember called")
	ctx := r.Context()
	userGroup, isUserGroupEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserGroupEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := uint(uid)

	var user models.User
	res := api.db.Where("usr_id = ?", userId).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("User with provided id %d was not found", userId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatUint(uint64(userId), 10)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading user from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	//check count of user members
	var membersCount int64
	res = api.db.Model(&models.Member{}).Where("usr_id = ?", userId).Count(&membersCount)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "user members"))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}
	if membersCount >= MaxUserMembers {
		response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed, fmt.Sprintf("max user members %d are already exists", MaxUserMembers)))
		return
	}

	var mc apimodels.MemberCreate
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&mc)
	if err != nil {
		log.Errorf("can't decode MemberUpdate data: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := mc.Validate(); err != nil {
		log.Errorf("MemberUpdate validation error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrBadParam, err.Error()))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	member, err := api.registerNewMember(DBTX, mc, user)
	if err != nil {
		log.Errorf("registerNewMember error: %s", err.Error())
		response.JsonError(w, err)
		return
	}

	//deactivating all previous user members
	res = DBTX.DB.Exec(`UPDATE members SET mbr_is_active = 0 WHERE usr_id = ? AND mbr_id != ? AND mbr_is_active = 1;`, user.Id, member.Id)
	if res.Error != nil {
		log.Errorf("deactivating user %s members error: %s", user.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user members deactivation"))
		return
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponseList(member, hiddenForAdminMemberFields))
		return
	}
	response.Json(w, api.MakeModelResponseList(member, hiddenNotSelfMemberFields))
	return
}

func (api *API) MembersListByUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := uint(uid)

	var members []models.Member
	res := api.db.Where("usr_id = ?", userId).Order("mbr_is_active desc").Find(&members)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(members) < 1 {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "user members"))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member loading"))
		return
	}

	for i := range members {
		err := api.db.Model(&members[i]).Association("Fraction").Find(&members[i].Fraction)
		if err != nil {
			log.Errorf("loading request Fraction Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Fraction loading"))
			return
		}

		err = api.db.Model(&members[i]).Association("Suit").Find(&members[i].Suit)
		if err != nil {
			log.Errorf("loading request Suit Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Suit loading"))
			return
		}

		err = api.db.Model(&members[i]).Association("PassportPhoto").Find(&members[i].PassportPhoto)
		if err != nil {
			log.Errorf("loading request PassportPhoto Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request PassportPhoto loading"))
			return
		}

		err = api.db.Model(&members[i]).Association("Photos").Find(&members[i].Photos)
		if err != nil {
			log.Errorf("loading member Photos assoc from DB error: %s", err.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
			return
		}
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponseList(members, hiddenForAdminMemberFields))
		return
	}
	response.Json(w, api.MakeModelResponseList(members, hiddenMemberFields))
	return
}

func (api *API) MembersAllRankSet(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	log.Debugf("MembersAllRankSet called")
	var (
		event *models.Event
		err   error
	)
	event, err = api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetActiveRegistrationEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil { //if no reg event found, just load the last one
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var requests []models.MembersEvent
	res := api.db.Where("evt_id = ? AND mev_confirmation IN (?)", event.Id, []models.MemberEventConfirmation{
		models.ConfirmationPaid,
		models.ConfirmationNone,
		models.ConfirmationCommented,
		models.ConfirmationPartiallyPaid,
		models.ConfirmationApproved,
	}).Order("mev_id asc").Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	var membersIds []uint
	for i := range requests {
		membersIds = append(membersIds, requests[i].MemberId)
	}

	log.Debugf("Found %d requests for rank set", len(membersIds))

	var members []models.Member
	res = api.db.Where("mbr_id IN (?)", membersIds).Order("mbr_id asc").Find(&members)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(members) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading members list from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "members loading"))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var resp []uint

	for i := range members {
		if members[i].Experience >= 1000000 {
			members[i].Rank = models.RankLegend
		} else if members[i].Experience >= 400000 {
			members[i].Rank = models.RankMaster
		} else if members[i].Experience >= 150000 {
			members[i].Rank = models.RankVeteran
		} else if members[i].Experience >= 30000 {
			members[i].Rank = models.RankExperienced
		} else if members[i].Experience < 30000 {
			log.Debugf("Ignored")
			continue
		}

		log.Debugf("setting new rank %s of %d for member id = %d", members[i].Rank, len(members), members[i].Id)

		res = DBTX.DB.Omit(clause.Associations).Save(&members[i])
		if res.Error != nil {
			log.Errorf("Cannot update member to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}

		resp = append(resp, members[i].Id)
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	if w != nil {
		response.Json(w, resp)
	}
	log.Debugf("finished MembersAllRankSet")
	return
}
