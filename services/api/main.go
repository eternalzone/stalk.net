package api

import (
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"fmt"
	"net/http"

	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware/acl"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware/jwt"
	"bitbucket.org/eternalzone/stalk.net/services/api/router"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"gorm.io/gorm"

	"context"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

type (
	ActionRoute struct {
		Name   string
		Path   string
		Method string
		Func   func(http.ResponseWriter, *http.Request)
	}

	// API serves the end users requests.
	API struct {
		config conf.Config
		db     *gorm.DB
		dao    dao.IDao

		mail mailer.IMailService

		router *mux.Router
		server *http.Server

		actionRoutesMap map[models.UserGroup][]ActionRoute
	}
)

// NewAPI initializes a new instance of API with needed fields, but doesn't start listening,
// nor creates the router.
func NewAPI(cfg conf.Config, m *gorm.DB, d dao.IDao, ms mailer.IMailService) *API {
	return &API{
		config: cfg,
		db:     m,
		dao:    d,
		mail:   ms,
	}
}

const (
	actionsAPIPrefix   = ""
	actionsAdminPrefix = "admin"
)

const (
	entry             = "./static/dist/index.html"
	static            = "./static/dist/"
	photos            = "./images/"
	firmwaresAnomaly  = "./firmwares/anomaly"
	firmwaresArtefact = "./firmwares/artefact"
	firmwaresSoul     = "./firmwares/soul"
	firmwaresDefence  = "./firmwares/defence"
	firmwaresUnknown  = "./firmwares/unknown"
)

const (
	ApiActionRequestApprove       = "request_approve"
	ApiActionRequestComment       = "request_comment"
	ApiActionRequestReject        = "request_reject"
	ApiActionRequestPayed         = "request_payed"
	ApiActionItemEdit             = "item_edit"
	ApiActionGpsAdd               = "gps_add"
	ApiActionRequestLookApprove   = "request_look_approve"
	ApiActionRequestLegendApprove = "request_legend_approve"
	ApiActionLocationEdit         = "location_edit"
	ApiActionChangeLogProcessed   = "change_log_record_processed"
)

// GracefulStop shuts down the server without interrupting any
// active connections.
func (api *API) GracefulStop(ctx context.Context) error {
	cont, cancel := context.WithTimeout(ctx, api.config.Service.WaitTimeout)
	defer cancel()
	return api.server.Shutdown(cont)
}

// Title returns the title.
func (api *API) Title() string {
	return "API"
}

// Run starts the http server and binds the handlers.
func (api *API) Run() {
	api.initialize()
	api.startServe()
}

func (api *API) indexHandler(entrypoint string) func(w http.ResponseWriter, r *http.Request) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, entrypoint)
	}

	return http.HandlerFunc(fn)
}

func (api *API) initialize() {
	api.router = mux.NewRouter()

	wrapper := negroni.Classic()
	wrapper.Use(cors.New(cors.Options{
		AllowedOrigins:     []string{"*"},
		AllowedMethods:     []string{"POST", "GET", "OPTIONS", "PUT", "DELETE", "PATCH"},
		AllowedHeaders:     []string{"Accept", "Origin", "Content-Type", "Accept-Language", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "X-User-Env", "X-Auth-Token", RealIpHeader, APIKeyHeader, APIKeyTypeHeader},
		MaxAge:             86400,
		AllowCredentials:   true,
		OptionsPassthrough: false,
		Debug:              true,
	}))

	publicMW := []negroni.HandlerFunc{
		negroni.HandlerFunc(api.GetUserEnv),
	}

	//public routes (no middleware, unless GetUserEnv)
	router.HandleActions(api.router, wrapper, actionsAPIPrefix, []router.Route{
		{"/api", "GET", api.Index, publicMW},
		//{"/test", "GET", api.Test, publicMW},
		{"/register", "POST", api.NewRegistration, publicMW},
		{"/login", "POST", api.Login, publicMW},
		{"/restore", "POST", api.RestorePasswordRequest, publicMW},
		{"/users/passchange", "POST", api.ChangePassword, publicMW},
		{"/city", "GET", api.CitiesList, publicMW},
		{"/members", "GET", api.MembersList, publicMW},
		{"/members/isfree/callsign", "GET", api.CheckCallsignIsFree, publicMW},
		{"/members/isfree/email", "GET", api.CheckEmailIsFree, publicMW},
		{"/images/uploadAvatar", "POST", api.UploadAvatarImage, publicMW},
		{"/images/uploadPhotos", "POST", api.UploadPhotos, publicMW},
		{"/fractions", "GET", api.LoadFractionsList, publicMW},
		{"/suits", "GET", api.LoadSuits, publicMW},
		{"/characters/{id}", "GET", api.GetCharacterById, publicMW},
		{"/events/active", "GET", api.FindActiveRegistrationEvent, publicMW},
		{"/member_data", "GET", api.GetMemberData, publicMW},
		{"/member_data_set", "POST", api.SetMemberData, publicMW},
		{"/member/code/{code}", "GET", api.GetMemberByCode, publicMW},
	})

	apiMW := []negroni.HandlerFunc{
		negroni.HandlerFunc(api.CheckAPIKey),
		negroni.HandlerFunc(api.GetUserEnv),
	}
	//API routes
	router.HandleActions(api.router, wrapper, actionsAPIPrefix, []router.Route{
		{"/api/scan/{data}", "GET", api.ScanCode, apiMW},
		{"/api/firmware/{type}/version", "GET", api.GetLatestFirmwareVersion, apiMW},
		{"/api/firmware/{type}/latest", "GET", api.GetLatestFirmwareFile, apiMW},
		{"/api/firmware/{type}", "POST", api.UploadNewFirmware, apiMW},
		{"/api/device/{mac}", "GET", api.FindDeviceData, apiMW},
	})

	//handle statics
	//this.router.PathPrefix("/static").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(static))))
	api.router.PathPrefix("/photos").Handler(http.StripPrefix("/photos/", http.FileServer(http.Dir(photos))))
	api.router.PathPrefix("/data/firmwares/anomaly").Handler(http.StripPrefix("/data/firmwares/anomaly/", http.FileServer(http.Dir(firmwaresAnomaly))))
	api.router.PathPrefix("/data/firmwares/artefact").Handler(http.StripPrefix("/data/firmwares/artefact/", http.FileServer(http.Dir(firmwaresArtefact))))
	api.router.PathPrefix("/data/firmwares/soul").Handler(http.StripPrefix("/data/firmwares/soul/", http.FileServer(http.Dir(firmwaresSoul))))
	api.router.PathPrefix("/data/firmwares/defence").Handler(http.StripPrefix("/data/firmwares/defence/", http.FileServer(http.Dir(firmwaresDefence))))
	api.router.PathPrefix("/data/firmwares/unknown").Handler(http.StripPrefix("/data/firmwares/unknown/", http.FileServer(http.Dir(firmwaresUnknown))))
	//this.router.PathPrefix("/").HandlerFunc(this.indexHandler(entry))

	jwtMiddleware, err := jwt.NewJwtFromBase64(api.config.Auth.ECDSAPublicKeyBase64)
	if err != nil {
		log.Fatalf("Unable to get JWT from base64 ECDSA public key: %v", err)
	}

	//user routes
	privateMW := []negroni.HandlerFunc{
		negroni.HandlerFunc(jwtMiddleware.CheckJWT),
		negroni.HandlerFunc(api.GetUserEnv),
		negroni.HandlerFunc(acl.NewAcl(models.UserMember, models.UserGameMaster, models.UserAdmin, models.UserFractionLeader)),
	}
	router.HandleActions(api.router, wrapper, actionsAPIPrefix, []router.Route{
		{"/actions", "GET", api.GetAllowedActionsByGroup, privateMW},
		{"/member/user/{id}", "GET", api.GetMemberByUserId, privateMW},
		{"/event/{eventId}/member/{memberId}", "GET", api.MemberIdEventRequest, privateMW},
		{"/event/{eventId}/member/{memberId}", "POST", api.SendMembersEventRequest, privateMW},
		{"/event/{eventId}/member", "GET", api.MembersListByEvent, privateMW},
		{"/user/{uuid}", "GET", api.GetUser, privateMW},
		{"/user", "GET", api.GetUser, privateMW},
		{"/member/{memberId}", "POST", api.UpdateMember, privateMW},
		{"/member", "POST", api.AddMember, privateMW},
		{"/member/user", "GET", api.MembersListByUser, privateMW},
		{"/events/active/member", "GET", api.MembersActiveEventRequest, privateMW},
	})

	//admin and gamemaster routes
	adminMW := []negroni.HandlerFunc{
		negroni.HandlerFunc(jwtMiddleware.CheckJWT),
		negroni.HandlerFunc(api.GetUserEnv),
		negroni.HandlerFunc(acl.NewAcl(models.UserGameMaster, models.UserAdmin)),
	}
	router.HandleActions(api.router, wrapper, actionsAPIPrefix, []router.Route{
		{"/member", "GET", api.MembersList, adminMW},
		{"/request/{rid}", "GET", api.GetEventRequestById, adminMW},
		{"/request/member/{rid}", "GET", api.GetMemberByEventRequest, adminMW},
		{"/request/user/{rid}", "GET", api.GetUserByEventRequest, adminMW},
		{"/event/requests", "GET", api.MembersEventRequestsList, adminMW},
		{"/item", "GET", api.ItemsList, adminMW},
		{"/item/{id}", "GET", api.GetItemByIdAdmin, adminMW},
		{"/item", "POST", api.ItemCreateOrUpdate, adminMW},
		{"/member/passport/{mid}", "POST", api.MemberPassportGenerate, adminMW},
		{"/members_passports", "POST", api.MembersAllPassportGenerate, adminMW},
		{"/members_rank_set", "POST", api.MembersAllRankSet, adminMW},

		{"/firmware", "GET", api.FirmwaresList, adminMW},
		{"/firmware/{type}", "GET", api.FirmwaresList, adminMW},
		{"/firmware/{type}", "POST", api.UploadNewFirmware, adminMW},

		{"/location", "GET", api.LocationsList, adminMW},
		{"/location/{id}", "GET", api.GetLocationByIdAdmin, adminMW},
		{"/location", "POST", api.LocationCreateOrUpdate, adminMW},
		{"/gps", "POST", api.GPSCreateOrUpdate, adminMW},

		{"/character", "GET", api.CharactersList, adminMW},
		{"/character/{id}", "GET", api.GetCharacterByIdAdmin, adminMW},
		{"/character", "POST", api.CharacterCreateOrUpdate, adminMW},

		{"/key", "POST", api.APIKeyCreateOrUpdate, adminMW},
		{"/key", "GET", api.APIKeysLoad, adminMW},
		{"/key/{id}", "GET", api.GetApiKeyById, adminMW},

		{"/notify/unpaid", "POST", api.NotifyUnpaidRequests, adminMW},

		{"/change_log/{id}", "GET", api.GetMembersChangeLogById, adminMW},
		{"/change_log", "GET", api.MembersChangeLogList, adminMW},
		{"/change_log_count", "GET", api.CountNewMembersChangeLogRecords, adminMW},
	})

	//set action routes list
	api.actionRoutesMap = map[models.UserGroup][]ActionRoute{
		models.UserMember:         {},
		models.UserFractionLeader: {},
		models.UserAdmin: {
			{ApiActionRequestLookApprove, "/request/approve_look/{rid}", "POST", api.SetCheckForRequest},
			{ApiActionRequestLegendApprove, "/request/approve_legend/{rid}", "POST", api.SetCheckForRequest},
			{ApiActionRequestApprove, "/request/approve/{rid}", "POST", api.ApproveRequest},
			{ApiActionRequestComment, "/request/comment/{rid}", "POST", api.CommentRequest},
			{ApiActionRequestReject, "/request/reject/{rid}", "POST", api.DeclineRequest},
			{ApiActionRequestPayed, "/request/payed/{rid}", "POST", api.PaidRequest},
			{ApiActionItemEdit, "/items/{id}", "POST", api.ItemCreateOrUpdate},
			{ApiActionLocationEdit, "/location/{id}", "POST", api.LocationCreateOrUpdate},
			{ApiActionGpsAdd, "/gps", "POST", api.GPSCreateOrUpdate},
			{ApiActionChangeLogProcessed, "/change_log/{id}", "POST", api.SetMembersChangeLogByIdAsProcessed},
		},
		models.UserGameMaster: {
			{ApiActionRequestComment, "/request/comment/{rid}", "POST", api.CommentRequest},
			{ApiActionRequestLookApprove, "/request/approve_look/{rid}", "POST", api.SetCheckForRequest},
			{ApiActionRequestLegendApprove, "/request/approve_legend/{rid}", "POST", api.SetCheckForRequest},
			{ApiActionItemEdit, "/items/{id}", "POST", api.ItemCreateOrUpdate},
			{ApiActionLocationEdit, "/location/{id}", "POST", api.LocationCreateOrUpdate},
			{ApiActionGpsAdd, "/gps", "POST", api.GPSCreateOrUpdate},
			{ApiActionChangeLogProcessed, "/change_log/{id}", "POST", api.SetMembersChangeLogByIdAsProcessed},
		},
	}

	//add action routes
	groupsList := make([]models.UserGroup, len(api.actionRoutesMap))
	i := 0
	for k := range api.actionRoutesMap {
		groupsList[i] = k
		i++
	}
	actionsMW := []negroni.HandlerFunc{
		negroni.HandlerFunc(jwtMiddleware.CheckJWT),
		negroni.HandlerFunc(api.GetUserEnv),
		negroni.HandlerFunc(acl.NewAcl(groupsList...)),
	}
	for _, actionRoutesList := range api.actionRoutesMap {
		router.HandleActions(api.router, wrapper, actionsAPIPrefix, api.makeRoutesFromActionRoutes(actionsMW, actionRoutesList))
	}
}

func (api *API) startServe() {
	log.Infof("Listening on port %d", api.config.Service.ListenOnPort)
	err := http.ListenAndServe(fmt.Sprintf(":%d", api.config.Service.ListenOnPort), api.router)
	if err != nil {
		log.Fatalf("Cannot run API service: %s", err.Error())
	}
}

func (API) makeRoutesFromActionRoutes(mw []negroni.HandlerFunc, arls []ActionRoute) (routes []router.Route) {
	for _, arl := range arls {
		routes = append(routes, router.Route{
			Path:       arl.Path,
			Method:     arl.Method,
			Func:       arl.Func,
			Middleware: mw,
		})
	}

	return routes
}

func (API) getCurrentUserId(r *http.Request) (uint, *resperrors.Error) {
	ctx := r.Context()
	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Error("Somehow user_id is nil. Probably bad middleware was not called!")
		return 0, resperrors.New(resperrors.ErrNotAllowed)
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)

	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		return 0, resperrors.New(resperrors.ErrNotAllowed)
	}

	return uint(uid), nil
}
