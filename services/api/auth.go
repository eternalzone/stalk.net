package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"encoding/base64"
	"encoding/json"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"net/http"
	"strings"
)

func (api *API) Login(w http.ResponseWriter, r *http.Request) {
	var data apimodels.LoginParams
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		log.Errorf("login params parse error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrBadRequest, "invalid request json"))
		return
	}

	data.Email = strings.ToLower(data.Email)

	if data.Email == "" {
		log.Debug("email is empty or not specified")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "email"))
		return
	}

	err = helpers.ValidateEmail(data.Email)
	if err != nil {
		log.Debug("email has invalid format")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "email"))
		return
	}

	if data.Password == "" {
		log.Debug("password is empty or not specified")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "password"))
		return
	}

	ctx := r.Context()
	userEnv, isUserEnv := ctx.Value("userEnv").(apimodels.UserEnv)
	if !isUserEnv {
		log.Error("Somehow userEnv is empty. Probably middleware was not called or bad auth!!")
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	var user models.User

	res := api.db.Where(models.User{Email: data.Email}).First(&user)
	if res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "user"))
			return
		}
		log.Errorf("cannot load user by email %s: %s", data.Email, res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	//check password
	err = bcrypt.CompareHashAndPassword([]byte(user.PassHash), []byte(data.Password))
	if err != nil {
		log.Errorf("CompareHashAndPassword error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "password"))
		return
	}

	accessToken, refreshToken, err := api.GenerateAuthTokens(user, userEnv)
	if err != nil {
		log.Errorf("GenerateAccessTokenForUser error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	tokens := map[string]string{
		"user_group":    string(user.Group),
		"access_token":  accessToken,
		"refresh_token": refreshToken,
	}

	response.Json(w, tokens)
	return
}

func (api *API) RestorePasswordRequest(w http.ResponseWriter, r *http.Request) {
	var data apimodels.RestoreByEmail
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrBadRequest, "invalid request json"))
		return
	}

	data.Email = strings.ToLower(data.Email)

	if data.Email == "" {
		log.Debug("email is empty or not specified")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "email"))
		return
	}

	err = helpers.ValidateEmail(data.Email)
	if err != nil {
		log.Debug("email has invalid format")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "email"))
		return
	}

	var user models.User

	res := api.db.Where(models.User{Email: data.Email}).First(&user)
	if res.Error != nil {
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "email"))
			return
		}
		log.Errorf("cannot load user by email %s: %s", data.Email, res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	tokenByte, err := bcrypt.GenerateFromPassword([]byte(user.PassHash), bcrypt.DefaultCost)
	if err != nil {
		log.Errorf("GenerateFromPassword error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	tokenBased := base64.StdEncoding.EncodeToString(tokenByte)

	err = api.mail.CreatePasswordRestoreEmail(api.db, user, api.config.SmtpSystem.FromEmail, mailer.PasswordRestoreEmailFields{
		Link: api.config.FrontendHost + "/#/restore/" + user.Uuid.String() + "/" + tokenBased,
	})
	if err != nil {
		log.Errorf("cannot CreatePasswordRestoreEmail: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreatePasswordRestoreEmail"))
		return
	}

	response.Json(w, map[string]interface{}{
		"email:": data.Email,
	})
	return
}
