package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"time"
)

var (
	hiddenForCreateApiKeysFields = make([]string, 0)
	hiddenApiKeysFields          = []string{
		"Key",
	}
)

func (api *API) APIKeyCreateOrUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userId, isUserEnv := ctx.Value(middleware.ContextUserIdKey).(float64)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err error
		res *gorm.DB
		key models.APIKey
	)

	var keycu apimodels.APIKeyCreateOrUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&keycu)
	if err != nil {
		log.Errorf("can't decode JSON body for APIKeyCreateOrUpdate: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := keycu.Validate(); err != nil {
		log.Errorf("can't validate APIKeyCreateOrUpdate: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, err.Error()))
		return
	}
	//additional manual validation
	if !models.KeyType(keycu.KeyType).IsValid() {
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrBadParam, "%s key type is invalid", keycu.KeyType))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	if keycu.Id == 0 { //create the new one
		log.Debugf("Received API Key Create request with Id %d, Type %s, Name %s", keycu.Id, keycu.KeyType, keycu.Name)

		key.UUID = uuid.NewV4()
		key.Name = keycu.Name
		key.Type = models.KeyType(keycu.KeyType)
		key.Description = keycu.Desc
		key.Key = helpers.RandFullString(15)
		key.IsActive = true //we create only active new keys
		key.UserId = sql.NullInt64{
			Int64: int64(userId),
			Valid: true,
		}
		if keycu.ActiveTo > 0 {
			key.ActiveTo = sql.NullTime{
				Time:  time.Unix(keycu.ActiveTo, 0),
				Valid: true,
			}
		}

		res = DBTX.DB.Create(&key)
		if res.Error != nil {
			log.Errorf("Cannot create key to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "key creating"))
			return
		}
	} else { //or update
		log.Debugf("Received api key Update request with Id %d", keycu.Id)

		res = DBTX.DB.Where("key_id = ?", keycu.Id).First(&key)
		if res.Error != nil {
			log.Error("loading api key %d from DB error: %s", keycu.Id, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "api key loading"))
			return
		}

		key.Name = keycu.Name
		key.Type = models.KeyType(keycu.KeyType)
		key.Description = keycu.Desc
		key.IsActive = keycu.IsActive

		key.ActiveTo = sql.NullTime{
			Valid: false,
		}
		if keycu.ActiveTo > 0 {
			key.ActiveTo = sql.NullTime{
				Time:  time.Unix(keycu.ActiveTo, 0),
				Valid: true,
			}
		}

		res = DBTX.DB.Save(&key)
		if res.Error != nil {
			log.Errorf("Cannot update key to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "key updating"))
			return
		}
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit DB transaction: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	if keycu.Id == 0 {
		response.Json(w, api.MakeModelResponse(key, hiddenForCreateApiKeysFields))
	} else {
		response.Json(w, api.MakeModelResponse(key, hiddenApiKeysFields))
	}
	return
}

func (api *API) APIKeysLoad(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var keys []models.APIKey

	//TODO apply some filters
	res := api.db.Find(&keys).Limit(50) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading api keys from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "api keys loading"))
		return
	}

	for i := range keys {
		if keys[i].UserId.Valid && keys[i].UserId.Int64 > 0 {
			keys[i].User = new(models.User)
			api.db.Model(&keys[i]).Association("User").Find(keys[i].User)
		}
	}

	response.Json(w, api.MakeModelResponseList(keys, hiddenApiKeysFields))
	return
}

func (api *API) GetApiKeyById(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	keyId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad api key id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var apiKey models.APIKey
	res := api.db.Where("key_id = ?", keyId).First(&apiKey)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("api key with provided id %d was not found", keyId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(keyId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading api key from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "api key loading"))
		return
	}

	if apiKey.UserId.Valid && apiKey.UserId.Int64 > 0 {
		apiKey.User = new(models.User)
		api.db.Model(&apiKey).Association("User").Find(apiKey.User)
	}

	response.Json(w, api.MakeModelResponse(apiKey, hiddenApiKeysFields))
	return
}
