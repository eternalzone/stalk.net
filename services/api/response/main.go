package response

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
)

// Json writes to ResponseWriter a single JSON-object
func Json(w http.ResponseWriter, data interface{}) {
	js, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(js)
	if err != nil {
		log.Error("Write to ResponseWriter error: %s", err)
		JsonError(w, resperrors.New(resperrors.ErrService, "ResponseWriter"))
		return
	}
}

// JsonError writes to ResponseWriter error
func JsonError(w http.ResponseWriter, err error) {
	var e *resperrors.Error
	var ok bool

	if e, ok = err.(*resperrors.Error); !ok {
		e = resperrors.FromError(err)
	}

	js, _ := json.Marshal(e.ToMap())
	w.WriteHeader(e.GetHttpCode())
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(js)
	if err != nil {
		log.Error("Write to ResponseWriter error: %s", err)
		JsonError(w, resperrors.New(resperrors.ErrService, "ResponseWriter"))
		return
	}
	return
}
