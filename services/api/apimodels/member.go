package apimodels

import (
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/wedancedalot/decimal"
	"regexp"
	"unicode/utf8"
)

type (
	UserCreate struct {
		Email             string `json:"email"`
		Password          string `json:"password"`
		Phone             string `json:"phone,omitempty"`
		FirstName         string `json:"first_name"`
		LastName          string `json:"last_name"`
		FatherName        string `json:"father_name,omitempty"`
		BornAt            string `json:"born_at"`
		CityId            uint   `json:"city_id"`
		Command           string `json:"command"`
		Contraindications string `json:"contraindications,omitempty"`
	}

	MemberCreate struct {
		Callsign string `json:"callsign"`
		Legend   string `json:"legend"`

		FractionId uint `json:"fraction_id,omitempty"`
		SuitId     uint `json:"suit_id"`

		PassportPhotoId uint   `json:"passport_photo_id"`
		PhotosIds       []uint `json:"photos_ids,omitempty"`

		//Rank           string `json:"rank,omitempty"`
		//PassportCode   string `json:"passport_code,omitempty"`
		//LegendMaster   string `json:"legend_master,omitempty"`
		//Comment        string `json:"comment,omitempty"`
		//PayedAmountUAH string `json:"payed_amount_uah,omitempty"`
		//Money          string `json:"money,omitempty"`
		//Experience     string `json:"experience,omitempty"`
	}

	MemberUpdate struct {
		Legend          *string `json:"legend"`
		FractionId      *uint   `json:"fraction_id,omitempty"`
		SuitId          *uint   `json:"suit_id"`
		PassportPhotoId *uint   `json:"passport_photo_id"`
		PhotosIds       []uint  `json:"photos_ids,omitempty"`
		Rank            *string `json:"rank,omitempty"`
		LegendMaster    *string `json:"legend_master,omitempty"`
		Level           *uint8  `json:"level,omitempty"`
		Money           *int64  `json:"money,omitempty"`
		Experience      *uint64 `json:"experience,omitempty"`
	}

	MemberUpdateData struct {
		Rank       *string `json:"rank,omitempty"`
		Level      *uint8  `json:"level,omitempty"`
		Money      *int64  `json:"money,omitempty"`
		Experience *uint64 `json:"experience,omitempty"`
		Data       *string `json:"data,omitempty"`
	}

	EventRequest struct {
		LeaderCandidate string `json:"leader_candidate,omitempty"`
		Pda             string `json:"pda,omitempty"`
		ShotLight       string `json:"shot_light,omitempty"`
		HasPassport     bool   `json:"has_passport,omitempty"`
	}

	RegistrationData struct {
		UserCreate
		MemberCreate
		EventRequest
	}

	RequestSetCheck struct {
		IsLookOk   bool `json:"is_look_ok,omitempty"`
		IsLegendOk bool `json:"is_legend_ok,omitempty"`
	}

	RequestAction struct {
		Comment string `json:"comment,omitempty"`
	}

	RequestApprove struct {
		RequestAction
		DiscountPrice decimal.Decimal `json:"discountPrice"`
		AfterRegPrice decimal.Decimal `json:"afterRegPrice"`
		Price         decimal.Decimal `json:"price"`
	}

	RequestComment struct {
		RequestAction
	}
	RequestDecline struct {
		RequestAction
	}

	RequestPaid struct {
		RequestAction
		PaidAmount       decimal.Decimal `json:"paidAmount"`
		PaymentType      string          `json:"paymentType"`
		ForceFullPayment bool            `json:"forceFullPayment"`
	}

	PassportImages struct {
		FrontImg  string `json:"FrontImg"`
		BackImg   string `json:"BackImg"`
		QRCodeImg string `json:"QRCodeImg"`

		FrontImgSys  string `json:"-"`
		BackImgSys   string `json:"-"`
		QRCodeImgSyS string `json:"-"`
	}

	QRData struct {
		Id      string `json:"mid"`
		Code    string `json:"mcode"`
		Version byte   `json:"v"`
	}
)

const (
	CallsignRegex     = `^[а-яА-Я ]+$`
	CallsignMinLength = 2
	CallsignMaxLength = 50
	LegendMaxLength   = 1800
)

func (rd *RegistrationData) Validate() error {
	err := rd.UserCreate.Validate()
	if err != nil {
		return fmt.Errorf("UserCreate.Validate() error: %s", err.Error())
	}

	err = rd.MemberCreate.Validate()
	if err != nil {
		return fmt.Errorf("MemberCreate.Validate() error: %s", err.Error())
	}

	err = rd.EventRequest.Validate()
	if err != nil {
		return fmt.Errorf("EventRequest.Validate() error: %s", err.Error())
	}

	return nil
}

func (mr *UserCreate) Validate() error {
	if !govalidator.IsEmail(mr.Email) {
		return fmt.Errorf("%s is not an email", mr.Email)
	}

	return nil
}

func (mr *MemberCreate) Validate() error {
	if utf8.RuneCountInString(mr.Callsign) < CallsignMinLength {
		return fmt.Errorf("%s callsign is too short (has %d, max %d)", mr.Callsign, utf8.RuneCountInString(mr.Callsign), CallsignMinLength)
	}
	if utf8.RuneCountInString(mr.Callsign) > CallsignMaxLength {
		return fmt.Errorf("%s callsign is too long (has %d, max %d)", mr.Callsign, utf8.RuneCountInString(mr.Callsign), CallsignMaxLength)
	}
	var callsignRegex = regexp.MustCompile(CallsignRegex)
	if !callsignRegex.MatchString(mr.Callsign) {
		return fmt.Errorf("%s callsign is not valid regex (%s)", mr.Callsign, CallsignRegex)
	}

	if utf8.RuneCountInString(mr.Legend) > LegendMaxLength {
		return fmt.Errorf("legend is too long, maximum -- %d symbols", LegendMaxLength)
	}

	return nil
}

func (mr *EventRequest) Validate() error {
	//TODO validate something here
	return nil
}

func (mu MemberUpdate) Validate() error {
	if mu.Legend != nil && utf8.RuneCountInString(*mu.Legend) > LegendMaxLength {
		return fmt.Errorf("legend is too long, maximum -- %d symbols", LegendMaxLength)
	}
	//TODO validate something else here
	return nil
}

func (mud MemberUpdateData) Validate() error {
	//TODO validate something here
	return nil
}
