package apimodels

import (
	"fmt"
	"unicode/utf8"
)

type (
	LocationCreateOrUpdate struct {
		Id       uint64   `json:"Id"`
		Title    string   `json:"Title"`
		Desc     string   `json:"Desc"`
		Type     string   `json:"Type"`
		Zone     string   `json:"Zone"`
		GPSId    int64    `json:"GPSId"`
		IsActive bool     `json:"IsActive,omitempty"`
		Photos   []string `json:"photos,omitempty"`
	}

	GPSCreateOrUpdate struct {
		Id        uint64  `json:"Id"`
		Desc      string  `json:"Desc"`
		Accuracy  float64 `json:"Accuracy"`
		Latitude  float64 `json:"Latitude"`
		Longitude float64 `json:"Longitude"`
		Speed     float64 `json:"Speed"`
		IsActive  bool    `json:"IsActive,omitempty"`
	}
)

func (lcou *LocationCreateOrUpdate) Validate() error {
	if utf8.RuneCountInString(lcou.Title) < 3 {
		return fmt.Errorf("%s title is too short (has %d, max %d)", lcou.Title, utf8.RuneCountInString(lcou.Title), 3)
	}
	return nil
}

func (gcou *GPSCreateOrUpdate) Validate() error {
	//TODO validate something here

	return nil
}
