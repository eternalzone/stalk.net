package apimodels

import (
	"fmt"
	"unicode/utf8"
)

type (
	ItemCreateOrUpdate struct {
		Id         uint64 `json:"Id"`
		Title      string `json:"Title"`
		Desc       string `json:"Desc"`
		ItemType   string `json:"ItemType"`
		Owner      string `json:"Owner"`
		Place      string `json:"Place"`
		IsVirtual  bool   `json:"IsVirtual,omitempty"`
		IsActive   bool   `json:"IsActive,omitempty"`
		Comment    string `json:"Comment"`
		Image      string `json:"Image"`
		Data       string `json:"Data"`
		LocationId uint   `json:"LocationId"`
		GPSId      uint   `json:"GPSId"`
	}
)

func (icou *ItemCreateOrUpdate) Validate() error {
	if utf8.RuneCountInString(icou.Title) < 3 {
		return fmt.Errorf("%s title is too short (has %d, max %d)", icou.Title, utf8.RuneCountInString(icou.Title), 3)
	}
	return nil
}
