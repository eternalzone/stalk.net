package apimodels

import (
	"github.com/satori/go.uuid"
)

type LoginParams struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
	Captcha  string `json:"captcha"`
}

type RestoreByEmail struct {
	Email string `json:"email"`
}

type ChangeUserPassword struct {
	UserUUID uuid.UUID `json:"user_uuid"`
	Token    string    `json:"token"`
	Old      string    `json:"old"`
	New      string    `json:"new"`
	NewAgain string    `json:"new_again"`
}

type UserEnv struct {
	UserUuid    *uuid.UUID             `json:"-"`
	IpAddress   string                 `json:"-"`
	HttpReferer string                 `json:"referer"`
	UTM         map[string]interface{} `json:"utm"`
	CookieId    string                 `json:"cookie_id"`
	BrowserId   string                 `json:"browser_id"`
	UserAgent   string                 `json:"-"`
	Locale      string                 `json:"locale"`
	Encodings   string                 `json:"encoding"`
	LocalTime   string                 `json:"local_time"`
	Json        string                 `json:"json"`
	LandingUrl  string                 `json:"landing_url"`
}
