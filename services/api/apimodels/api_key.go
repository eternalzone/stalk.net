package apimodels

type (
	APIKeyCreateOrUpdate struct {
		Id       uint64 `json:"Id"`
		Name     string `json:"Name"`
		KeyType  string `json:"KeyType"`
		Desc     string `json:"Desc"`
		IsActive bool   `json:"IsActive"`
		ActiveTo int64  `json:"ActiveTo"`
	}
)

func (apcou *APIKeyCreateOrUpdate) Validate() error {
	return nil
}
