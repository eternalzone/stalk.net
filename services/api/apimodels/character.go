package apimodels

import (
	"fmt"
	"unicode/utf8"
)

type (
	CharacterCreateOrUpdate struct {
		Id         uint64 `json:"Id"`
		Name       string `json:"Name"`
		Image      string `json:"Image"`
		Legend     string `json:"Legend"`
		MembersIds []uint `json:"MembersIds"`
		IsActive   bool   `json:"IsActive"`
	}
)

func (ccou *CharacterCreateOrUpdate) Validate() error {
	if utf8.RuneCountInString(ccou.Name) < 3 {
		return fmt.Errorf("%s title is too short (has %d, max %d)", ccou.Name, utf8.RuneCountInString(ccou.Name), 3)
	}
	return nil
}
