package api

import (
	"errors"
	"gorm.io/gorm"
	"net/http"
	"unicode/utf8"

	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
)

func (api *API) CitiesList(w http.ResponseWriter, r *http.Request) {
	var cities = make([]*models.City, 0)

	filter := r.URL.Query().Get("filter")
	if utf8.RuneCountInString(filter) < 3 {
		response.Json(w, cities)
		return
	}

	var city models.City
	res := api.db.Where("cty_name = ?", filter).First(&city)
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrService, "cities loading"))
		return
	}
	if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		cities = append(cities, &city)
		response.Json(w, cities)
		return
	}

	api.db.Where("cty_name LIKE ?", "%"+filter+"%").Find(&cities)

	response.Json(w, cities)
}
