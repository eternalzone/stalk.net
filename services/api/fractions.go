package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"errors"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func (api *API) LoadFractionsList(w http.ResponseWriter, r *http.Request) {
	showAll, _ := strconv.ParseBool(r.URL.Query().Get("show_all"))

	log.Infof("showAll %v", showAll)

	event, err := api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetLastEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var fractionsEv []models.FractionsEvent
	res := api.db.Where("fev_is_active = ? AND evt_id = ? AND fev_max_members > 0", true, event.Id).Find(&fractionsEv)
	if res.Error != nil {
		log.Error("loading fractions events from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "fractions events loading"))
		return
	}

	var resp = make([]map[string]interface{}, 0)
	for i := range fractionsEv { //prepare data
		var fraction models.Fraction //do not touch this or use some pointer with nilling at the end of func (data in Find used somehow as WHERE clauses)
		res := api.db.Where("frc_id = ? AND frc_is_active = ?", fractionsEv[i].FractionId, true).First(&fraction)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			continue
		}
		if res.Error != nil {
			log.Error("loading fraction from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "fraction loading"))
			return
		}

		if showAll || (fractionsEv[i].MembersTotal < fractionsEv[i].MembersMax && fractionsEv[i].RequestsTotal < fractionsEv[i].MembersMax) {
			api.db.Model(&fraction).Association("Image").Find(&fraction.Image)
			resp = append(resp, map[string]interface{}{
				"id":           fraction.Id,
				"title":        fraction.Title,
				"desc":         fraction.Desc,
				"img":          helpers.GetImagePath(r, fraction.Image.Name),
				"places_left":  fractionsEv[i].MembersMax - fractionsEv[i].RequestsTotal,
				"places_total": fractionsEv[i].MembersTotal,
				"is_empty":     fractionsEv[i].MembersTotal == 0,
			})
		}
	}

	response.Json(w, resp)
	return
}
