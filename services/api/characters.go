package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

var (
	hiddenForAdminCharactersFields = []string{}

	hiddenCharactersFields = []string{
		"Id",
	}
)

func (api *API) CharactersList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var characters []models.Character

	//TODO apply some filters
	res := api.db.Where("chr_is_active = ?", true).Order("chr_id DESC").Find(&characters).Limit(50) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading characters from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "characters loading"))
		return
	}

	for i := range characters {
		api.db.Model(&characters[i]).Association("Image").Find(&characters[i].Image)
		api.db.Model(&characters[i]).Association("Members").Find(&characters[i].Members)
	}

	response.Json(w, api.MakeModelResponseList(characters, hiddenForAdminCharactersFields))
}

func (api *API) CharacterCreateOrUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err       error
		res       *gorm.DB
		image     models.Image
		character models.Character
	)

	var ccu apimodels.CharacterCreateOrUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&ccu)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := ccu.Validate(); err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, err.Error()))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	if ccu.Image != "" { //replace image only if needs
		image, err = api.createNewImageFromBase64(DBTX, ccu.Image, models.ImageAvatar)
		if err != nil {
			log.Errorf("Cannot createNewImageFromBase64: %s", err.Error())
			response.JsonError(w, err)
			return
		}
	}

	if ccu.Id == 0 { //create the new one
		log.Debugf("Received character Create request with Id %d, Name %s", ccu.Id, ccu.Name)

		character.Name = ccu.Name
		character.Legend = ccu.Legend
		character.IsActive = ccu.IsActive

		if image.Id > 0 {
			character.ImageId = image.Id
		}

		res = DBTX.DB.Create(&character)
		if res.Error != nil {
			log.Errorf("Cannot create character to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "character creating"))
			return
		}

		//associate members
		for _, mid := range ccu.MembersIds {
			mi := models.CharacterMember{
				CharacterId: character.Id,
				MemberId:    mid,
			}

			res = DBTX.DB.Create(&mi)
			if res.Error != nil {
				log.Errorf("Cannot create CharacterMember to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "CharacterMember assoc creating"))
				return
			}
		}
	} else { //or update
		log.Debugf("Received character Update request with Id %d, Name %s", ccu.Id, ccu.Name)

		res = DBTX.DB.Where("chr_id = ?", ccu.Id).First(&character)
		if res.Error != nil {
			log.Error("loading character %d from DB error: %s", ccu.Id, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "character loading"))
			return
		}
		api.db.Model(&character).Association("Members").Find(&character.Members)

		character.Name = ccu.Name
		character.Legend = ccu.Legend
		character.IsActive = ccu.IsActive

		if image.Id > 0 {
			character.ImageId = image.Id
		}

		res = DBTX.DB.Save(&character)
		if res.Error != nil {
			log.Errorf("Cannot update character to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "character updating"))
			return
		}

		//re-associate members
		var membersToAdd []uint
		var membersToDelete []uint

		//find members to add
		for _, mid := range ccu.MembersIds {
			for i := range character.Members { //find if members exists
				if character.Members[i].Id == mid {
					continue //do nothing if already exists
				}

				membersToAdd = append(membersToAdd, mid)
			}
		}

		//find members to delete
		for i := range character.Members {
			for _, mid := range ccu.MembersIds {
				if character.Members[i].Id == mid {
					continue //do nothing if already exists
				}

				membersToDelete = append(membersToDelete, mid)
			}
		}

		//add new members
		for _, mid := range membersToAdd {
			mi := models.CharacterMember{
				CharacterId: character.Id,
				MemberId:    mid,
			}

			res = DBTX.DB.Create(&mi)
			if res.Error != nil {
				log.Errorf("Cannot create CharacterMember to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "CharacterMember assoc creating"))
				return
			}
		}

		//remove members
		if len(membersToDelete) > 0 {
			res = DBTX.DB.Where("mbr_id IN (?)", membersToDelete).Delete(&models.CharacterMember{})
			if res.Error != nil {
				log.Errorf("Cannot delete CharacterMember in DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "CharacterMember assoc removing"))
				return
			}
		}
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit DB transaction: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, api.MakeModelResponse(character, hiddenForAdminCharactersFields))
	return
}

func (api *API) GetCharacterByIdAdmin(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	characterId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad character id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var character models.Character
	res := api.db.Where("chr_id = ?", characterId).First(&character)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("character with provided id %d was not found", characterId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(characterId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading character from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "character loading"))
		return
	}

	api.db.Model(&character).Association("Image").Find(&character.Image)
	api.db.Model(&character).Association("Members").Find(&character.Members)

	response.Json(w, api.MakeModelResponse(character, hiddenForAdminCharactersFields))
	return
}

func (api *API) GetCharacterById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	characterId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad character id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var character models.Character
	res := api.db.Where("chr_id = ?", characterId).First(&character)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("character with provided id %d was not found", characterId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(characterId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading character from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "character loading"))
		return
	}

	api.db.Model(&character).Association("Image").Find(&character.Image)

	response.Json(w, api.MakeModelResponse(character, hiddenCharactersFields))
	return
}
