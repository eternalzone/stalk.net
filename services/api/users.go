package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"time"
	"unicode/utf8"
)

var (
	hiddenForAdminUserFields = []string{
		"PassHash",
	}

	hiddenUserFields = []string{
		"PassHash",
	}
)

func (api *API) CheckEmailIsFree(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")

	if utf8.RuneCountInString(email) < 3 {
		log.Warn("provided email is too short")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "email"))
		return
	}

	var user models.User

	err := api.db.Where("usr_email = ?", email).First(&user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		response.Json(w, true)
		return
	}
	if err != nil {
		log.Errorf("loading user from DB error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "user loading"))
		return
	}

	response.Json(w, false)
	return
}

func (api *API) registerNewUser(DBTX dao.Transaction, uc apimodels.UserCreate) (models.User, error) {
	var user models.User

	userUuid := uuid.NewV4()

	//check uuid, email to be unique
	res := DBTX.DB.Where("usr_uuid = ? OR usr_email = ?",
		userUuid, uc.Email,
	).First(&user)
	if !errors.Is(res.Error, gorm.ErrRecordNotFound) && res == nil {
		log.Warnf("Duplicate user was found by uuid %s or email %s", userUuid.String(), uc.Email)
		return user, resperrors.New(resperrors.ErrAlreadyExists, "user")
	}
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Errorf("Cannot load user for check from DB: %s", res.Error.Error())
		return user, resperrors.New(resperrors.ErrService)
	}

	passHashByte, err := bcrypt.GenerateFromPassword([]byte(uc.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Errorf("GenerateFromPassword error: %s", err.Error())
		return user, resperrors.New(resperrors.ErrService)
	}

	bornAt, err := time.Parse("02.01.2006", uc.BornAt)
	if err != nil {
		log.Errorf("Bad born at date provided %s: %s", uc.BornAt, err.Error())
		return user, resperrors.New(resperrors.ErrBadParam, "born_at")
	}

	user = models.User{
		Uuid:              userUuid,
		Email:             uc.Email,
		PassHash:          string(passHashByte),
		FirstName:         uc.FirstName,
		LastName:          uc.LastName,
		FatherName:        helpers.ToNullString(uc.FatherName),
		Phone:             helpers.ToNullString(uc.Phone),
		Group:             models.UserMember,
		CityId:            uc.CityId,
		BornAt:            helpers.ToNullTime(bornAt),
		Contraindications: helpers.ToNullString(uc.Contraindications),
	}
	//process command string
	if uc.Command != "" {
		var command models.Command
		res := DBTX.DB.Where("cmd_title = ?",
			uc.Command,
		).First(&command)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			//create api command
			command = models.Command{
				Title:   uc.Command,
				ImageId: helpers.ToNullInt64NonZero(0),
			}
			res = DBTX.DB.Create(&command)
		}
		if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Errorf("Cannot process command from DB: %s", res.Error.Error())
			return user, resperrors.New(resperrors.ErrService)
		}

		user.CommandId = command.Id
	}

	res = DBTX.DB.Create(&user)
	if res.Error != nil {
		log.Errorf("Cannot create user to DB: %s", res.Error.Error())
		return user, resperrors.New(resperrors.ErrService)
	}

	log.Debugf("Register new user with uuid=%d", user.Uuid)

	return user, nil
}

func (api *API) GetUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	userUuid := uuid.FromStringOrNil(r.URL.Query().Get("uuid"))

	if userUuid == uuid.Nil {
		ctx := r.Context()

		if userCtx, ok := ctx.Value(middleware.ContextUserUuidKey).(string); ok {
			userUuid = uuid.FromStringOrNil(userCtx)
		}

		if userUuid == uuid.Nil {
			log.Error("Somehow userEnv.UserUuid is nil. Probably bad middleware was not called!")
			response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
			return
		}
	}

	var user models.User
	res := api.db.Where("usr_uuid = ?", userUuid.String()).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("User with provided uuid %s was not found", userUuid.String())
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, userUuid.String()))
		return
	}
	if res.Error != nil {
		log.Errorf("loading user from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(user, hiddenForAdminUserFields))
		return
	}

	response.Json(w, api.MakeModelResponse(user, hiddenForAdminUserFields))
	return
}

func (api *API) GetUserByEventRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad user id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var me models.MembersEvent
	res := api.db.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(requestId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading"))
		return
	}

	api.db.Model(&me).Association("Member").Find(&me.Member)

	var user models.User
	res = api.db.Where("usr_id = ?", me.Member.UserId).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("user with provided id %d was not found", me.Member.UserId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.Member.UserId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading user from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	api.db.Model(&user).Association("City").Find(&user.City)
	api.db.Model(&user).Association("Command").Find(&user.Command)

	response.Json(w, api.MakeModelResponse(user, hiddenForAdminUserFields))
	return
}

func (api *API) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var data apimodels.ChangeUserPassword
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrBadRequest, "invalid request json"))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var user models.User
	res := DBTX.DB.Where("usr_uuid = ?", data.UserUUID).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("user with provided uuid %s was not found", data.UserUUID.String())
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, data.UserUUID.String()))
		return
	}
	if res.Error != nil {
		log.Errorf("loading user from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "user loading"))
		return
	}

	//validate token if provided
	if data.Token != "" {
		decodedToken, err := base64.StdEncoding.DecodeString(data.Token)
		if err != nil {
			log.Errorf("decoding token from base64 error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "token finding"))
			return
		}
		logrus.Println(string(decodedToken))
		err = bcrypt.CompareHashAndPassword(decodedToken, []byte(user.PassHash))
		if err != nil {
			log.Errorf("CompareHashAndPassword error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrBadAuth, "password"))
			return
		}
	} else {
		//check password
		err = bcrypt.CompareHashAndPassword([]byte(user.PassHash), []byte(data.Old))
		if err != nil {
			log.Errorf("CompareHashAndPassword error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrBadAuth, "password"))
			return
		}
	}

	if data.New != data.NewAgain {
		log.Errorf("new user provided passwords differs")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "new password"))
		return
	}

	passHashByte, err := bcrypt.GenerateFromPassword([]byte(data.New), bcrypt.DefaultCost)
	if err != nil {
		log.Errorf("GenerateFromPassword error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	user.PassHash = string(passHashByte) //set new password

	res = DBTX.DB.Save(&user)
	if res.Error != nil {
		log.Errorf("updating user password in DB DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"user_uuid:": user.Uuid.String(),
	})
	return
}
