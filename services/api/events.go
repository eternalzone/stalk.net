package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"errors"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"net/http"
)

var (
	hiddenForAdminEventsFields = []string{}

	hiddenEventsFields = []string{
		//"PriceUAH",
		//"DiscountUntil",
		//"DiscountPriceUAH",
		//"DiscountVariants",
		"CreatedAt",
		"UpdatedAt",
	}
)

func (api *API) FindActiveRegistrationEvent(w http.ResponseWriter, r *http.Request) {
	event, err := api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetLastEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	evt, err := api.dao.GetLastEvent()
	if err != nil {
		log.Error("GetLastEvent error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "GetLastEvent loading"))
		return
	}
	if evt == nil {
		logrus.Warn("no active event found")
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	//calculate members info
	var fractions []*models.Fraction
	res := api.db.Where("frc_is_active = true").Find(&fractions)
	if res.Error != nil {
		log.Error("loading fractions from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "fractions loading"))
		return
	}

	//make IN array
	frIds := make([]uint, len(fractions))
	for i := range fractions {
		if fractions[i] == nil {
			continue //just to make sure :)
		}

		frIds[i] = fractions[i].Id
	}

	var fevs []*models.FractionsEvent
	res = api.db.Where("frc_id IN (?) AND evt_id = ? AND fev_is_active = ?", frIds, event.Id, true).Find(&fevs)
	logrus.Debugf("%+v", res.Error)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(fevs) == 0 {
		logrus.Warn("no active fraction event found")
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}
	if res.Error != nil {
		log.Error("loading fractions events from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "fractions events loading"))
		return
	}

	for i := range fevs { //prepare data
		if fevs[i] == nil {
			continue //just to make sure :)
		}

		event.MembersInfo.Max += uint64(fevs[i].MembersMax)
		event.MembersInfo.Requests += uint64(fevs[i].RequestsTotal)
		event.MembersInfo.Total += uint64(fevs[i].MembersTotal)
	}

	//disable reg if members full
	if event.MembersInfo.Total >= event.MembersInfo.Max || event.MembersInfo.Requests >= event.MembersInfo.Max {
		logrus.Debug("fraction full")
		event.IsRegistrationOpened = false
	}

	response.Json(w, api.MakeModelResponse(*event, hiddenEventsFields))
	return
}
