package acl

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"context"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

// Acl http handler
type Acl struct {
	allowedGroups []models.UserGroup
}

// NewAcl initialises a new ACL middleware by array of UserGroups and Check it
func NewAcl(groups ...models.UserGroup) func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	acl := &Acl{groups}
	return acl.Check
}

// Check is Negroni compatible interface
func (this *Acl) Check(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	authHeader := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(authHeader) != 2 {
		log.Debugf("JWT is empty or have invalid structure, token: %s", r.Header.Get("Authorization"))
		response.JsonError(w, resperrors.New(resperrors.ErrBadJwt))
		return
	}

	token, _ := jwt.Parse(authHeader[1], func(token *jwt.Token) (interface{}, error) {
		return []byte(""), nil
	})

	if token == nil {
		log.Errorf("Can not parse token headers")
		response.JsonError(w, resperrors.New(resperrors.ErrBadJwt))
		return
	}

	if groupStr, ok := token.Header["group"]; ok {

		role := models.UserGroup(groupStr.(string))

		var isAllowed bool
		for _, allowedRole := range this.allowedGroups {
			if role == allowedRole {
				isAllowed = true
				break
			}
		}

		if !isAllowed {
			log.Warnf("somebody tries not allowed action: %+v", token.Header)
			response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed))
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, "token", token)
		r = r.WithContext(ctx)

		next(w, r)
		return
	}

	log.Errorf("Header \"role\" not presented in JWT")
	response.JsonError(w, resperrors.New(resperrors.ErrBadJwt))
}
