package middleware

const UserUuidHeader = "user_uuid"
const UserIdHeader = "user_id"
const UserGroupHeader = "group"

const ContextUserUuidKey = "user_uuid"
const ContextUserIdKey = "user_id"
const ContextUserGroupKey = "group"

const ContextAPIKey = "api_key"
