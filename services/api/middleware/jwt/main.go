package jwt

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"context"
	"crypto/ecdsa"
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

const authorizationHeader = "Authorization"

// Jwt http handler
type Jwt struct {
	publicKey ecdsa.PublicKey
}

// NewJwtFromBase64 initialises Jwt structure by base64-encoded pem-key
func NewJwtFromBase64(pemBase64 string) (*Jwt, error) {
	pem, err := base64.StdEncoding.DecodeString(pemBase64)
	if err != nil {
		return nil, fmt.Errorf("unable to decode base64 ECDSA public key: %v", err)
	}

	return NewJwt(pem)
}

// NewJwt initialises Jwt structure by pem-key
func NewJwt(pem []byte) (*Jwt, error) {
	publicKey, err := jwt.ParseECPublicKeyFromPEM(pem)
	if err != nil {
		return nil, fmt.Errorf("unable to parse ECDSA public key: %v", err)
	}

	return &Jwt{*publicKey}, nil
}

func (this *Jwt) ParseAndCheckToken(t string) (*jwt.Token, error) {
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			log.Errorf("Unexpected signing method: %v", token.Header["alg"])
			return nil, fmt.Errorf("Bad JWT method")
		}

		return &this.publicKey, nil
	})

	if err != nil {
		return nil, fmt.Errorf("Can not parse JWT token, %v", err)
	}

	if _, ok := token.Claims.(jwt.MapClaims); !ok || !token.Valid {
		return nil, fmt.Errorf("JWT token is invalid")
	}

	return token, nil
}

func (this *Jwt) CheckJWT(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	ctx := r.Context()
	if userUuid, ok := ctx.Value(middleware.ContextUserUuidKey).(string); ok && userUuid != "" {
		log.Debugf("User uuid is already set. Skipping jwt middleware")
		next(w, r)
		return
	}

	authHeader := strings.SplitN(r.Header.Get(authorizationHeader), " ", 2)
	if len(authHeader) != 2 {
		log.Debugf("JWT is empty or have invalid structure, token: %s", r.Header.Get(authorizationHeader))
		next(w, r)
		return
	}

	token, err := this.ParseAndCheckToken(authHeader[1])
	if err != nil {
		log.Debugf(err.Error())
		next(w, r)
		return
	}

	if token == nil {
		log.Debugf("Token is empty")
		next(w, r)
		return
	}

	userUuid, ok := token.Header[middleware.UserUuidHeader]
	if !ok || userUuid.(string) == "" {
		log.Debugf("Header '%s' not presented in JWT", middleware.UserUuidHeader)
		next(w, r)
		return
	}
	ctx = context.WithValue(ctx, middleware.ContextUserUuidKey, userUuid)
	r = r.WithContext(ctx)

	userId, ok := token.Header[middleware.UserIdHeader]
	if !ok || userId.(float64) == 0 {
		log.Debugf("Header '%s' not presented in JWT", middleware.UserIdHeader)
		next(w, r)
		return
	}
	ctx = context.WithValue(ctx, middleware.ContextUserIdKey, userId)
	r = r.WithContext(ctx)

	userGroup, ok := token.Header[middleware.UserGroupHeader]
	if !ok || userGroup.(string) == "" {
		log.Debugf("Header '%s' not presented in JWT", middleware.UserGroupHeader)
		next(w, r)
		return
	}
	ctx = context.WithValue(ctx, middleware.ContextUserGroupKey, models.UserGroup(userGroup.(string)))
	r = r.WithContext(ctx)

	next(w, r)
}
