package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"context"
	"database/sql"
	"errors"
	"gorm.io/gorm"
	"net"
	"net/http"
	"reflect"
	"time"

	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"github.com/Nargott/goutils"
	"github.com/wedancedalot/decimal"
)

const RealIpHeader = "X-Forwarded-For"
const APIKeyHeader = "API-Key"
const APIKeyTypeHeader = "API-Key-Type"

func (api *API) GetUserEnv(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	ip := r.Header.Get(RealIpHeader)
	if ip == "" {
		ip = r.RemoteAddr
	}

	clearIp, _, err := net.SplitHostPort(ip)
	if err != nil {
		clearIp = ip
	}

	ctx := r.Context()
	r = r.WithContext(ctx)

	var userEnv apimodels.UserEnv

	userEnv.UserAgent = r.UserAgent()
	userEnv.IpAddress = clearIp

	ctx = context.WithValue(ctx, "userEnv", userEnv)
	r = r.WithContext(ctx)

	//userDataBase64 := r.Header.Get("X-User-Env")
	//if userDataBase64 == "" {
	//	next(w, r)
	//	return
	//}
	//
	//userDataJson, err := base64.StdEncoding.DecodeString(userDataBase64)
	//if err != nil {
	//	log.Warnf("Unable to decode base64 X-User-Env header: %s", err.Error())
	//	next(w, r)
	//	return
	//}
	//
	//userDataJsonBytes := []byte(userDataJson)
	//
	//err = json.Unmarshal(userDataJsonBytes, &userEnv)
	//if err != nil {
	//	log.Warnf("X-User-Env header is not valid json: %s", userDataJson)
	//	next(w, r)
	//	return
	//}

	//ctx = context.WithValue(ctx, "userEnv", &userEnv)
	//r = r.WithContext(ctx)

	next(w, r)
	return
}

func (api *API) MakeModelResponse(model interface{}, hiddenFields []string) (res map[string]interface{}) {
	modelReflect := reflect.ValueOf(model)

	res = make(map[string]interface{}, modelReflect.NumField())

	if modelReflect.Kind() == reflect.Ptr {
		modelReflect = modelReflect.Elem()
	}

	modelRefType := modelReflect.Type()
	fieldsCount := modelReflect.NumField()

	var fieldData interface{}

	for i := 0; i < fieldsCount; i++ {
		if goutils.IsStringInArray(modelRefType.Field(i).Name, hiddenFields) {
			continue
		}

		field := modelReflect.Field(i)

		fieldData = ""
		switch field.Type().String() {
		case "sql.NullFloat64":
			fieldData = field.Interface().(sql.NullFloat64).Float64
		case "sql.NullInt64":
			fieldData = field.Interface().(sql.NullInt64).Int64
		case "sql.NullString":
			fieldData = field.Interface().(sql.NullString).String
		case "sql.NullTime":
			if field.Interface().(sql.NullTime).Valid {
				fieldData = field.Interface().(sql.NullTime).Time.Unix()
			}
		case "decimal.Decimal":
			fieldData = field.Interface().(decimal.Decimal).String()
		case "time.Time":
			fieldData = field.Interface().(time.Time).Unix()
		default:
			if field.Kind() == reflect.Struct {
				fieldData = api.MakeModelResponse(field.Interface(), hiddenFields)
			} else {
				fieldData = field.Interface()
			}
		}

		res[modelRefType.Field(i).Name] = fieldData
	}

	return res
}

func (api *API) MakeModelResponseList(modelsList interface{}, hiddenFields []string) (res []map[string]interface{}) {
	modelReflect := reflect.ValueOf(modelsList)

	if modelReflect.Kind() != reflect.Slice {
		return []map[string]interface{}{api.MakeModelResponse(modelsList, hiddenFields)}
	}

	if modelReflect.Len() > 0 {
		modelsListInterface := make([]interface{}, modelReflect.Len())

		for i := 0; i < modelReflect.Len(); i++ {
			modelsListInterface[i] = modelReflect.Index(i).Interface()
		}

		for _, m := range modelsListInterface {
			res = append(res, api.MakeModelResponse(m, hiddenFields))
		}

		return res
	}

	res = make([]map[string]interface{}, 0) //workaround to return empty arrays, not null after json marshaller
	return
}

func (api *API) checkIfAdminGroup(group models.UserGroup) bool {
	switch group {
	case models.UserGameMaster,
		models.UserAdmin:
		return true
	}

	return false
}

func (api *API) CheckAPIKey(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	APIKey := r.Header.Get(APIKeyHeader)
	if APIKey == "" {
		log.Warnf("%s is empty", APIKeyHeader)
		next(w, r)
		return
	}

	APIKeyTypeS := r.Header.Get(APIKeyTypeHeader)
	if APIKeyTypeS == "" {
		log.Warnf("%s is empty", APIKeyTypeHeader)
		next(w, r)
		return
	}

	APIKeyType := models.KeyType(APIKeyTypeS)
	if !APIKeyType.IsValid() {
		log.Warnf("%s %s is not valid", APIKeyTypeHeader, APIKeyTypeS)
		next(w, r)
		return
	}

	var key models.APIKey
	res := api.db.Where(&models.APIKey{Key: APIKey}).First(&key)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Warnf("Api Key %s is not found", APIKey)
		next(w, r)
		return
	}
	if res.Error != nil {
		log.Errorf("Api Key %s loading error:", res.Error.Error())
		next(w, r)
		return
	}

	if !key.IsValid() {
		log.Warnf("Api Key %s is not valid anymore", APIKey)
		next(w, r)
		return
	}

	if !key.IsKeyTypeValidFor(APIKeyType) {
		log.Warnf("For Api Key %s wrong type %s provided, when accepts only %s", APIKey, APIKeyType, key.Type)
		next(w, r)
		return
	}

	ctx := r.Context()
	r = r.WithContext(ctx)

	ctx = context.WithValue(ctx, middleware.ContextAPIKey, key)
	r = r.WithContext(ctx)

	next(w, r)
	return
}
