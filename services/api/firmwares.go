package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func (api *API) FindDeviceData(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	APIKey, isAPIKey := ctx.Value(middleware.ContextAPIKey).(models.APIKey)
	if !isAPIKey {
		log.Errorf("AUTH: %s is empty. Probably middleware was not called or bad auth!", middleware.ContextAPIKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	if !APIKey.IsValid() {
		log.Errorf("Api Key %s is not valid", ctx.Value(middleware.ContextAPIKey).(models.APIKey).Key)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)
	deviceMAC := params["mac"]

	var deviceType = models.DeviceTypeSoul //default value
	if dt := r.URL.Query().Get("type"); dt != "" && models.DeviceType(dt).IsValid() {
		deviceType = models.DeviceType(dt)
	}

	var deviceSubType uint8 = 0 //default value
	if dst := r.URL.Query().Get("subtype"); dst != "" {
		dstParsed, err := strconv.ParseUint(dst, 10, 8)
		if err == nil {
			deviceSubType = uint8(dstParsed)
		}
	}

	//processing tag if provided
	tag := models.FirmwareTag{
		Title:    r.URL.Query().Get("tag"),
		IsActive: true,
	}
	if tag.Validate() == nil {
		res := api.db.Where("ftg_title = ?", tag.Title).First(&tag) //handle inactive tags?
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {           //no such tag found, create the new one
			log.Errorf("FindDeviceData tag not found: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "db tag not found"))
			return
		}
		if res.Error != nil {
			log.Errorf("loading firmware tag from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "db tag loading"))
			return
		}
	}

	var device models.Device
	device.MAC = deviceMAC
	if tag.Id > 0 {
		device.FirmwareTagId = helpers.ToNullInt64(int64(tag.Id))
	}
	err := device.Validate()
	if err != nil {
		log.Warnf("device (MAC address validation error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "MAC"))
		return
	}

	res := api.db.Where(device).First(&device)
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrService, "device loading"))
		return
	}
	if errors.Is(res.Error, gorm.ErrRecordNotFound) { //no such device was found, create the new one
		device = models.Device{
			UUID:       uuid.NewV4(),
			MAC:        deviceMAC,
			Type:       deviceType,
			SubType:    deviceSubType,
			IsActive:   true,
			FirmwareId: sql.NullInt64{Valid: false},
			Config:     nil,
			ActiveAt:   sql.NullTime{Time: time.Now(), Valid: true},
		}
		if tag.Id > 0 {
			device.FirmwareTagId = helpers.ToNullInt64(int64(tag.Id))
		}

		res = api.db.Create(&device)
		if res.Error != nil {
			log.Errorf("cannot create new device (MAC = %s) in DB: %s", deviceMAC, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "device creation"))
			return
		}
	}

	var fwParams models.FirmwareQueryParams
	if device.FirmwareId.Valid && device.FirmwareId.Int64 > 0 {
		fwParams.FirmwareId = uint(device.FirmwareId.Int64)
	}
	if device.FirmwareTagId.Valid && device.FirmwareTagId.Int64 > 0 {
		fwParams.FirmwareTagId = uint(device.FirmwareTagId.Int64)
	}
	fw, err := api.dao.GetLatestFirmware(r, device.GetFirmwareType(), fwParams)
	if err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrService, "latest firmware loading"))
		return
	}
	if fw != nil {
		device.LatestFirmware = fw
	}

	response.Json(w, device)
	return
}

func (api *API) GetLatestFirmwareVersion(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	APIKey, isAPIKey := ctx.Value(middleware.ContextAPIKey).(models.APIKey)
	if !isAPIKey {
		log.Errorf("AUTH: %s is empty. Probably middleware was not called or bad auth!", middleware.ContextAPIKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	if !APIKey.IsValid() {
		log.Errorf("Api Key %s is not valid", ctx.Value(middleware.ContextAPIKey).(models.APIKey).Key)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)
	fwType := models.FirmwareType(params["type"])

	tag := r.URL.Query().Get("tag")
	var fwParams models.FirmwareQueryParams
	if len(strings.TrimSpace(tag)) > 2 {
		fwParams.FirmwareTagTitle = tag
	}

	fw, err := api.dao.GetLatestFirmware(r, fwType, fwParams)
	if err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrService, "latest firmware loading"))
		return
	}
	if fw == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "latest firmware"))
		return
	}

	response.Json(w, fw.Version)
}

func (api *API) GetLatestFirmwareFile(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	APIKey, isAPIKey := ctx.Value(middleware.ContextAPIKey).(models.APIKey)
	if !isAPIKey {
		log.Errorf("AUTH: %s is empty. Probably middleware was not called or bad auth!", middleware.ContextAPIKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	if !APIKey.IsValid() {
		log.Errorf("Api Key %s is not valid", ctx.Value(middleware.ContextAPIKey).(models.APIKey).Key)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)
	fwType := models.FirmwareType(params["type"])

	tag := r.URL.Query().Get("tag")
	var fwParams models.FirmwareQueryParams
	if len(strings.TrimSpace(tag)) > 2 {
		fwParams.FirmwareTagTitle = tag
	}
	log.Debugf("tag = %+v", tag)
	fw, err := api.dao.GetLatestFirmware(r, fwType, fwParams)
	if err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrService, "latest firmware loading"))
		return
	}
	if fw == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "latest firmware"))
		return
	}

	response.Json(w, fw.URL)
}

func (api *API) FirmwaresList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	fwType := models.FirmwareType(params["type"])
	if !fwType.IsValid() && fwType != "" {
		log.Errorf("invalid FirmwareType provided %s", params["type"])
		response.JsonError(w, resperrors.New(resperrors.ErrInvalid, params["type"]))
		return
	}

	var firmwares []models.Firmware

	q := api.db.Order("fwr_id DESC")
	if fwType.IsValid() { //means we want special type
		q = q.Where("fwr_type = ?", string(fwType))
	}
	res := q.Find(&firmwares).Limit(50) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading firmwares from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "firmwares loading"))
		return
	}

	for i := range firmwares {
		if firmwares[i].FirmwareTagId.Valid {
			err := api.db.Model(&firmwares[i]).Association("FirmwareTag").Find(&firmwares[i].FirmwareTag)
			if err != nil {
				log.Errorf("loading member firmwares[%d] assoc from DB error: %s", i, err.Error())
				response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "firmware tag loading"))
				return
			}
		}
	}

	response.Json(w, api.MakeModelResponseList(firmwares, []string{}))
}

func (api *API) UploadNewFirmware(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	APIKey, isAPIKey := ctx.Value(middleware.ContextAPIKey).(models.APIKey)
	if !isUserEnv && !isAPIKey {
		log.Errorf("AUTH: %s and %s is empty. Probably middleware was not called or bad auth!", middleware.ContextUserGroupKey, middleware.ContextAPIKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	if !isUserEnv && !APIKey.IsValid() {
		log.Errorf("Api Key %s is not valid", ctx.Value(middleware.ContextAPIKey).(models.APIKey).Key)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	fwType := models.FirmwareType(params["type"])
	if !fwType.IsValid() {
		log.Error("invalid FirmwareType provided %s", params["type"])
		response.JsonError(w, resperrors.New(resperrors.ErrInvalid, params["type"]))
		return
	}

	var makeCurrent = true
	IsCurrent, err := strconv.ParseBool(r.FormValue("IsCurrent"))
	if err == nil { //if value exists
		makeCurrent = IsCurrent
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()
	var res *gorm.DB
	//processing tag if provided
	tag := models.FirmwareTag{
		Title:    r.FormValue("Tag"),
		IsActive: true,
	}
	log.Infof("provided tag is %s", tag.Title)
	if tag.Validate() == nil {
		res = DBTX.DB.Where("ftg_title = ?", tag.Title).First(&tag) //handle inactive tags?
		if res.Error != nil {
			log.Errorf("loading firmware tag from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "db tag loading"))
			return
		}
	}

	//load last known firmware
	var lastFWVersion uint = 0
	var oldFWList []models.Firmware
	if tag.Id > 0 {
		res = DBTX.DB.Raw(`SELECT * FROM firmwares WHERE fwr_type = ? AND ftg_id = ? AND (fwr_version = 
		(SELECT MAX(fwr_version) FROM firmwares WHERE fwr_type = ?  AND ftg_id = ?));`,
			string(fwType), tag.Id, string(fwType), tag.Id).Find(&oldFWList)
	} else {
		res = DBTX.DB.Raw(`SELECT * FROM firmwares WHERE fwr_type = ? AND (fwr_version = 
		(SELECT MAX(fwr_version) FROM firmwares WHERE fwr_type = ?));`,
			string(fwType), string(fwType)).Find(&oldFWList)
	}
	if !errors.Is(res.Error, gorm.ErrRecordNotFound) || len(oldFWList) < 1 { //TODO remove this
		for _, fw := range oldFWList {
			if fw.Version > lastFWVersion {
				lastFWVersion = fw.Version

				if makeCurrent && fw.IsCurrent {
					fw.IsCurrent = false
					res := DBTX.DB.Save(&fw)
					if res.Error != nil {
						log.Errorf("cannot update (remove is_current flag) firmware to DB: %s", res.Error.Error())
						response.JsonError(w, resperrors.New(resperrors.ErrService, "firmware updating"))
						return
					}
				}
			}
		}
		log.Debugf("lastFWVersion %d", lastFWVersion)
	}
	if res.Error != nil {
		log.Errorf("loading firmware from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "firmware loading"))
		return
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Errorf("ParseMultipartForm error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "ParseMultipartForm"))
		return
	}

	file, handler, err := r.FormFile("Bin")
	if err != nil {
		log.Errorf("Uploaded file loading error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "file opening"))
		return
	}
	defer file.Close()

	inputFileName := strings.Split(handler.Filename, ".")
	if len(inputFileName) == 0 {
		log.Error("Bad file")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "bin"))
		return
	}
	fileExt := inputFileName[len(inputFileName)-1] //last entry is file extension

	//TODO detect a type
	outputFileName := fmt.Sprintf("%s_%d.%s", string(fwType), uint64(time.Now().UTC().UnixNano()), fileExt)

	f, err := os.OpenFile("./"+models.FirmwareBasePath+"/"+string(fwType)+"/"+outputFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Errorf("File saving error occurred: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "file saving"))
		return
	}
	defer f.Close()
	_, err = io.Copy(f, file)
	if err != nil {
		log.Error("io.Copy error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "io.Copy"))
		return
	}

	fw := models.Firmware{
		Version:      lastFWVersion + 1, //increment version
		FirmwareType: fwType,
		Title:        helpers.ToNullString(r.FormValue("Title")),
		Desc:         helpers.ToNullString(r.FormValue("Desc")),
		Filename:     outputFileName,
		IsCurrent:    makeCurrent,
	}
	if tag.Id > 0 {
		fw.FirmwareTagId = helpers.ToNullInt64NonZero(int64(tag.Id))
		fw.FirmwareTag = &tag
	}

	res = DBTX.DB.Create(&fw)
	if res.Error != nil {
		log.Errorf("Saving firmware to DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "db saving"))
		return
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, fw)
	return
}
