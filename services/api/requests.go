package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/wedancedalot/decimal"
	"gorm.io/gorm"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var (
	hiddenForAdminRequestFields = []string{}

	hiddenRequestFields = []string{
		//"Comment",
	}
)

func (api *API) registerNewEventRequest(DBTX dao.Transaction, mc apimodels.EventRequest, member models.Member, eventId ...uint) (models.MembersEvent, error) {
	var request models.MembersEvent
	var event *models.Event
	var err error

	if len(eventId) < 1 || eventId[0] == 0 {
		event, err = api.dao.GetLastEvent()
		if err != nil {
			log.Errorf("Cannot GetLastEvent: %s", err.Error())
			return request, resperrors.New(resperrors.ErrService)
		}
		if event == nil || !event.IsRegistrationOpened {
			log.Errorf("Active registration event is not found")
			return request, resperrors.New(resperrors.ErrNotFound, "event")
		}
	} else {
		event = new(models.Event)
		res := api.db.Where(&models.Event{Id: eventId[0]}).First(event)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Errorf("Event %d is not found", eventId[0])
			return request, resperrors.New(resperrors.ErrNotFound, "event")
		}
		if res.Error != nil {
			log.Errorf("Cannot load event %d from DB: %s", eventId[0], res.Error.Error())
			return request, resperrors.New(resperrors.ErrService)
		}
	}

	//check member, event to be unique
	res := DBTX.DB.Where("mbr_id = ? AND evt_id = ?",
		member.Id, event.Id,
	).First(&request)
	if !errors.Is(res.Error, gorm.ErrRecordNotFound) && res == nil {
		log.Warnf("Duplicate request was found by member %d and event %d", member.Id, event.Id)
		return request, resperrors.NewWithDesc(resperrors.ErrAlreadyExists, "request")
	}
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Errorf("Cannot load MemberEvent for check from DB: %s", res.Error.Error())
		return request, resperrors.New(resperrors.ErrService)
	}

	//check fraction
	var fe models.FractionsEvent
	res = DBTX.DB.Where("evt_id = ? AND frc_id = ?", event.Id, member.FractionId).First(&fe)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("fraction event with provided event id %d was not found", event.Id)
		return request, resperrors.New(resperrors.ErrService)
	}
	if res.Error != nil {
		log.Errorf("loading fraction event from DB error: %s", res.Error.Error())
		return request, resperrors.New(resperrors.ErrService)
	}

	if fe.MembersTotal >= fe.MembersMax {
		log.Errorf("fraction id %d is full of members now (%d of %d max)", fe.FractionId, fe.MembersTotal, fe.MembersMax)
		return request, resperrors.New(resperrors.ErrFractionFull)
	}

	if fe.RequestsTotal >= fe.MembersMax {
		log.Errorf("fraction id %d is full of requests now (%d of %d max)", fe.FractionId, fe.RequestsTotal, fe.MembersMax)
		return request, resperrors.New(resperrors.ErrFractionFull)
	}

	if strings.TrimSpace(mc.Pda) == "" {
		mc.Pda = "none"
	}

	request = models.MembersEvent{
		MemberId:        member.Id,
		EventId:         event.Id,
		Pda:             models.MemberEventPda(mc.Pda),
		ShotLight:       models.MemberEventShotLight(mc.ShotLight),
		HasPassport:     mc.HasPassport,
		LeaderCandidate: helpers.ToNullString(mc.LeaderCandidate),
		Confirmation:    models.ConfirmationNone,
		PaymentType:     models.PaymentTypeNone,
	}

	res = DBTX.DB.Create(&request)
	if res.Error != nil {
		log.Errorf("Cannot create member to DB: %s", res.Error.Error())
		return request, resperrors.New(resperrors.ErrService)
	}
	log.Debugf("Register request with id=%d", request.Id)

	//book a request place
	if !api.checkIfAdminGroup(member.User.Group) { //increment fraction value only for non-"admin" users
		fe.RequestsTotal++
	}

	res = DBTX.DB.Save(&fe)
	if res.Error != nil {
		log.Errorf("saving FractionsEvent %d to DB error: %s", fe.Id, res.Error.Error())
		return request, resperrors.New(resperrors.ErrService)
	}

	logData, err := json.Marshal(map[string]interface{}{
		"requestId": request.Id,
	})
	res = DBTX.DB.Create(&models.MembersChangeLog{
		EditorUserId:      member.UserId,
		TargetMemberId:    member.Id,
		Type:              models.MCTNewRequest,
		ProcessedByUserId: sql.NullInt64{},
		PreviousVersion:   sql.NullString{},
		CurrentVersion:    helpers.ToNullString(string(logData)),
	})
	if res.Error != nil {
		log.Errorf("Cannot create member change log (new request) record to DB: %s", res.Error.Error())
		return request, resperrors.New(resperrors.ErrService)
	}

	return request, nil
}

func (api *API) NewRegistration(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		res *gorm.DB
	)

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var rd apimodels.RegistrationData
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rd)
	if err != nil {
		log.Errorf("can't decode RegistrationData: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := rd.Validate(); err != nil {
		log.Errorf("RegistrationData validation error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam))
		return
	}

	user, err := api.registerNewUser(DBTX, rd.UserCreate)
	if err != nil {
		log.Errorf("registerNewUser error: %s", err.Error())
		response.JsonError(w, err)
		return
	}

	member, err := api.registerNewMember(DBTX, rd.MemberCreate, user)
	if err != nil {
		log.Errorf("registerNewMember error: %s", err.Error())
		response.JsonError(w, err)
		return
	}

	_, err = api.registerNewEventRequest(DBTX, rd.EventRequest, member)
	if err != nil {
		log.Errorf("registerNewEventRequest error: %s", err.Error())
		response.JsonError(w, err)
		return
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"user_uuid:":   user.Uuid.String(),
		"member_code:": member.Code,
	})
	return
}

func (api *API) MembersEventRequestsList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		event *models.Event
		err   error
	)
	event, err = api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetActiveRegistrationEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil { //if no reg event found, just load the last one
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var requests []models.MembersEvent
	res := api.db.Where("evt_id = ? AND mev_confirmation NOT IN (?)", event.Id, []models.MemberEventConfirmation{
		models.ConfirmationPaid,
	}).Order("mev_id asc").Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	var resp []models.MembersEvent
	for i := range requests {
		err := api.db.Model(&requests[i]).Association("Member").Find(&requests[i].Member)
		if err != nil {
			log.Errorf("loading request Member Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Member loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("User").Find(&requests[i].Member.User)
		if err != nil {
			log.Errorf("loading request User Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request user loading"))
			return
		}
		if requests[i].Member.User.IsBanned {
			continue
		}

		err = api.db.Model(&requests[i]).Association("Event").Find(&requests[i].Event)
		if err != nil {
			log.Errorf("loading request Event Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Event loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Fraction").Find(&requests[i].Member.Fraction)
		if err != nil {
			log.Errorf("loading request Fraction Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Fraction loading"))
			return
		}

		err = api.db.Model(&requests[i].Member.User).Association("City").Find(&requests[i].Member.User.City)
		if err != nil {
			log.Errorf("loading request City Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request City loading"))
			return
		}

		resp = append(resp, requests[i])
	}

	response.Json(w, api.MakeModelResponseList(resp, hiddenForAdminUserFields))
	return
}

func (api *API) SetCheckForRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err error
		res *gorm.DB
	)

	curUserId, resperr := api.getCurrentUserId(r)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	params := mux.Vars(r)
	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var rsc apimodels.RequestSetCheck
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rsc)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var me models.MembersEvent
	res = DBTX.DB.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(requestId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading"))
		return
	}

	if me.Confirmation != models.ConfirmationNone && me.Confirmation != models.ConfirmationCommented {
		log.Errorf("request is already on a state of confirmation %s, cannot be checked", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrNotAllowed, "no way to add check on a state "+string(me.Confirmation)))
		return
	}

	typeExt := ApiActionRequestLegendApprove
	if rsc.IsLegendOk {
		me.IsLegendOK = true
	}
	if rsc.IsLookOk {
		typeExt = ApiActionRequestLookApprove
		me.IsLookOK = true
	}

	res = DBTX.DB.Save(&me)
	if res.Error != nil {
		log.Errorf("saving member event request %d to DB error: %s", me.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member request saving"))
		return
	}

	var member models.Member
	res = DBTX.DB.Where("mbr_id = ?", me.MemberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided id %d was not found", me.MemberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.MemberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	res = DBTX.DB.Save(&member)
	if res.Error != nil {
		log.Errorf("saving member %d to DB error: %s", member.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member saving"))
		return
	}

	//add log entry
	entryData, err := json.Marshal(map[string]interface{}{
		"mev_id": me.Id,
	})
	err = api.dao.PutActionsLogItem(&models.ActionLog{
		EntityName:   reflect.TypeOf(me).Name(),
		EntityId:     me.Id,
		FuncName:     helpers.GetCurrentFuncName(),
		Type:         models.ActionLogTypeUpdate,
		TypeExtended: typeExt,
		UserId:       curUserId,
		Data:         string(entryData),
	}, DBTX)

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("cannot commit tx: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"request_id:": me.Id,
		"status:":     me.Confirmation,
	})
	return
}

func (api *API) ApproveRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err error
		res *gorm.DB
	)

	curUserId, resperr := api.getCurrentUserId(r)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	params := mux.Vars(r)
	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var ra apimodels.RequestApprove
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&ra)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var me models.MembersEvent
	res = DBTX.DB.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(requestId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading"))
		return
	}

	me.DiscountAmountUAH = ra.DiscountPrice
	me.AfterRegAmountUAH = ra.AfterRegPrice
	me.AmountUAH = ra.Price
	me.Comment = helpers.ToNullString(ra.Comment)

	me.IsLegendOK = true
	me.IsLookOK = true

	me.Confirmation = models.ConfirmationApproved

	//detect if can set paid
	if me.DiscountAmountUAH.Equal(decimal.Zero) || me.AmountUAH.Equal(decimal.Zero) {
		me.Confirmation = models.ConfirmationPaid
	}

	res = DBTX.DB.Save(&me)
	if res.Error != nil {
		log.Errorf("saving member event request %d to DB error: %s", me.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member request saving"))
		return
	}

	var member models.Member
	res = DBTX.DB.Where("mbr_id = ?", me.MemberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided id %d was not found", me.MemberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.MemberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	/*member.IsActive = true

	res = DBTX.DB.Save(&member)
	if res.Error != nil {
		log.Errorf("saving member %d to DB error: %s", member.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member saving"))
		return
	}*/

	//load user for email record creation
	err = DBTX.DB.Model(&member).Association("User").Find(&member.User)
	if err != nil {
		log.Errorf("loading User from member from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	//load fraction for email record creation
	err = DBTX.DB.Model(&member).Association("Fraction").Find(&member.Fraction)
	if err != nil {
		log.Errorf("loading Fraction from member from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "fraction loading"))
		return
	}

	//load event for email record creation
	err = DBTX.DB.Model(&me).Association("Event").Find(&me.Event)
	if err != nil {
		log.Errorf("loading Event from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "event loading"))
		return
	}

	switch me.Confirmation {
	case models.ConfirmationApproved:
		err = api.mail.CreateApprovedEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.ApprovedEmailFields{
			EventName:         me.Event.Title,
			Callsign:          member.Callsign,
			Fraction:          member.Fraction.Title,
			Amount:            me.AmountUAH.String(),
			DiscountDate:      me.Event.DiscountUntil.Time.Format("02.01.2006"),
			DiscountAmountUAH: me.DiscountAmountUAH.String(),
			Comment:           me.Comment.String,
		})
		if err != nil {
			log.Errorf("cannot CreateApprovedEmail: %s", err.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreateApprovedEmail"))
			return
		}
	case models.ConfirmationPaid:
		err = api.mail.CreateApprovedAndPaidEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.ApprovedAndPaidEmailFields{
			EventName: me.Event.Title,
			Callsign:  member.Callsign,
			Fraction:  member.Fraction.Title,
			Comment:   me.Comment.String,
		})
		if err != nil {
			log.Errorf("cannot CreateApprovedAndPaidEmail: %s", err.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreateApprovedAndPaidEmail"))
			return
		}

		//load event
		var event models.Event
		res = DBTX.DB.Where("evt_id = ?", me.EventId).First(&event)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("event with provided id %d was not found", me.EventId)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.EventId)))
			return
		}
		if res.Error != nil {
			log.Errorf("loading event from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "event loading"))
			return
		}

		//load fraction event
		var fe models.FractionsEvent
		res = DBTX.DB.Where("frc_id = ? AND evt_id = ?", member.FractionId, event.Id).First(&fe)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("FractionsEvent with provided fraction id %d and event %s was not found", member.FractionId, event.Id)
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrNotFound, "FractionsEvent"))
			return
		}
		if res.Error != nil {
			log.Errorf("loading FractionsEvent from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "FractionsEvent loading"))
			return
		}

		var fraction models.Fraction
		res = DBTX.DB.Where("frc_id = ?", member.FractionId).First(&fraction)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("fraction with provided id %d was not found", me.MemberId)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatUint(uint64(me.MemberId), 10)))
			return
		}
		if res.Error != nil {
			log.Errorf("loading fraction from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "fraction loading"))
			return
		}

		//book a place
		if fe.MembersTotal >= fe.MembersMax {
			log.Errorf("fraction %s is full of members now (%d of %d max)", fraction.Title, fe.MembersTotal, fe.MembersMax)
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrFractionFull, fmt.Sprintf("%s %d/%d", fraction.Title, fe.MembersTotal, fe.MembersMax)))
			return
		}
		if !api.checkIfAdminGroup(member.User.Group) { //increment fraction value only for non-"admin" users
			fe.MembersTotal++
		}

		res = DBTX.DB.Save(&fe)
		if res.Error != nil {
			log.Errorf("saving FractionsEvent %d to DB error: %s", fe.Id, res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "FractionsEvent saving"))
			return
		}
	}

	//add log entry
	entryData, err := json.Marshal(map[string]interface{}{
		"mev_id": me.Id,
	})
	err = api.dao.PutActionsLogItem(&models.ActionLog{
		EntityName:   reflect.TypeOf(me).Name(),
		EntityId:     me.Id,
		FuncName:     helpers.GetCurrentFuncName(),
		Type:         models.ActionLogTypeUpdate,
		TypeExtended: ApiActionRequestApprove,
		UserId:       curUserId,
		Data:         string(entryData),
	}, DBTX)

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("cannot commit tx: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"request_id:": me.Id,
		"status:":     me.Confirmation,
	})
	return
}

func (api *API) simpleRequestAction(curUserId uint, apiAction string, requestId int64, confirmation models.MemberEventConfirmation, ad apimodels.RequestAction) *resperrors.Error {
	var (
		err error
		res *gorm.DB
	)

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var me models.MembersEvent
	res = DBTX.DB.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		return resperrors.New(resperrors.ErrNotFound, string(requestId))
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading")
	}

	me.Comment = helpers.ToNullString(ad.Comment)
	me.Confirmation = confirmation

	res = DBTX.DB.Save(&me)
	if res.Error != nil {
		log.Errorf("saving member event request %d to DB error: %s", me.Id, res.Error.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "member request saving")
	}

	var member models.Member
	res = DBTX.DB.Where("mbr_id = ?", me.MemberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided id %d was not found", me.MemberId)
		return resperrors.New(resperrors.ErrNotFound, string(me.MemberId))
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "member loading")
	}

	//load user for email record creation
	err = DBTX.DB.Model(&member).Association("User").Find(&member.User)
	if err != nil {
		log.Errorf("loading User from member from DB error: %s", err.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "user loading")
	}

	//load fraction for email record creation
	err = DBTX.DB.Model(&member).Association("Fraction").Find(&member.Fraction)
	if err != nil {
		log.Errorf("loading Fraction from member from DB error: %s", err.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "fraction loading")
	}

	//load event for email record creation
	err = DBTX.DB.Model(&me).Association("Event").Find(&me.Event)
	if err != nil {
		log.Errorf("loading Event from DB error: %s", err.Error())
		return resperrors.NewWithDesc(resperrors.ErrService, "event loading")
	}

	switch confirmation {
	case models.ConfirmationCommented:
		err = api.mail.CreateCommentedEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.CommentedEmailFields{
			EventName: me.Event.Title,
			Callsign:  member.Callsign,
			Fraction:  member.Fraction.Title,
			Comment:   me.Comment.String,
		})
		if err != nil {
			log.Errorf("cannot CreateCommentedEmail: %s", err.Error())
			return resperrors.NewWithDesc(resperrors.ErrService, "CreateCommentedEmail")
		}
	case models.ConfirmationDeclined:
		//load fraction event
		var fe models.FractionsEvent
		res = DBTX.DB.Where("frc_id = ? AND evt_id = ?", member.FractionId, me.EventId).First(&fe)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("FractionsEvent with provided fraction id %d and event %s was not found", member.FractionId, me.EventId)
			return resperrors.NewWithDesc(resperrors.ErrNotFound, "FractionsEvent")
		}
		if res.Error != nil {
			log.Errorf("loading FractionsEvent from DB error: %s", res.Error.Error())
			return resperrors.NewWithDesc(resperrors.ErrService, "FractionsEvent loading")
		}

		//unbook a place
		if !api.checkIfAdminGroup(member.User.Group) { //decrement fraction value only for non-"admin" users
			if fe.RequestsTotal > 0 {
				fe.RequestsTotal--
			}
		}

		res = DBTX.DB.Save(&fe)
		if res.Error != nil {
			log.Errorf("saving FractionsEvent %d to DB error: %s", fe.Id, res.Error.Error())
			return resperrors.New(resperrors.ErrService)
		}

		err = api.mail.CreateDeclinedEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.DeclinedEmailFields{
			EventName: me.Event.Title,
			Callsign:  member.Callsign,
			Fraction:  member.Fraction.Title,
			Comment:   me.Comment.String,
		})
		if err != nil {
			log.Errorf("cannot CreateDeclinedEmail: %s", err.Error())
			return resperrors.NewWithDesc(resperrors.ErrService, "CreateDeclinedEmail")
		}
	}

	//add log entry
	err = api.dao.PutActionsLogItem(&models.ActionLog{
		EntityName:   reflect.TypeOf(me).Name(),
		EntityId:     me.Id,
		Desc:         ad.Comment,
		FuncName:     helpers.GetCurrentFuncName(),
		Type:         models.ActionLogTypeUpdate,
		TypeExtended: apiAction,
		UserId:       curUserId,
		Data:         "", //TODO put json of item here, probably?
	}, DBTX)

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("cannot commit tx: %s", err.Error())
		return resperrors.New(resperrors.ErrService)
	}

	return nil
}

func (api *API) CommentRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	curUserId, resperr := api.getCurrentUserId(r)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	params := mux.Vars(r)
	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var rc apimodels.RequestComment
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rc)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	resperr = api.simpleRequestAction(curUserId, ApiActionRequestComment, requestId, models.ConfirmationCommented, rc.RequestAction)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	response.Json(w, map[string]interface{}{
		"request_id:": requestId,
		"status:":     models.ConfirmationCommented,
	})
	return
}

func (api *API) DeclineRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	curUserId, resperr := api.getCurrentUserId(r)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	params := mux.Vars(r)
	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var rc apimodels.RequestComment
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rc)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	resperr = api.simpleRequestAction(curUserId, ApiActionRequestReject, requestId, models.ConfirmationDeclined, rc.RequestAction)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	response.Json(w, map[string]interface{}{
		"request_id:": requestId,
		"status:":     models.ConfirmationDeclined,
	})
	return
}

func (api *API) PaidRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err error
		res *gorm.DB
	)

	curUserId, resperr := api.getCurrentUserId(r)
	if resperr != nil {
		response.JsonError(w, resperr)
		return
	}

	params := mux.Vars(r)
	requestId, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad request id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "rid"))
		return
	}

	var rp apimodels.RequestPaid
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rp)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var me models.MembersEvent
	res = DBTX.DB.Where("mev_id = ?", requestId).First(&me)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member event (request) with provided request id %d was not found", requestId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(requestId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member event (request) from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member event (request) loading"))
		return
	}

	//load event for email record creation
	var event models.Event
	res = DBTX.DB.Where("evt_id = ?", me.EventId).First(&event)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("event with provided id %d was not found", me.EventId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.EventId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading event from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "event loading"))
		return
	}

	me.PayedAmountUAH = rp.PaidAmount
	me.Comment = helpers.ToNullString(rp.Comment)
	me.PaymentType = models.MemberEventPaymentType(rp.PaymentType)

	//detect is fully payed
	if !rp.ForceFullPayment {
		rp.ForceFullPayment =
			(time.Now().Before(event.DiscountUntil.Time) && me.PayedAmountUAH.GreaterThanOrEqual(me.DiscountAmountUAH)) ||
				(me.PayedAmountUAH.GreaterThanOrEqual(me.AmountUAH))
	}

	me.Confirmation = models.ConfirmationPartiallyPaid
	if rp.ForceFullPayment {
		me.Confirmation = models.ConfirmationPaid
	}

	res = DBTX.DB.Save(&me)
	if res.Error != nil {
		log.Errorf("saving member event request %d to DB error: %s", me.Id, res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member request saving"))
		return
	}

	var member models.Member
	res = DBTX.DB.Where("mbr_id = ?", me.MemberId).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member with provided id %d was not found", me.MemberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.MemberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member loading"))
		return
	}

	//load user for email record creation
	err = DBTX.DB.Model(&member).Association("User").Find(&member.User)
	if err != nil {
		log.Errorf("loading User from member from DB error: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	var fraction models.Fraction
	res = DBTX.DB.Where("frc_id = ?", member.FractionId).First(&fraction)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("fraction with provided id %d was not found", me.MemberId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(me.MemberId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading fraction from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "fraction loading"))
		return
	}

	switch me.Confirmation {
	case models.ConfirmationPartiallyPaid:
		amountDiff := me.AmountUAH.Sub(me.PayedAmountUAH)
		if event.DiscountUntil.Time.Before(time.Now()) {
			amountDiff = me.DiscountAmountUAH.Sub(me.PayedAmountUAH)
		}

		err = api.mail.CreatePaidPartiallyEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.PaidPartiallyEmailFields{
			EventName:         event.Title,
			Callsign:          member.Callsign,
			Fraction:          fraction.Title,
			Amount:            me.AmountUAH.String(),
			DiscountDate:      event.DiscountUntil.Time.Format("02.01.2006"),
			DiscountAmountUAH: me.DiscountAmountUAH.String(),
			Comment:           me.Comment.String,
			PaidAmount:        me.PayedAmountUAH.String(),
			AmountDiff:        amountDiff.String(),
		})
		if err != nil {
			log.Errorf("cannot CreatePaidPartiallyEmail: %s", err.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreatePaidPartiallyEmail"))
			return
		}
	case models.ConfirmationPaid:
		err = api.mail.CreatePaidEmail(DBTX.DB, member.User, api.config.SmtpSystem.FromEmail, mailer.PaidEmailFields{
			EventName:  event.Title,
			Callsign:   member.Callsign,
			Fraction:   fraction.Title,
			Comment:    me.Comment.String,
			PaidAmount: me.PayedAmountUAH.String(),
		})
		if err != nil {
			log.Errorf("cannot CreatePaidEmail: %s", err.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreatePaidEmail"))
			return
		}

		//load fraction event
		var fe models.FractionsEvent
		res = DBTX.DB.Where("frc_id = ? AND evt_id = ?", member.FractionId, event.Id).First(&fe)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("FractionsEvent with provided fraction id %d and event %s was not found", member.FractionId, event.Id)
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrNotFound, "FractionsEvent"))
			return
		}
		if res.Error != nil {
			log.Errorf("loading FractionsEvent from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "FractionsEvent loading"))
			return
		}

		//book a place
		if fe.MembersTotal >= fe.MembersMax {
			log.Errorf("fraction %s is full of members now (%d of %d max)", fraction.Title, fe.MembersTotal, fe.MembersMax)
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrFractionFull, fmt.Sprintf("%s %d/%d", fraction.Title, fe.MembersTotal, fe.MembersMax)))
			return
		}
		if !api.checkIfAdminGroup(member.User.Group) { //increment fraction value only for non-"admin" users
			fe.MembersTotal++
		}

		res = DBTX.DB.Save(&fe)
		if res.Error != nil {
			log.Errorf("saving FractionsEvent %d to DB error: %s", fe.Id, res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "FractionsEvent saving"))
			return
		}
	}

	//add log entry
	err = api.dao.PutActionsLogItem(&models.ActionLog{
		EntityName:   reflect.TypeOf(me).Name(),
		EntityId:     me.Id,
		Desc:         me.PayedAmountUAH.String(),
		FuncName:     helpers.GetCurrentFuncName(),
		Type:         models.ActionLogTypeUpdate,
		TypeExtended: ApiActionRequestPayed,
		UserId:       curUserId,
		Data:         "", //TODO put json of item here, probably?
	}, DBTX)

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("cannot commit tx: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"request_id:": me.Id,
		"status:":     me.Confirmation,
	})
	return
}

func (api *API) MemberIdEventRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	evt, err := api.dao.GetLastEvent()
	if err != nil {
		log.Error("GetLastEvent error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "GetLastEvent loading"))
		return
	}
	if evt == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	params := mux.Vars(r)

	eventId, err := strconv.ParseInt(params["eventId"], 10, 64)
	if err != nil {
		log.Errorf("bad eventId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "eventId"))
		return
	}

	memberId, err := strconv.ParseInt(params["memberId"], 10, 64)
	if err != nil {
		log.Errorf("bad memberId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "memberId"))
		return
	}

	var request models.MembersEvent
	res := api.db.Where("evt_id = ? AND mbr_id = ?", eventId, memberId).Order("mev_id asc").First(&request)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(request, hiddenForAdminRequestFields))
		return
	}

	response.Json(w, api.MakeModelResponse(request, hiddenRequestFields))
	return
}

func (api *API) GetEventRequestById(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	rid, err := strconv.ParseInt(params["rid"], 10, 64)
	if err != nil {
		log.Errorf("bad eventId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "eventId"))
		return
	}

	var request models.MembersEvent
	res := api.db.Where("mev_id = ?", rid).Order("mev_id asc").First(&request)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(request, hiddenForAdminRequestFields))
		return
	}

	response.Json(w, api.MakeModelResponse(request, hiddenRequestFields))
	return
}

func (api *API) SendMembersEventRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	eventId, err := strconv.ParseUint(params["eventId"], 10, 64)
	if err != nil {
		log.Errorf("bad eventId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "eventId"))
		return
	}

	memberId, err := strconv.ParseUint(params["memberId"], 10, 64)
	if err != nil {
		log.Errorf("bad memberId param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "memberId"))
		return
	}

	var er apimodels.EventRequest
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&er)
	if err != nil {
		log.Errorf("can't decode EventRequest data", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var member models.Member
	res := DBTX.DB.Where(models.Member{Id: uint(memberId), IsActive: true}).First(&member)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrNotFound, "member", strconv.FormatUint(uint64(memberId), 10)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	newME, err := api.registerNewEventRequest(DBTX, er, member, uint(eventId))
	if err != nil {
		response.JsonError(w, err)
		return
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit tx to DB: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"id:": newME.Id,
	})
}

func (api *API) MembersActiveEventRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userGroup, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := uint(uid)

	event, err := api.dao.GetActiveRegistrationEvent()
	if err != nil {
		logrus.Errorf("Cannot GetActiveRegistrationEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	member, err := api.dao.GetActiveUserMember(userId)
	if err != nil {
		logrus.Errorf("Cannot GetActiveUserMember: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if member == nil {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var request models.MembersEvent
	res := api.db.Where("evt_id = ? AND mbr_id = ?", event.Id, member.Id).First(&request)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}
	if res.Error != nil {
		log.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	if api.checkIfAdminGroup(userGroup) {
		response.Json(w, api.MakeModelResponse(request, hiddenForAdminRequestFields))
		return
	}

	response.Json(w, api.MakeModelResponse(request, hiddenRequestFields))
	return
}
