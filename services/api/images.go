package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"encoding/base64"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	MaxFilesLimit = 4
)

func (api *API) UploadAvatarImage(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Errorf("ParseMultipartForm error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "ParseMultipartForm"))
		return
	}

	file, handler, err := r.FormFile("img")
	if err != nil {
		log.Errorf("Uploaded file loading error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "file opening"))
		return
	}
	defer file.Close()

	inputFileName := strings.Split(handler.Filename, ".")
	if len(inputFileName) == 0 {
		log.Error("Bad file")
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "img"))
		return
	}
	fileExt := inputFileName[len(inputFileName)-1] //last entry is file extension

	outputFileName := fmt.Sprintf("%s_%d.%s", models.ImageAvatar, uint64(time.Now().UTC().UnixNano()), fileExt)

	f, err := os.OpenFile(models.ImageBasePath+"/"+outputFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Errorf("File saving error occurred: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "file saving"))
		return
	}
	defer f.Close()
	_, err = io.Copy(f, file)
	if err != nil {
		log.Error("io.Copy error: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "io.Copy"))
		return
	}

	image := models.Image{
		ImageType: models.ImageAvatar,
		Name:      outputFileName,
		IsTemp:    true,
	}

	res := api.db.Create(&image)
	if res.Error != nil {
		log.Errorf("Saving image to DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "db saving"))
		return
	}

	response.Json(w, map[string]interface{}{
		"img_id": image.Id,
	})
}

func (api *API) UploadPhotos(w http.ResponseWriter, r *http.Request) {
	log.Debug("UploadPhotos called!")
	err := r.ParseMultipartForm(256 << 20)
	if err != nil {
		log.Error("ParseMultipartForm error: %s", err)
		response.JsonError(w, resperrors.New(resperrors.ErrService, "ParseMultipartForm"))
		return
	}

	fhs := r.MultipartForm.File["file"]

	if len(fhs) == 0 {
		log.Error("No files uploaded by user")
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest, "no files"))
		return
	}
	if len(fhs) > MaxFilesLimit {
		log.Error("Too many (%d) files uploaded by user", len(fhs))
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest, "too many files"))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	var images []*models.Image

	for _, handler := range fhs {
		file, err := handler.Open()
		if err != nil {
			log.Error("Uploaded file loading error: %s", err)
			response.JsonError(w, resperrors.New(resperrors.ErrService, "file opening"))
			return
		}
		defer file.Close()

		inputFileName := strings.Split(handler.Filename, ".")
		if len(inputFileName) == 0 {
			log.Error("Bad file")
			response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "img"))
			return
		}
		fileExt := inputFileName[len(inputFileName)-1] //last entry is file extension

		outputFileName := fmt.Sprintf("%s_%d.%s", models.ImagePhoto, uint64(time.Now().UTC().UnixNano()), fileExt)

		f, err := os.OpenFile(models.ImageBasePath+"/"+outputFileName, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			log.Errorf("File saving error occurred: %s", err)
			response.JsonError(w, resperrors.New(resperrors.ErrService, "file saving"))
			return
		}
		defer f.Close()
		_, err = io.Copy(f, file)
		if err != nil {
			log.Errorf("File copying error occurred: %s", err)
			response.JsonError(w, resperrors.New(resperrors.ErrService, "file copying"))
			return
		}

		images = append(images, &models.Image{
			ImageType: models.ImagePhoto,
			Name:      outputFileName,
			IsTemp:    true,
		})

		res := DBTX.DB.Create(images[len(images)-1])
		if res.Error != nil {
			log.Error("Saving image to DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "db saving"))
			DBTX.Rollback()
			return
		}
	}

	res := DBTX.Commit()
	if res != nil {
		log.Error("Saving images (TX commit) to DB error: %s", res.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "db tx commit"))
		DBTX.Rollback()
		return
	}

	var resp []map[string]interface{}
	for _, img := range images {
		resp = append(resp, map[string]interface{}{
			"img_id":  img.Id,
			"img_src": helpers.GetImagePath(r, img.Name),
		})
	}

	response.Json(w, resp)
}

func (api *API) makeImageStatic(DBTX dao.Transaction, imgId uint) error {
	var img models.Image
	res := DBTX.DB.Model(&img).Where("img_id = ?", imgId).Update("img_is_tmp", false)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Errorf("Cannot find such image id %d in DB (may be cleaner bot removed it?)", imgId)
		return resperrors.New(resperrors.ErrNotFound, "img_id")
	}
	if res.Error != nil {
		log.Errorf("Cannot find such image id %d in DB: %s", imgId, res.Error.Error())
		return resperrors.New(resperrors.ErrService)
	}

	return nil
}

func (api *API) makeImageRemoved(DBTX dao.Transaction, imgId uint) error {
	var img models.Image
	res := DBTX.DB.Model(&img).Where("img_id = ?", imgId).Update("img_is_tmp", true)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Errorf("Cannot find such image id %d in DB (may be cleaner bot removed it?)", imgId)
		return resperrors.New(resperrors.ErrNotFound, "img_id")
	}
	if res.Error != nil {
		log.Errorf("Cannot find such image id %d in DB: %s", imgId, res.Error.Error())
		return resperrors.New(resperrors.ErrService)
	}

	return nil
}

func (api *API) createNewImageFromBase64(DBTX dao.Transaction, imgData string, imgType models.ImageType) (image models.Image, err error) {
	const (
		base64Part    = ";base64,"
		dataImagePart = "data:image/"
	)

	index := strings.Index(imgData, base64Part)
	if index < 0 {
		return image, fmt.Errorf("bad image data was provided (no base64 proper format?)")
	}
	fileExt := imgData[len(dataImagePart):index]

	outputFileName := fmt.Sprintf("%s_%d.%s", imgType, uint64(time.Now().UTC().UnixNano()), fileExt)

	f, err := os.OpenFile(models.ImageBasePath+"/"+outputFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return image, fmt.Errorf("file creating error was occurred: %s", err.Error())
	}
	defer f.Close()

	dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(imgData[index+len(base64Part):])) // pass reader to NewDecoder

	_, err = io.Copy(f, dec)
	if err != nil {
		return image, fmt.Errorf("io.Copy error: %s", err.Error())
	}

	image = models.Image{
		ImageType: models.ImageItem,
		Name:      outputFileName,
		IsTemp:    false,
	}

	res := DBTX.DB.Create(&image)
	if res.Error != nil {
		return image, fmt.Errorf("saving image to DB error: %s", res.Error.Error())
	}

	return image, nil
}
