package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"bitbucket.org/eternalzone/stalk.net/services/mailer"
	"errors"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"net/http"
)

func (api *API) NotifyUnpaidRequests(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		event *models.Event
		err   error
	)

	event, err = api.dao.GetLastEvent()
	if err != nil {
		logrus.Errorf("Cannot GetLastEvent: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}
	if event == nil { //if no reg event found, just load the last one
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound))
		return
	}

	var requests []models.MembersEvent
	res := api.db.Where("evt_id = ? AND mev_confirmation IN (?)", event.Id, []models.MemberEventConfirmation{
		models.ConfirmationApproved,
	}).Order("mev_id asc").Find(&requests)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(requests) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		logrus.Errorf("loading request from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
		return
	}

	list := make([]mailer.NotifyUnpaidEmailMemberFields, 0)
	for i := range requests {
		err := api.db.Model(&requests[i]).Association("Member").Find(&requests[i].Member)
		if err != nil {
			logrus.Errorf("loading request association Member from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("Fraction").Find(&requests[i].Member.Fraction)
		if err != nil {
			logrus.Errorf("loading request association Fraction from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
			return
		}

		err = api.db.Model(&requests[i].Member).Association("User").Find(&requests[i].Member.User)
		if err != nil {
			logrus.Errorf("loading request association User from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request loading"))
			return
		}

		list = append(list, mailer.NotifyUnpaidEmailMemberFields{
			Callsign: requests[i].Member.Callsign,
			Fraction: requests[i].Member.Fraction.Title,
			Amount:   requests[i].AmountUAH.String(),
			User:     requests[i].Member.User,
		})
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()
	err = api.mail.CreateNotifyUnpaidEmails(DBTX.DB, api.config.SmtpSystem.FromEmail, mailer.NotifyUnpaidEmailFieldsArg{
		EventName:   event.Title,
		MembersList: list,
	})
	if err != nil {
		logrus.Errorf("cannot CreateApprovedEmail: %s", err.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "CreateApprovedEmail"))
		return
	}

	err = DBTX.Commit()
	if err != nil {
		logrus.Errorf("cannot commit tx: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, map[string]interface{}{
		"event_id:": event.Id,
	})
	return
}
