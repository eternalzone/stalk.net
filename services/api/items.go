package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"strings"
)

var (
	hiddenForAdminItemsFields = make([]string, 0)

	hiddenItemsFields = []string{
		"Id",
	}
)

func (api *API) LoadSuits(w http.ResponseWriter, r *http.Request) {
	var items []*models.Item

	res := api.db.Where("itm_type = ?", models.ItemSuit).Find(&items)
	if res.Error != nil {
		log.Error("loading items from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "items loading"))
		return
	}

	var resp []map[string]interface{}

	for _, it := range items {
		resp = append(resp, map[string]interface{}{
			"id":    it.Id,
			"title": it.Title,
			"desc":  it.Desc,
		})
	}

	response.Json(w, resp)
}

func (api *API) ItemsList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var items []models.Item

	//TODO apply some filters
	res := api.db.Where("itm_is_active = ?", true).Order("itm_id DESC").Find(&items).Limit(500) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading items from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "items loading"))
		return
	}

	for i := range items {
		api.db.Model(&items[i]).Association("Image").Find(&items[i].Image)
		api.db.Model(&items[i]).Association("Location").Find(&items[i].Location)
		api.db.Model(&items[i]).Association("GPSList").Find(&items[i].GPSList)
	}

	//fill up the active positions
	for i := range items {
		for j := range items[i].GPSList {
			if items[i].GPSList[j].IsActive {
				items[i].GPSId = items[i].GPSList[j].Id
				items[i].GPS = items[i].GPSList[j]
			}
		}
	}

	response.Json(w, api.MakeModelResponseList(items, hiddenForAdminItemsFields))
}

func (api *API) ItemCreateOrUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var (
		err   error
		res   *gorm.DB
		image models.Image
		item  models.Item
	)

	var icu apimodels.ItemCreateOrUpdate
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&icu)
	if err != nil {
		log.Error(err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadRequest))
		return
	}

	if err := icu.Validate(); err != nil {
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, err.Error()))
		return
	}

	DBTX := dao.NewTransaction(api.db)
	defer DBTX.Rollback()

	if icu.Image != "" { //replace image only if needs
		image, err = api.createNewImageFromBase64(DBTX, icu.Image, models.ImageItem)
		if err != nil {
			log.Errorf("Cannot createNewImageFromBase64: %s", err.Error())
			response.JsonError(w, err)
			return
		}
	}

	if icu.Id == 0 { //create the new one
		log.Debugf("Received item Create request with Id %d, Title %s", icu.Id, icu.Title)

		item.Desc = icu.Desc
		item.Title = icu.Title
		item.ItemType = models.ItemType(icu.ItemType)
		item.Owner = models.ItemOwner(icu.Owner)
		item.Place = models.ItemPlace(icu.Place)
		item.IsVirtual = icu.IsVirtual
		item.IsActive = icu.IsActive
		item.Comment = icu.Comment

		if strings.TrimSpace(icu.Data) != "" {
			item.Data = icu.Data
		}

		ctx := r.Context()
		userEnv, isUserEnv := ctx.Value("userEnv").(apimodels.UserEnv)
		if !isUserEnv {
			log.Error("Somehow userEnv is empty. Probably middleware was not called or bad auth!!")
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		if userEnv.UserUuid == nil {
			log.Error("Somehow userEnv.UserUuid is empty")
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		var user models.User
		res := api.db.Where(models.User{Uuid: *userEnv.UserUuid}).First(&user)
		if res.Error != nil {
			if errors.Is(res.Error, gorm.ErrRecordNotFound) {
				response.JsonError(w, resperrors.New(resperrors.ErrNotFound, "user"))
				return
			}
			log.Errorf("cannot load user by uuid %s: %s", userEnv.UserUuid.String(), res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
			return
		}

		item.CreatedByUserId = user.Id

		if icu.LocationId > 0 {
			item.LocationId = icu.LocationId
		}

		//create code if type is must to have a code
		if (item.ItemType == models.ItemQuest || item.ItemType == models.ItemOther) && !item.IsVirtual {
			item.Code = helpers.ToNullString(helpers.RandHexString(models.ItemCodeLength))
			//TODO check the code to be unique
		}

		if image.Id > 0 {
			item.ImageId = image.Id
		}

		res = DBTX.DB.Create(&item)
		if res.Error != nil {
			log.Errorf("Cannot create item to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "item creating"))
			return
		}

		if icu.GPSId > 0 {
			igps := models.ItemGPS{
				ItemId:   item.Id,
				GPSId:    icu.GPSId,
				IsActive: true,
			}

			res = DBTX.DB.Create(&igps)
			if res.Error != nil {
				log.Errorf("Cannot create items_gps to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "items_gps connection creating"))
				return
			}
		}
	} else { //or update
		log.Debugf("Received item Update request with Id %d, Title %s", icu.Id, icu.Title)

		res = DBTX.DB.Where("itm_id = ?", icu.Id).First(&item)
		if res.Error != nil {
			log.Error("loading item %d from DB error: %s", icu.Id, res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "item loading"))
			return
		}

		item.Desc = icu.Desc
		item.Title = icu.Title
		item.ItemType = models.ItemType(icu.ItemType)
		item.Owner = models.ItemOwner(icu.Owner)
		item.Place = models.ItemPlace(icu.Place)
		item.IsVirtual = icu.IsVirtual
		item.IsActive = icu.IsActive
		item.Comment = icu.Comment
		item.Data = icu.Data //TODO check if this user can edit data may be?

		if icu.LocationId > 0 && icu.LocationId != item.LocationId {
			item.LocationId = icu.LocationId
		}

		//create new code if no previous and new type is must to have a code
		if !item.Code.Valid && (icu.ItemType == string(models.ItemQuest) || icu.ItemType == string(models.ItemOther)) && !icu.IsVirtual {
			item.Code = helpers.ToNullString(helpers.RandHexString(models.ItemCodeLength))
		}

		if image.Id > 0 {
			item.ImageId = image.Id
		}

		res = DBTX.DB.Save(&item)
		if res.Error != nil {
			log.Errorf("Cannot update item to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "item updating"))
			return
		}

		//check and update GPS List
		var itemGPS models.ItemGPS
		res := DBTX.DB.Where("itm_id = ? AND itg_is_active = ?", icu.Id, true).First(&itemGPS)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("items_gps with provided itm_id %d was not found", icu.Id)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(icu.Id)))
			return
		}
		if res.Error != nil {
			log.Errorf("items_gps item from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "items_gps loading"))
			return
		}

		if icu.GPSId > 0 && icu.GPSId != itemGPS.Id {
			//disable previous record
			itemGPS.IsActive = false
			res = DBTX.DB.Save(&itemGPS)
			if res.Error != nil {
				log.Errorf("cannot update (disable) items_gps id %d to DB: %s", itemGPS.Id, res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "old items_gps disabling"))
				return
			}

			//create the new one
			igps := models.ItemGPS{
				ItemId:   item.Id,
				GPSId:    icu.GPSId,
				IsActive: true,
			}

			res = DBTX.DB.Create(&igps)
			if res.Error != nil {
				log.Errorf("cannot create items_gps to DB: %s", res.Error.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "items_gps assoc creating"))
				return
			}
		}
	}

	err = DBTX.Commit()
	if err != nil {
		log.Errorf("Cannot commit DB transaction: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
		return
	}

	response.Json(w, api.MakeModelResponse(item, hiddenForAdminItemsFields))
	return
}

func (api *API) ItemsLoad(w http.ResponseWriter, r *http.Request) {
	var items []models.Item

	//TODO apply some filters
	res := api.db.Find(&items).Limit(50) //TODO temporary limit
	if res.Error != nil {
		log.Error("loading items from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "items loading"))
		return
	}

	for i := range items {
		api.db.Model(&items[i]).Association("Image").Find(&items[i].Image)
		api.db.Model(&items[i]).Association("Location").Find(&items[i].Location)
		api.db.Model(&items[i]).Association("GPSList").Find(&items[i].GPSList)
	}

	//fill up the active positions
	for i := range items {
		for j := range items[i].GPSList {
			if items[i].GPSList[j].IsActive {
				items[i].GPSId = items[i].GPSList[j].Id
				items[i].GPS = items[i].GPSList[j]
			}
		}
	}

	response.Json(w, api.MakeModelResponseList(items, hiddenForAdminItemsFields))
	return
}

func (api *API) GetItemByIdAdmin(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	params := mux.Vars(r)

	itemId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad item id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var item models.Item
	res := api.db.Where("itm_id = ?", itemId).First(&item)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("item with provided id %d was not found", itemId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, string(itemId)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading item from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "item loading"))
		return
	}

	api.db.Model(&item).Association("Image").Find(&item.Image)
	api.db.Model(&item).Association("Location").Find(&item.Location)
	api.db.Model(&item).Association("GPSList").Find(&item.GPSList)

	//fill up the active position
	for j := range item.GPSList {
		if item.GPSList[j].IsActive {
			item.GPSId = item.GPSList[j].Id
			item.GPS = item.GPSList[j]
		}
	}

	response.Json(w, api.MakeModelResponse(item, hiddenForAdminItemsFields))
	return
}
