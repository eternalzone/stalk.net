package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/helpers"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"errors"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

var hiddenForAdminMemberChangeLogFields = []string{}

func (api *API) MembersChangeLogList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var recs []models.MembersChangeLog

	res := api.db.Limit(100).Order("mcl_id DESC").Find(&recs)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) || len(recs) < 1 {
		response.Json(w, []string{})
		return
	}
	if res.Error != nil {
		log.Errorf("loading member change log list from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member change log loading"))
		return
	}

	for i := range recs {
		err := api.db.Model(&recs[i]).Association("Editor").Find(&recs[i].Editor)
		if err != nil {
			log.Errorf("loading change record Editor Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Editor loading"))
			return
		}
		recs[i].Editor.PassHash = "" //hide passhash

		err = api.db.Model(&recs[i]).Association("Target").Find(&recs[i].Target)
		if err != nil {
			log.Errorf("loading change record Target Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target loading"))
			return
		}

		err = api.db.Model(&recs[i].Target).Association("User").Find(&recs[i].Target.User)
		if err != nil {
			log.Errorf("loading change record Target.User Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target.User loading"))
			return
		}
		recs[i].Target.User.PassHash = "" //hide passhash

		err = api.db.Model(&recs[i].Target).Association("Fraction").Find(&recs[i].Target.Fraction)
		if err != nil {
			log.Errorf("loading change record Target.Fraction Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target.Fraction loading"))
			return
		}

		err = api.db.Model(&recs[i].Target).Association("Suit").Find(&recs[i].Target.Suit)
		if err != nil {
			log.Errorf("loading change record Target.Suit Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target.Suit loading"))
			return
		}

		err = api.db.Model(&recs[i].Target).Association("PassportPhoto").Find(&recs[i].Target.PassportPhoto)
		if err != nil {
			log.Errorf("loading change record Target.PassportPhoto Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target.PassportPhoto loading"))
			return
		}

		err = api.db.Model(&recs[i].Target).Association("Photos").Find(&recs[i].Target.Photos)
		if err != nil {
			log.Errorf("loading change record Target.Photos Association from DB error: %s", err.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService, "change log Target.Photos loading"))
			return
		}

		if recs[i].ProcessedByUserId.Valid && recs[i].ProcessedByUserId.Int64 > 0 {
			recs[i].ProcessedBy = new(models.User)
			err = api.db.Model(&recs[i]).Association("ProcessedBy").Find(recs[i].ProcessedBy)
			if err != nil {
				log.Errorf("loading change record Target Association from DB error: %s", err.Error())
				response.JsonError(w, resperrors.New(resperrors.ErrService, "request Editor loading"))
				return
			}
		}
	}

	response.Json(w, api.MakeModelResponseList(recs, hiddenForAdminMemberChangeLogFields))
	return
}

func (api *API) GetMembersChangeLogById(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := uint(uid)

	params := mux.Vars(r)

	mclId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad member change log id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var mcl models.MembersChangeLog
	res := api.db.Where("mcl_id = ?", mclId).First(&mcl)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member change log record with provided id %d was not found", mclId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatInt(mclId, 10)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member change log record from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member change log record loading"))
		return
	}

	err = api.db.Model(&mcl).Association("Editor").Find(&mcl.Editor)
	if err != nil {
		log.Errorf("loading change record Editor Association from DB error: %s", err)
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request Editor loading"))
		return
	}

	err = api.db.Model(&mcl).Association("Target").Find(&mcl.Target)
	if err != nil {
		log.Errorf("loading change record Target Association from DB error: %s", err)
		response.JsonError(w, resperrors.New(resperrors.ErrService, "request Editor loading"))
		return
	}

	if mcl.ProcessedByUserId.Valid && mcl.ProcessedByUserId.Int64 > 0 {
		mcl.ProcessedBy = new(models.User)
		err = api.db.Model(&mcl).Association("ProcessedBy").Find(mcl.ProcessedBy)
		if err != nil {
			log.Errorf("loading change record Target Association from DB error: %s", err)
			response.JsonError(w, resperrors.New(resperrors.ErrService, "request Editor loading"))
			return
		}
	}

	if !mcl.ProcessedByUserId.Valid || mcl.IsNew {
		mcl.ProcessedByUserId = helpers.ToNullInt64(int64(userId))
		mcl.IsNew = false

		res = api.db.Save(&mcl)
		if res.Error != nil {
			log.Errorf("Cannot update mcl record to DB: %s", res.Error.Error())
			response.JsonError(w, resperrors.New(resperrors.ErrService))
		}

		var ProcessedBy models.User
		res = api.db.Where("usr_id = ?", mcl.ProcessedByUserId.Int64).First(&ProcessedBy)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			log.Error("user (change log editor) record with provided id %d was not found", mcl.ProcessedByUserId.Int64)
			response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatInt(mclId, 10)))
			return
		}
		if res.Error != nil {
			log.Errorf("loading user (change log editor) record from DB error: %s", res.Error.Error())
			response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user change log editor record loading"))
			return
		}
		mcl.ProcessedBy = &ProcessedBy
	}

	response.Json(w, api.MakeModelResponse(mcl, hiddenForAdminMemberChangeLogFields))
	return
}

func (api *API) CountNewMembersChangeLogRecords(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	var count int64

	res := api.db.Model(&models.MembersChangeLog{}).Where("usr_id_processed_by IS NULL").Count(&count)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		response.Json(w, 0)
		return
	}
	if res.Error != nil {
		log.Errorf("loading count of new member change log records from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService, "member change log records count"))
		return
	}

	response.Json(w, count)
	return
}

// SetMembersChangeLogByIdAsProcessed this is not used atm (processed sets up by first get by id)
func (api *API) SetMembersChangeLogByIdAsProcessed(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	_, isUserEnv := ctx.Value(middleware.ContextUserGroupKey).(models.UserGroup)
	if !isUserEnv {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}

	if ctx.Value(middleware.ContextUserIdKey) == nil {
		log.Errorf("Somehow %s is empty. Probably middleware was not called or bad auth!!", middleware.ContextUserGroupKey)
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	uid := ctx.Value(middleware.ContextUserIdKey).(float64)
	if uid == 0 {
		log.Error("Somehow user_id is 0. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrBadAuth))
		return
	}
	userId := int64(uid)

	params := mux.Vars(r)

	mclId, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		log.Errorf("bad member change log id param provided: %s", err.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrBadParam, "id"))
		return
	}

	var mcl models.MembersChangeLog
	res := api.db.Where("mcl_id = ?", mclId).First(&mcl)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("member change log record with provided id %d was not found", mclId)
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, strconv.FormatInt(mclId, 10)))
		return
	}
	if res.Error != nil {
		log.Errorf("loading member change log record from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "member change log record loading"))
		return
	}

	api.db.Model(&mcl).Association("Editor").Find(&mcl.Editor)
	api.db.Model(&mcl).Association("Target").Find(&mcl.Target)
	//api.db.Model(&mcl).Association("ProcessedBy").Find(mcl.ProcessedBy)

	mcl.ProcessedByUserId = helpers.ToNullInt64(userId)

	res = api.db.Save(&mcl)
	if res.Error != nil {
		log.Errorf("Cannot update (set as processed) member change log %d to DB: %s", mcl.Id, res.Error.Error())
		response.JsonError(w, resperrors.New(resperrors.ErrService))
	}

	response.Json(w, api.MakeModelResponse(mcl, hiddenForAdminMemberChangeLogFields))
	return
}
