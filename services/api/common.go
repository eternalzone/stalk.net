package api

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"bitbucket.org/eternalzone/stalk.net/services/api/response"
	"bitbucket.org/eternalzone/stalk.net/services/api/response/resperrors"
	"errors"
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
	"net/http"
)

func (api *API) Index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte("<h1>stalk.net API</h1>"))
	return
}

//func (api *API) Test(w http.ResponseWriter, r *http.Request) {
//
//	response.Json(w, city)
//	return
//}

func (api *API) GetAllowedActionsByGroup(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if ctx.Value(middleware.ContextUserUuidKey) == nil {
		log.Error("Somehow user_uuid is nil. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed))
		return
	}

	userUuid := uuid.FromStringOrNil(ctx.Value(middleware.ContextUserUuidKey).(string))
	if userUuid == uuid.Nil {
		log.Error("Somehow userEnv.UserUuid is nil. Probably bad middleware was not called!")
		response.JsonError(w, resperrors.New(resperrors.ErrNotAllowed))
		return
	}

	var user models.User
	res := api.db.Where("usr_uuid = ?", userUuid.String()).First(&user)
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		log.Error("User with provided uuid %s was not found", userUuid.String())
		response.JsonError(w, resperrors.New(resperrors.ErrNotFound, userUuid.String()))
		return
	}
	if res.Error != nil {
		log.Errorf("loading user from DB error: %s", res.Error.Error())
		response.JsonError(w, resperrors.NewWithDesc(resperrors.ErrService, "user loading"))
		return
	}

	var resp = make([]string, 0)
	for _, actions := range api.actionRoutesMap[user.Group] {
		resp = append(resp, actions.Name)
	}

	response.Json(w, resp)
	return
}
