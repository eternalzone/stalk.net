package api

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bitbucket.org/eternalzone/stalk.net/services/api/apimodels"
	"bitbucket.org/eternalzone/stalk.net/services/api/middleware"
	"crypto/ecdsa"
	"encoding/base64"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/satori/go.uuid"
	"time"
)

func (api *API) GenerateAuthTokens(user models.User, userEnv apimodels.UserEnv) (accessToken, refreshToken string, err error) {
	accessToken, err = api.generateAccessToken(user)
	if err != nil {
		return accessToken, refreshToken, err
	}
	refreshToken, err = api.generateRefreshToken() //user, userEnv)
	if err != nil {
		return accessToken, refreshToken, err
	}

	return accessToken, refreshToken, nil
}

func (api *API) generateAccessToken(user models.User) (string, error) {
	if user.Id == 0 {
		err := fmt.Errorf("can not generate tokens for user: user is not properly loaded")
		log.Errorf("[generateAccessToken] error: %s", err.Error())
		return "", err
	}

	var privateKey *ecdsa.PrivateKey
	pem, err := base64.StdEncoding.DecodeString(api.config.Auth.ECDSAPrivateKeyBase64)

	if err != nil {
		log.Errorf("[generateAccessToken] DecodeString error: %s", err.Error())
		return "", err
	}

	//log.Debugf("%s", string(pem))

	privateKey, err = jwt.ParseECPrivateKeyFromPEM(pem)

	if err != nil {
		log.Errorf("[generateAccessToken] ParseECPrivateKeyFromPEM error: %s", err.Error())
		return "", err
	}

	// create the jwt token
	token := jwt.NewWithClaims(jwt.SigningMethodES256, jwt.MapClaims{
		"exp": time.Now().Add(time.Second * conf.TtlJWT).Unix(),
	})
	token.Header[middleware.UserUuidHeader] = user.Uuid.String()
	token.Header[middleware.UserIdHeader] = user.Id
	token.Header[middleware.UserGroupHeader] = string(user.Group)

	accessToken, err := token.SignedString(privateKey)
	if err != nil {
		log.Errorf("[generateAccessToken] SignedString error: %s", err.Error())
		return "", err
	}

	return accessToken, nil
}

func (api *API) generateRefreshToken( /*user models.User, userEnv apimodels.UserEnv*/ ) (string, error) {
	//createdAt := time.Now()
	//expiresAt := time.Unix(createdAt.Unix()+conf.TtlRefreshToken, 0)

	refreshTokenUUID := uuid.NewV4()

	//TODO realise tokens mechanism
	//token := &models.Token{
	//	Uuid:        refreshTokenUUID,
	//	UserId:      user.Id,
	//	Type:        models.TokenTypeRefresh,
	//	ExpiresAt:   mysql.ToNullTime(expiresAt),
	//	RequestIP:   userEnv.IpAddress,
	//	RequestData: userEnv.UserAgent,
	//}
	//
	//_, err = this.dao.CreateToken(token, nil)
	//if err != nil {
	//	log.Errorf("[Auth] [CreateToken] error: %s", err.Error())
	//	return "", err
	//}

	return refreshTokenUUID.String(), nil
}
