package mailer

import "bitbucket.org/eternalzone/stalk.net/dao/models"

type (
	BaseEmailFields struct {
		Contacts string
		Sign     string
	}
	RegistrationEmailFields struct {
		EventName string
		Callsign  string
		Fraction  string
		Comment   string
		BaseEmailFields
	}

	ApprovedEmailFields struct {
		EventName         string
		Callsign          string
		Fraction          string
		Comment           string
		Amount            string
		DiscountDate      string
		DiscountAmountUAH string
		PayCard           string
		PayCardHolder     string
		BankName          string
		ReplyToEmail      string
		BaseEmailFields
	}
	ApprovedAndPaidEmailFields struct {
		EventName string
		Callsign  string
		Fraction  string
		Comment   string
		BaseEmailFields
	}

	PaidEmailFields struct {
		EventName  string
		Callsign   string
		Fraction   string
		Comment    string
		PaidAmount string
		BaseEmailFields
	}
	PaidPartiallyEmailFields struct {
		EventName         string
		Callsign          string
		Fraction          string
		Comment           string
		Amount            string
		DiscountDate      string
		DiscountAmountUAH string
		PaidAmount        string
		AmountDiff        string
		BaseEmailFields
	}

	CommentedEmailFields struct {
		EventName string
		Callsign  string
		Fraction  string
		Comment   string
		BaseEmailFields
	}
	DeclinedEmailFields struct {
		EventName string
		Callsign  string
		Fraction  string
		Comment   string
		BaseEmailFields
	}

	PasswordRestoreEmailFields struct {
		Link string
	}

	NotifyUnpaidEmailMemberFields struct {
		Callsign string
		Fraction string
		Amount   string
		User     models.User
	}

	NotifyUnpaidEmailFieldsArg struct {
		EventName     string
		MembersList   []NotifyUnpaidEmailMemberFields
		PayCard       string
		PayCardHolder string
		BankName      string
		ReplyToEmail  string
		BaseEmailFields
	}

	NotifyUnpaidEmailFields struct {
		EventName     string
		Callsign      string
		Fraction      string
		Amount        string
		PayCard       string
		PayCardHolder string
		BankName      string
		ReplyToEmail  string
		BaseEmailFields
	}
)
