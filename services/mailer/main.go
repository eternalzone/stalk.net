package mailer

import (
	"bitbucket.org/eternalzone/stalk.net/conf"
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"errors"
	"fmt"
	"github.com/go-gomail/gomail"
	"gorm.io/gorm"
	"strings"
	"time"
)

type IMailService interface {
	Send(email string, subject, bodyTxt, bodyHtml string) error
	CreateApprovedEmail(db *gorm.DB, user models.User, fromEmail string, fields ApprovedEmailFields) error
	CreateCommentedEmail(db *gorm.DB, user models.User, fromEmail string, fields CommentedEmailFields) error
	CreateDeclinedEmail(db *gorm.DB, user models.User, fromEmail string, fields DeclinedEmailFields) error
	CreateApprovedAndPaidEmail(db *gorm.DB, user models.User, fromEmail string, fields ApprovedAndPaidEmailFields) error
	CreatePaidEmail(db *gorm.DB, user models.User, fromEmail string, fields PaidEmailFields) error
	CreatePaidPartiallyEmail(db *gorm.DB, user models.User, fromEmail string, fields PaidPartiallyEmailFields) error
	CreatePasswordRestoreEmail(db *gorm.DB, user models.User, fromEmail string, fields PasswordRestoreEmailFields) error
	CreateNotifyUnpaidEmails(db *gorm.DB, fromEmail string, fields NotifyUnpaidEmailFieldsArg) error
}

type MailService struct {
	conf    conf.Config
	baseUrl string
}

// NewBalanceUpdater creates a new balance updater instance.
func NewMailService(c conf.Config) (IMailService, error) {
	baseUrl := strings.TrimSuffix(c.FrontendHost, "/")
	return &MailService{
		conf:    c,
		baseUrl: baseUrl,
	}, nil
}

type templateParam struct {
	Title   string
	Subject string
}

func (ms *MailService) Send(email string, subject, bodyTxt, bodyHtml string) error {
	m := gomail.NewMessage()
	m.SetHeader("Subject", subject)
	m.SetAddressHeader("From", ms.conf.SmtpSystem.FromEmail, ms.conf.SmtpSystem.FromName)
	m.SetAddressHeader("Reply-To", ReplyToEmail, ReplyToName)

	m.SetHeader("To", email)
	// Send email
	m.SetBody("text/plain", bodyTxt)

	if bodyHtml != "" {
		m.AddAlternative("text/html", bodyHtml)
	}

	d := gomail.NewPlainDialer(ms.conf.SmtpSystem.Host, ms.conf.SmtpSystem.Port, ms.conf.SmtpSystem.User, ms.conf.SmtpSystem.Password)

	err := d.DialAndSend(m)
	if err != nil {
		return errors.New(fmt.Sprintf("Cannot send e-mail: %s", err.Error()))
	}

	return nil
}

func getUTC(createdAt *time.Time) string {
	if createdAt == nil {
		t := time.Now()

		return getUTC(&t)
	}

	return createdAt.UTC().Format("2006-01-02 15:04:05 (UTC)")
}
