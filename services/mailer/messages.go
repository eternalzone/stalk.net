package mailer

import (
	"bitbucket.org/eternalzone/stalk.net/dao/models"
	"bitbucket.org/eternalzone/stalk.net/log"
	"bytes"
	"fmt"
	"gorm.io/gorm"
	"html/template"
	"strings"
)

func (ms *MailService) getEmailActionTextBody(textTemplate string, data interface{}) (body string, err error) {
	tmpl, err := template.New("ApprovedBody").Parse(textTemplate)
	if err != nil {
		return body, fmt.Errorf("cannot parse email body template: %s", err.Error())
	}

	var tpl bytes.Buffer
	err = tmpl.Execute(&tpl, data)
	if err != nil {
		return body, fmt.Errorf("cannot execute email body template: %s", err.Error())
	}

	return tpl.String(), nil
}

func (ms *MailService) CreateApprovedEmail(db *gorm.DB, user models.User, fromEmail string, fields ApprovedEmailFields) error {
	fields.Comment = ""
	if strings.TrimSpace(fields.Comment) != "" {
		fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	}

	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	fields.PayCard = fmt.Sprintf("%s", PayCard)
	fields.PayCardHolder = fmt.Sprintf("%s", PayCardHolder)
	fields.BankName = fmt.Sprintf("%s", BankName)
	fields.ReplyToEmail = fmt.Sprintf("%s", ReplyToEmail)

	body, err := ms.getEmailActionTextBody(ApprovedEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Заявка принята",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error.Error())
		return res.Error
	}

	return nil
}

func (ms *MailService) CreateCommentedEmail(db *gorm.DB, user models.User, fromEmail string, fields CommentedEmailFields) error {
	if fields.Comment != "" {
		fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	}
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	body, err := ms.getEmailActionTextBody(CommentedEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Заявка прокомментирована",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreateDeclinedEmail(db *gorm.DB, user models.User, fromEmail string, fields DeclinedEmailFields) error {
	fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	body, err := ms.getEmailActionTextBody(DeclinedEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Заявка отклонена",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreateApprovedAndPaidEmail(db *gorm.DB, user models.User, fromEmail string, fields ApprovedAndPaidEmailFields) error {
	fields.Comment = ""
	if strings.TrimSpace(fields.Comment) != "" {
		fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	}
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	body, err := ms.getEmailActionTextBody(ApprovedAndPaidEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Заявка принята",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreatePaidEmail(db *gorm.DB, user models.User, fromEmail string, fields PaidEmailFields) error {
	fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	body, err := ms.getEmailActionTextBody(PaidEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Взнос принят",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreatePaidPartiallyEmail(db *gorm.DB, user models.User, fromEmail string, fields PaidPartiallyEmailFields) error {
	fields.Comment = fmt.Sprintf("%s %s", CommentMessage, fields.Comment)
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	body, err := ms.getEmailActionTextBody(PaidPartiallyEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Часть взноса принята",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypeNotification,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreatePasswordRestoreEmail(db *gorm.DB, user models.User, fromEmail string, fields PasswordRestoreEmailFields) error {
	body, err := ms.getEmailActionTextBody(PasswordRestoreEmailTextTemplate, fields)
	if err != nil {
		return err
	}

	email := models.Email{
		Title:    "Сброс пароля в stalk.net.ua",
		UserIdTo: user.Id,
		From:     fromEmail,
		To:       user.Email,
		Body:     body,
		BodyHtml: "",
		Status:   models.EmailStatusSent,
		Type:     models.EmailTypePM,
	}

	log.Infof("sending EMAIL %d to %s", email.Id, email.To)

	err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
	if err != nil {
		email.Status = models.EmailStatusError
		email.Notice = fmt.Sprintf("error: %s", err.Error())
		log.Errorf("cannot send email %d: %s", email.Id, err.Error())
		//no return to save email data
	}

	res := db.Create(&email)
	if res.Error != nil {
		log.Errorf("Cannot create email to DB: %s", res.Error)
		return res.Error
	}

	return nil
}

func (ms *MailService) CreateNotifyUnpaidEmails(db *gorm.DB, fromEmail string, fields NotifyUnpaidEmailFieldsArg) error {
	fields.Contacts = fmt.Sprintf("%s", Contacts)
	fields.Sign = fmt.Sprintf("%s", Sign)

	fields.PayCard = fmt.Sprintf("%s", PayCard)
	fields.PayCardHolder = fmt.Sprintf("%s", PayCardHolder)
	fields.BankName = fmt.Sprintf("%s", BankName)
	fields.ReplyToEmail = fmt.Sprintf("%s", ReplyToEmail)

	for i := range fields.MembersList {
		f := NotifyUnpaidEmailFields{
			EventName:     fields.EventName,
			Callsign:      fields.MembersList[i].Callsign,
			Fraction:      fields.MembersList[i].Fraction,
			Amount:        fields.MembersList[i].Amount,
			PayCard:       fields.PayCard,
			PayCardHolder: fields.PayCardHolder,
			BankName:      fields.BankName,
			ReplyToEmail:  fields.ReplyToEmail,
			BaseEmailFields: BaseEmailFields{
				Contacts: fields.Contacts,
				Sign:     fields.Sign,
			},
		}
		body, err := ms.getEmailActionTextBody(NotifyUnpaidEmailsTextTemplate, f)
		if err != nil {
			return err
		}

		email := models.Email{
			Title:    "Напоминание об оплате взноса",
			UserIdTo: fields.MembersList[i].User.Id,
			From:     fromEmail,
			To:       fields.MembersList[i].User.Email,
			Body:     body,
			BodyHtml: "",
			Status:   models.EmailStatusSent,
			Type:     models.EmailTypeNotification,
		}

		log.Infof("sending EMAIL %d to %s", email.Id, email.To)

		err = ms.Send(email.To, email.Title, email.Body, email.BodyHtml)
		if err != nil {
			email.Status = models.EmailStatusError
			email.Notice = fmt.Sprintf("error: %s", err.Error())
			log.Errorf("cannot send email %d: %s", email.Id, err.Error())
			//no return to save email data
		}

		res := db.Create(&email)
		if res.Error != nil {
			log.Errorf("Cannot create email to DB: %s", res.Error.Error())
			return res.Error
		}
	}

	return nil
}
